
#include "ika.h"
#include "caret.h"
#include "ai.h"
#include "caret.fdh"

Caret caret[MAX_CARETS];
CaretMaker caretmaker[MAX_CARETMAKERS];

// CreateCaret/CreateCaretMaker returns these as dummies to prevent
// crashing, if we fail because the max carets is reached.
// that way we don't have to enclose every caret creation in a if() statement.
Caret dummy_caret;
CaretMaker dummy_caretmaker;

static void (*ai_table[NUM_CARETS])(Caret *o) =
{
	ai_star,
	ai_bubble,
	ai_numbers,
	ai_levelup
};

static int caret_sprites[NUM_CARETS] =
{
	SPR_STAR,
	SPR_BUBBLE,
	SPR_NUMBERS,
	SPR_LEVELUP,
};

Caret *CreateCaret(int type)
{
	for(int i=0;i<MAX_CARETS;i++)
	{
		if (!caret[i].in_use)
		{
			memset(&caret[i], 0, sizeof(Caret));
			caret[i].in_use = true;
			caret[i].type = type;
			return &caret[i];
		}
	}
	
	//staterr("Caret table full");
	return &dummy_caret;
}

void run_carets(void)
{
	for(int i=0;i<MAX_CARETS;i++)
	{
		if (caret[i].in_use)
		{
			(*ai_table[caret[i].type])(&caret[i]);
		}
	}
}

void DrawCarets(void)
{
	for(int i=0;i<MAX_CARETS;i++)
	{
		if (!caret[i].in_use) continue;
		Caret *o = &caret[i];
		
		int spr = caret_sprites[o->type];
		int scr_x = (o->x >> CSF) - (map.xscroll >> CSF);
		int scr_y = (o->y >> CSF) - (map.yscroll >> CSF);
		scr_x -= sprites[spr].frame[o->frame].dir[0].drawpoint.x;
		scr_y -= sprites[spr].frame[o->frame].dir[0].drawpoint.y;
		
		draw_sprite(scr_x, scr_y, spr, o->frame, o->dir);
	}
}

void DeleteCarets(void)
{
	for(int i=0;i<MAX_CARETS;i++)
		caret[i].in_use = false;
}

/*
void c------------------------------() {}
*/

CaretMaker *CreateCaretMaker(int type, int amt)
{
	for(int i=0;i<MAX_CARETMAKERS;i++)
	{
		if (!caretmaker[i].in_use)
		{
			memset(&caretmaker[i], 0, sizeof(CaretMaker));
			caretmaker[i].in_use = true;
			caretmaker[i].type = type;
			caretmaker[i].amt = amt;
			return &caretmaker[i];
		}
	}
	
	//staterr("CaretMaker table full");
	return &dummy_caretmaker;
}

void run_caretmakers(void)
{
	for(int i=0;i<MAX_CARETMAKERS;i++)
	{
		if (!caretmaker[i].in_use) continue;
		caretmaker[i].in_use = false;
		CaretMaker *maker = &caretmaker[i];
		
		while(maker->amt > 0)
		{
			Caret *c = CreateCaret(0);
			if (!c) break;
			
			c->type = maker->type;
			c->frame = maker->frame;
			c->dir = maker->dir;
			
			c->x = maker->x + (randrange(-maker->xrange, maker->xrange) << CSF);
			c->y = maker->y + (randrange(-maker->yrange, maker->yrange) << CSF);
			
			c->xinertia = randrange(maker->min_xinertia, maker->max_xinertia);
			c->yinertia = randrange(maker->min_yinertia, maker->max_yinertia);
			
			maker->amt--;
		}
	}
	
}

/*
void c------------------------------() {}
*/

/* caret parameters

	+4	is the caret, of course
	+8	is the map info struct
*/

void ai_star(Caret *o)
{
	o->yinertia += 0x20;
	o->x += o->xinertia;
	o->y += o->yinertia;
	
	if (WENT_OFF_MAP(o))
		o->in_use = false;
	
	if (++o->timer > 8)
	{
		o->timer = 0;
		if (++o->frame > 5)
			o->in_use = false;
	}
}


void ai_bubble(Caret *o)
{
	o->yinertia -= 0x10;
	o->x += o->xinertia;
	o->y += o->yinertia;
	
	if (WENT_OFF_MAP(o))
	{
		o->in_use = false;
	}
	
	if (++o->timer > 32)
	{
		o->timer = 0;
		if (++o->frame > 5)
			o->in_use = false;
	}
}

void ai_numbers(Caret *o)
{
	o->y -= 0x155;
	
	if (WENT_OFF_MAP(o) || ++o->timer > 72)
		o->in_use = false;
}

void ai_levelup(Caret *o)
{
	o->y -= 0x155;
	
	if (WENT_OFF_MAP(o))
		o->in_use = false;
	
	if (++o->timer > 3)
	{
		o->timer = 0;
		if (++o->timer2 > 24)
			o->in_use = false;
		
		o->frame = (o->timer2 & 1);
	}
}





