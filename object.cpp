
#include "ika.h"
#include "object.h"
#include "object.fdh"
#include <cassert>

Object objects[MAX_OBJECTS];
Object dummy_object;
ObjProp objprop[NUM_OBJECT_TYPES];

static const char *SAVEFILE_FNAME = "NPChar.dat";

Object *CreateObject(int x, int y, int type)
{
int i;

	for(i=0;;i++)
	{
		if (i >= MAX_OBJECTS)
		{
			staterr("@@ failed to create object--out of object slots");
			memset(&dummy_object, 0, sizeof(dummy_object));
			return &dummy_object;
		}
		
		if (!objects[i].exists)
			break;
	}
	
	// initilize
	memset(&objects[i], 0, sizeof(Object));
	objects[i].x = x;
	objects[i].y = y;
	objects[i].type = type;
	objects[i].exists = true;
	
	return &objects[i];
}

// deletes the specified object, or well, marks it to be deleted.
// it's not actually freed till the end of the tick.
void Object::Delete()
{
	this->exists = false;
}

/*
void c------------------------------() {}
*/

bool objects_init(void)
{
	memset(objects, 0, sizeof(objects));
	for(int i=0;i<MAX_OBJECTS;i++)
		objects[i].exists = false;
	
	if (objects_load())
		return 1;
	
	init_ironhead();
	return 0;
}

bool objects_load(void)
{
	Objects::DeleteAll();
	
	char fname[MAXPATHLEN];
	GetDataFilename(SAVEFILE_FNAME, fname, sizeof(fname));
	stat("loading object states from %s", fname);
	
	FILE *fp = fopen(fname, "rb");
	if (!fp)
	{
		staterr("failed to open %s", fname);
		return 1;
	}
	
	int count = 0;
	for(int i=0;i<MAX_OBJECTS;i++)
	{
		objects[i].exists = fgetc(fp);
		objects[i].ty = fgetc(fp);
		objects[i].type = fgetc(fp);
		objects[i].event = fgeti(fp);
		objects[i].dir = fgetc(fp);
		
		objects[i].x = fgetl(fp);
		objects[i].y = fgetl(fp);
		objects[i].x2 = fgetl(fp);
		objects[i].y2 = fgetl(fp);
		
		if (objects[i].exists)
		{
			count++;
			
			if (objects[i].type >= NUM_OBJECT_TYPES)
			{
				staterr("invalid object type %d on entity %d", objects[i].type, i);
				fclose(fp);
				return 1;
			}
		}
	}
	
	stat("loaded %d objects", count);
	fclose(fp);
	return 0;
}

bool restore_enemies(void)
{
char fname[MAXPATHLEN];
FILE *fp;

	GetDataFilename(SAVEFILE_FNAME, fname, sizeof(fname));
	stat("restoring objects from %s", fname);
	
	fp = fopen(fname, "rb");
	if (!fp)
	{
		staterr("failed to open %s", fname);
		return 1;
	}
	
	Object obj;
	memset(&obj, 0, sizeof(Object));
	
	int count = 0;
	for(int i=0;i<MAX_OBJECTS;i++)
	{
		obj.exists = fgetc(fp);		// exists
		obj.ty = fgetc(fp);			// ty
		obj.type = fgetc(fp);		// type
		obj.event = fgeti(fp);		// event
		obj.dir = fgetc(fp);		// dir
		
		obj.x = fgetl(fp);			// x
		obj.y = fgetl(fp);			// y
		obj.x2 = fgetl(fp);			// x2
		obj.y2 = fgetl(fp);			// y2
		
		if (obj.ty == TY_ENEMY)
		{
			memcpy(&objects[i], &obj, sizeof(Object));
			count++;
		}
	}
	
	stat("restored %d enemies", count);
	fclose(fp);
	return 0;
}


bool objects_save(void)
{
	char fname[MAXPATHLEN];
	GetDataFilename(SAVEFILE_FNAME, fname, sizeof(fname));
	stat("saving object states to %s", fname);
	
	FILE *fp = fopen(fname, "wb");
	if (!fp)
	{
		staterr("failed to open %s", fname);
		return 1;
	}
	
	int count = 0;
	for(int i=0;i<MAX_OBJECTS;i++)
	{
		fputc(objects[i].exists, fp);
		fputc(objects[i].ty, fp);
		fputc(objects[i].type, fp);
		fputi(objects[i].event, fp);
		fputc(objects[i].dir, fp);
		
		fputl(objects[i].x, fp);
		fputl(objects[i].y, fp);
		fputl(objects[i].x2, fp);
		fputl(objects[i].y2, fp);
		
		if (objects[i].exists)
			count++;
	}
	
	stat("saved %d objects", count);
	fclose(fp);
	return 0;
}

/*
void c------------------------------() {}
*/

// for each point in pointlist, treats the point as a CSF'ed offset
// within the object's sprite. The tile under each position is checked
// to see if it's attributes contain one or more of the attributes
// specified in attrmask.
//
// If any of the points match, returns 1, and optionally returns
// the map coordinates of the first matched tile in tile_x/y.
bool Object::CheckAttribute(const SIFPoint *pointlist, int npoints, uint32_t attrmask, \
							int *tile_x, int *tile_y)
{
int x, y, xoff, yoff;

	xoff = (this->x >> CSF);
	yoff = (this->y >> CSF);
	
	for(int i=0;i<npoints;i++)
	{
		x = (xoff + pointlist[i].x) / TILE_W;
		y = (yoff + pointlist[i].y) / TILE_H;
		
		if (x >= 0 && y >= 0 && x < map.xsize && y < map.ysize)
		{
			if ((tileattr[map.tiles[x][y]] & attrmask) != 0)
			{
				if (tile_x) *tile_x = x;
				if (tile_y) *tile_y = y;
				return true;
			}
		}
	}
	
	return false;
}

// for each point in pointlist, treats the point as a CSF'ed offset
// within the object's sprite. Then checks the attributes of the tile
// under each point. Returns an attribute mask containing the cumulative
// attributes of all the tiles under each point in the list.
//
// if tile is non-null, it is set to the tile type of the last tile checked.
uint32_t Object::GetAttributes(const SIFPoint *pointlist, int npoints, int *tile)
{
int tileno = 0;
uint32_t attr = 0;

	int xoff = (this->x >> CSF);
	int yoff = (this->y >> CSF);
	
	for(int i=0;i<npoints;i++)
	{
		int x = (xoff + pointlist[i].x) / TILE_W;
		int y = (yoff + pointlist[i].y) / TILE_H;
		
		if (x >= 0 && y >= 0 && x < map.xsize && y < map.ysize)
		{
			tileno = map.tiles[x][y];
			attr |= tileattr[tileno];
		}
	}
	
	if (tile) *tile = tileno;
	return attr;
}

// update the blocked states of object o.
// updatemask specifies which states are in need of updating.
void Object::UpdateBlockStates(uint8_t updatemask)
{
Object * const &o = this;
SIFSprite *sprite = Sprite();

	if (updatemask & LEFTMASK)
		o->blockl = CheckAttribute(&sprite->block_l, TA_SOLID);
	
	if (updatemask & RIGHTMASK)
		o->blockr = CheckAttribute(&sprite->block_r, TA_SOLID);
	
	if (updatemask & UPMASK)
		o->blocku = CheckAttribute(&sprite->block_u, TA_SOLID);
	
	if (updatemask & DOWNMASK)
		o->blockd = CheckAttribute(&sprite->block_d, TA_SOLID);
}

/*
void c------------------------------() {}
*/

void Object::Move(void)
{
Object * const &o = this;

	apply_xinertia(o->xinertia);
	apply_yinertia(o->yinertia);
	
	if (o->blockd && o->yinertia > 0) o->yinertia = 0;
	if (o->blocku && o->yinertia < 0) o->yinertia = 0;
}

// tries to move the object in the X direction by the given amount.
// returns nonzero if the object was blocked.
bool Object::apply_xinertia(int inertia)
{
Object * const &o = this;

	if (inertia == 0)
		return 0;
	
	// only apply inertia one pixel at a time so we have
	// proper hit detection--prevents objects traveling at
	// high speed from becoming embedded in walls
	if (inertia > 0)
	{
		if (o->blockr) return 1;
		
		while(inertia > (1<<CSF))
		{
			o->x += (1 << CSF);
			inertia -= (1<<CSF);
			
			o->UpdateBlockStates(RIGHTMASK);
			if (o->blockr) return 1;
		}
	}
	else if (inertia < 0)
	{
		if (o->blockl) return 1;
		
		while(inertia < -(1<<CSF))
		{
			o->x -= (1 << CSF);
			inertia += (1<<CSF);
			
			o->UpdateBlockStates(LEFTMASK);
			if (o->blockl) return 1;
		}
	}
	
	// apply any remaining inertia
	if (inertia)
		o->x += inertia;
	
	return 0;
}

// tries to move the object in the Y direction by the given amount.
// returns nonzero if the object was blocked.
bool Object::apply_yinertia(int inertia)
{
Object * const &o = this;

	if (inertia == 0)
		return 0;
	
	// only apply inertia one pixel at a time so we have
	// proper hit detection--prevents objects traveling at
	// high speed from becoming embedded in walls
	if (inertia > 0)
	{
		if (o->blockd) return 1;
		
		while(inertia > (1<<CSF))
		{
			o->y += (1<<CSF);
			inertia -= (1<<CSF);
			
			o->UpdateBlockStates(DOWNMASK);
			if (o->blockd) return 1;
		}
	}
	else if (inertia < 0)
	{
		if (o->blocku) return 1;
		
		while(inertia < -(1<<CSF))
		{
			o->y -= (1<<CSF);
			inertia += (1<<CSF);
			
			o->UpdateBlockStates(UPMASK);
			if (o->blocku) return 1;
		}
	}
	
	// apply any remaining inertia
	if (inertia)
		o->y += inertia;
	
	return 0;
}

/*
void c------------------------------() {}
*/

// destroys/frees all objects immediately.
void Objects::DeleteAll(void)
{
Object *o;

	staterr("===== Objects::DeleteAll() =====");
	FOREACH_OBJECT(o)
	{
		//staterr("DeleteAll: o = %p, ITERATOR = %d; MAX_OBJECTS = %d", o, ITERATOR, MAX_OBJECTS);
		o->Delete();
	}
	//staterr("<< DeleteAll() out");
}

// update the blocked states of all objects
void Objects::UpdateAllBlockStates(void)
{
	Object *o;
	FOREACH_OBJECT(o)
	{
		o->UpdateBlockStates(ALLDIRMASK);
	}
}


int GetObjectIndex(Object *o)
{
	for(int i=0;i<MAX_OBJECTS;i++)
	{
		if (o == &objects[i])
			return i;
	}
	
	return -1;
}

/*
void c------------------------------() {}
*/

void Object::Draw()
{
Object * const &o = this;

	int scr_x = (o->x >> CSF) - (map.xscroll >> CSF);
	int scr_y = (o->y >> CSF) - (map.yscroll >> CSF);
	scr_x -= sprites[objprop[o->type].sprite].frame[o->frame].dir[o->dir].drawpoint.x;
	scr_y -= sprites[objprop[o->type].sprite].frame[o->frame].dir[o->dir].drawpoint.y;
	
	/*const char *str = stprintf("%04x", o->state);
	int x = scr_x + 8;
	x -= (GetFontWidth(str, FONT_DEFAULT_SPACING) / 2);
	font_draw(x, scr_y - GetFontHeight() - 1, \
				str, FONT_DEFAULT_SPACING, &greenfont);*/
	
	draw_sprite(scr_x, scr_y, objprop[o->type].sprite, o->frame, o->dir);
}












