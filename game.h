
#ifndef _GAME_H
#define _GAME_H

#define NUM_FLAGS		256
#define FLAGDATA_LEN	(NUM_FLAGS / 8)
typedef uint32_t tstamp;

enum
{
	GM_NORMAL,
	GM_ROLL,
	GM_TITLE,
	GM_EDITOR
};

struct Game
{
	bool running;
	int ffwdtime;
	int mode;
	
	bool hidescene;
	bool hidestatusbar;
	tstamp loadstarttime;
	
	int entcount;			// number of NPC's on-board the ship
	uint8_t flagdata[FLAGDATA_LEN];
	
	// static member functions--not private (Game is an object, not a namespace)
	static bool init();
	static bool initlevel();
	static void close();
	
	static void tick();
	bool setmode(int newmode);
	
	static bool readflag(uint16_t no);
	static void setflag(uint16_t no, bool newvalue);
};

extern Game game;

extern const char *savefilename;
bool game_load(const char *fname = savefilename);
bool game_save(const char *fname = savefilename);

#endif
