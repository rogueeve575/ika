
#include "ika.h"
#include "itembox.h"

#define MAXLINELEN			26
#define CONTENT_X			(MSG_X + 16)
#define CONTENT_Y			(MSG_Y + 6)

#define FONT_SPACING		8
#define LINE_SPACING		16

ItemBox itembox;

bool ItemBox::Init()
{
	fVisible = false;
	return 0;
}

/*
void c------------------------------() {}
*/

void ItemBox::SetVisible(bool enable)
{
	if (fVisible != enable)
	{
		fVisible = enable;
		newinputs &= ~(SKEY | ZKEY | LEFTKEY | RIGHTKEY);
		fCurSel = 0;
	}
}

bool ItemBox::IsVisible(void)
{
	return fVisible;
}

/*
void c------------------------------() {}
*/

void ItemBox::Run(void)
{
	if (newinputs & SKEY)
	{
		SetVisible(false);
		return;
	}
	
	if (textbox.IsVisible())
		return;
	
	if (newinputs & LEFTKEY)
	{
		sound(SND_PROPEL);
		if (--fCurSel < 0) fCurSel = (NUM_INVENTORY_SLOTS - 1);
	}
	
	if (newinputs & RIGHTKEY)
	{
		sound(SND_PROPEL);
		if (++fCurSel >= NUM_INVENTORY_SLOTS) fCurSel = 0;
	}
	
	if (newinputs & ZKEY)
	{
		int item = player->inventory[fCurSel];
		if (item != ITEM_NULL)
			StartScript(2000 + item);
	}
}


void ItemBox::Draw(void)
{
	if (!fVisible) return;
	
	draw_sprite(IB_X, IB_Y, SPR_ITEMBOX);
	
	// draw item list
	int x = ISEL_X;
	int index = 0;
	do
	{
		draw_sprite(x, ISEL_Y, SPR_ITEM, player->inventory[index++]);
		x += 16;
	}
	while(index < NUM_INVENTORY_SLOTS);
	
	// draw item selector
	draw_sprite(ISEL_X + (fCurSel * 16), ISEL_Y, SPR_ITEMCURSOR);
}


