
// sprites routines
#include "graphics.h"
#include <string.h>
#include "../siflib/sif.h"
#include "../siflib/sifloader.h"
#include "../siflib/sectSprites.h"
#include "../siflib/sectStringArray.h"
#include "../common/StringList.h"
#include "../datadirs.h"
using namespace Graphics;

#include "sprites.h"
#include "sprites.fdh"

static NXSurface *spritesheet[MAX_SPRITESHEETS];
static int num_spritesheets;
static StringList sheetfiles;

SIFSprite sprites[MAX_SPRITES];
int num_sprites;


bool Sprites::Init()
{
	memset(spritesheet, 0, sizeof(spritesheet));
	
	// load sprites info--sheet positions, bounding boxes etc
	if (load_sif("sprites.sif"))
		return 1;
	
	num_spritesheets = sheetfiles.CountItems();
	return 0;
}

void Sprites::Close()
{
	FlushSheets();
	sheetfiles.MakeEmpty();
}

void Sprites::FlushSheets()
{
	for(int i=0;i<MAX_SPRITESHEETS;i++)
	{
		if (spritesheet[i])
		{
			delete spritesheet[i];
			spritesheet[i] = NULL;
		}
	}
}

/*
void c------------------------------() {}
*/

// ensure the given spritesheet is loaded
static void Sprites::LoadSheetIfNeeded(int sheetno)
{
	if (!spritesheet[sheetno]) {
		char pbm_name[MAXPATHLEN];
		GetGraphicsFilename(sheetfiles.StringAt(sheetno), pbm_name, sizeof(pbm_name));
		stat("Lazyloading spritesheet '%s'", pbm_name);

		spritesheet[sheetno] = new NXSurface;
		spritesheet[sheetno]->LoadImage(pbm_name, true);
	}
}


// master sprite drawing function
static void Sprites::BlitSprite(int x, int y, int s, int frame, uint8_t dir, \
								int xoff, int yoff, int wd, int ht)
{
	LoadSheetIfNeeded(sprites[s].spritesheet);
	
	dir %= sprites[s].ndirs;
	SIFDir *sprdir = &sprites[s].frame[frame].dir[dir];
	
	DrawSurface(spritesheet[sprites[s].spritesheet], \
				x, y, \
				(sprdir->sheet_offset.x + xoff), \
				(sprdir->sheet_offset.y + yoff), \
				wd, ht);
}

// draw sprite "s" at [x,y]. drawing frame "frame" and dir "dir".
void Sprites::draw_sprite(int x, int y, int s, int frame, uint8_t dir)
{
	BlitSprite(x, y, s, frame, dir, 0, 0, sprites[s].w, sprites[s].h);
}

/*
void c------------------------------() {}
*/

// return the NXSurface for a given spritesheet #
NXSurface *Sprites::get_spritesheet(int sheetno)
{
	LoadSheetIfNeeded(sheetno);
	return spritesheet[sheetno];
}

// create an empty spritesheet of the given size and return it's index.
int Sprites::create_spritesheet(int wd, int ht)
{
	if (num_spritesheets >= MAX_SPRITESHEETS)
		return -1;
	
	spritesheet[num_spritesheets] = new NXSurface(wd, ht);
	return num_spritesheets++;
}

// draw a sprite onto some surface other than the screen
void Sprites::draw_sprite_to_surface(NXSurface *dst, int x, int y, int s, int frame, uint8_t dir)
{
	Graphics::SetDrawTarget(dst);
	draw_sprite(x, y, s, frame, dir);
	Graphics::SetDrawTarget(screen);
}

/*
void c------------------------------() {}
*/

static bool load_sif(const char *fname)
{
SIFLoader sif;
uint8_t *sheetdata, *spritesdata;
int sheetdatalength, spritesdatalength;

	if (sif.LoadHeader(fname))
		return 1;
	
	if (!(sheetdata = sif.FindSection(SIF_SECTION_SHEETS, &sheetdatalength)))
	{
		staterr("load_sif: file '%s' missing SIF_SECTION_SHEETS", fname);
		return 1;
	}
	
	if (!(spritesdata = sif.FindSection(SIF_SECTION_SPRITES, &spritesdatalength)))
	{
		staterr("load_sif: file '%s' missing SIF_SECTION_SPRITES", fname);
		return 1;
	}
	
	// decode sheets
	sheetfiles.MakeEmpty();
	if (SIFStringArraySect::Decode(sheetdata, sheetdatalength, &sheetfiles))
		return 1;
	
	// decode sprites
	if (SIFSpritesSect::Decode(spritesdata, spritesdatalength, \
						&sprites[0], &num_sprites, MAX_SPRITES))
	{
		staterr("load_sif: SIFSpritesSect decoder failed");
		return 1;
	}
	
	sif.CloseFile();
	
	offset_by_draw_points();
	return 0;
}


// offset things like blockl/r/u/d, bounding box etc by the draw point of all
// sprites so that these things are consistent with where the sprite appears to be
static void offset_by_draw_points()
{
	for(int s=0;s<num_sprites;s++)
	{
		int dx = -sprites[s].frame[0].dir[0].drawpoint.x;
		int dy = -sprites[s].frame[0].dir[0].drawpoint.y;
		
		sprites[s].bbox.offset(dx, dy);
		sprites[s].slopebox.offset(dx, dy);
		sprites[s].solidbox.offset(dx, dy);
		
		sprites[s].block_l.offset(dx, dy);
		sprites[s].block_r.offset(dx, dy);
		sprites[s].block_u.offset(dx, dy);
		sprites[s].block_d.offset(dx, dy);
		
		for(int f=0;f<sprites[s].nframes;f++)
		{
			for(int d=0;d<sprites[s].ndirs;d++)
			{
				int dx = -sprites[s].frame[f].dir[d].drawpoint.x;
				int dy = -sprites[s].frame[f].dir[d].drawpoint.y;
				sprites[s].frame[f].dir[d].pf_bbox.offset(dx, dy);
			}
		}
	}
}

