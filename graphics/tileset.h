
#ifndef _TILESET_H
#define _TILESET_H

#define TILE_W				16
#define TILE_H				16

namespace Tileset
{
	bool Init();
	void Close();
	
	bool Load(int number, const char *file);
	void draw_tile(int x, int y, int t);
	
	void draw_bubbles(NXSurface *sfc, int x, int y, int dir);
	void move_bubbles();
	
	NXSurface *GetSurface();
};


#endif
