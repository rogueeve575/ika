
#ifndef _DIRS_H
#define _DIRS_H

enum
{
	LEFT,
	RIGHT,
	UP,
	DOWN
};

enum
{
	LEFTMASK  = 0x01,
	RIGHTMASK = 0x02,
	UPMASK    = 0x04,
	DOWNMASK  = 0x08,
	
	ALLDIRMASK = 0xff
};

#endif
