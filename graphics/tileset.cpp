
// manages the tileset
#include "graphics.h"
#include "dirs.h"
#include "../datadirs.h"
#include "tileset.h"
using namespace Graphics;

static NXSurface *tileset[16];
static int bubblepos;

bool Tileset::Init()
{
	memset(tileset, 0, sizeof(tileset));
	return 0;
}

void Tileset::Close()
{
	for(int i=0;i<16;i++)
		delete tileset[i];
}

/*
void c------------------------------() {}
*/

// load the given tileset into memory, replacing any other tileset.
bool Tileset::Load(int baseoffs, const char *file)
{
	char fullfname[MAXPATHLEN];
	GetGraphicsFilename(file, fullfname, sizeof(fullfname));
	stat("Loading Tileset: '%s'", fullfname);
	
	baseoffs /= 16;
	delete tileset[baseoffs];
	
	tileset[baseoffs] = NXSurface::FromFile(fullfname, true);
	if (!tileset[baseoffs])
	{
		staterr("failed to load tileset '%s'!", fullfname);
		return 1;
	}
	
	stat("0x%02x: loaded %s", baseoffs, file);
	return 0;
}

// draw the given tile from the current tileset to the screen
void Tileset::draw_tile(int x, int y, int t)
{
	int sheetno = (t / 16);
	
	if (sheetno == 6)	// PrtDir moving bubbles
	{
		draw_bubbles(tileset[sheetno], x, y, (t % 16));
	}
	else if (tileset[sheetno])
	{
		int srcx = (TILE_W * (t % 16));
		DrawSurface(tileset[sheetno], x, y, srcx, 0, TILE_W, TILE_H);
	}
	else
	{
		FillRect(x, y, x+15, y+15, 0, t, 0);
	}
}

// draw moving bubbles
void Tileset::draw_bubbles(NXSurface *sfc, int x, int y, int dir)
{
int srcx, srcy;

	int bp = (bubblepos >> CSF);
	switch(dir)
	{
		case LEFT:
		{
			srcx = bp;
			srcy = 0;
		}
		break;
		
		case UP:
		{
			srcx = 0;
			srcy = bp;
		}
		break;
		
		case RIGHT:
		{
			srcx = (TILE_W - bp);
			srcy = 0;
		}
		break;
		
		case DOWN:
		{
			srcx = 0;
			srcy = (TILE_H - bp);
		}
		break;

		default:
			staterr("invalid direction!");
			return;
	}

	DrawSurface(sfc, x, y, srcx, srcy, TILE_W, TILE_H);
}


void Tileset::move_bubbles()
{
	const int speed = (1 << CSF) + ((1 << CSF) / 2);
	bubblepos = (bubblepos + speed) % (TILE_W << CSF);
}











