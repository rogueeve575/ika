
// graphics routines
#include <SDL/SDL.h>
#include <SDL/SDL_getenv.h>
#include <stdlib.h>
#include "graphics.h"
#include "graphics.fdh"

NXSurface *screen = NULL;				// created from SDL's screen
static NXSurface *drawtarget = NULL;	// target of DrawRect etc; almost always screen
int screen_bpp = 16;					// the default if we can't get video info

const NXColor BLACK(0, 0, 0);			// pure black, only works if no colorkey

static bool is_fullscreen = false;
static int current_res = -1;

bool Graphics::init(int scaleFactor)
{
const SDL_VideoInfo *info;

	// it's faster if we create the SDL screen at the bpp of the real screen.
	// max fps went from 120 to 160 on my X11 system this way.
	if ((info = SDL_GetVideoInfo()))
	{
		stat("videoinfo: desktop bpp %d", info->vfmt->BitsPerPixel);
		if (info->vfmt->BitsPerPixel > 8)
			screen_bpp = info->vfmt->BitsPerPixel;
	}
	
	if (SetResolution(scaleFactor, false))
		return 1;
	
	return 0;
}

void Graphics::close()
{
	stat("Graphics::Close()");
	SDL_ShowCursor(true);
}

/*
void c------------------------------() {}
*/

bool Graphics::InitVideo()
{
SDL_Surface *sdl_screen;

	stat("Graphics::InitVideo");
	if (drawtarget == screen) drawtarget = NULL;
	if (screen) delete screen;
	
	uint32_t flags = SDL_SWSURFACE;
	if (is_fullscreen) flags |= SDL_FULLSCREEN;
	
	putenv((char *)"SDL_VIDEO_CENTERED=1");
	SDL_WM_SetCaption("nxIka", NULL);
	
	sdl_screen = SDL_SetVideoMode(SCREEN_WIDTH*SCALE, SCREEN_HEIGHT*SCALE, screen_bpp, flags);
	if (!sdl_screen)
	{
		staterr("error setting video mode: %s", SDL_GetError());
		return 1;
	}
	
	SDL_ShowCursor(is_fullscreen == false);
	
	screen = new NXSurface(sdl_screen, false);
	if (!drawtarget) drawtarget = screen;
	return 0;
}

bool Graphics::FlushAll()
{
	stat("Graphics::FlushAll()");
	return font_reload();
}

void Graphics::SetFullscreen(bool enable)
{
	if (is_fullscreen != enable)
	{
		is_fullscreen = enable;
		InitVideo();
		Graphics::FlushAll();
	}
}

// change the video mode to one of the available resolution codes, currently:
// 0 - 640x480, Fullscreen
// 1 - Windowed scale x1 (320x240)
// 2 - Windowed scale x2 (640x480)
// 3 - Windowed scale x3 (960x720)
bool Graphics::SetResolution(int r, bool restoreOnFailure)
{
	stat("Graphics::SetResolution(%d)", r);
	if (r > MAXSCALE || r < 1) {
		staterr("invalid scaling factor %dx", r);
		return 1;
	}

	if (r == current_res)
		return 0;
	
	int old_res = current_res;
	int factor;
	
	if (r == 0)
	{
		is_fullscreen = true;
		factor = 2;
	}
	else
	{
		is_fullscreen = false;
		factor = r;
	}
	
	stat("Setting scaling %dx and fullscreen=%s", factor, is_fullscreen ? "yes":"no");
	NXSurface::SetScale(factor);
	
	if (Graphics::InitVideo())
	{
		staterr("Switch to resolution %d failed!", r);
		
		if (restoreOnFailure)
		{
			staterr("Trying to recover old mode %d.", r, old_res);
			if (Graphics::SetResolution(old_res, false))
			{
				staterr("Fatal error: vidmode recovery failed!!!");
			}
		}
		
		return 1;
	}
	
	if (Graphics::FlushAll()) return 1;
	return 0;
}

// return a pointer to a null-terminated list of available resolutions.
const char **Graphics::GetResolutions()
{
static const char *res_str[]   =
{
	"Fullscreen",
	"320x240", "640x480", "960x720",
	NULL
};

	return res_str;
}

/*
void c------------------------------() {}
*/

// blit from one surface to another, just like SDL_BlitSurface.
void Graphics::BlitSurface(NXSurface *src, NXRect *srcrect, NXSurface *dst, NXRect *dstrect)
{
	dst->DrawSurface(src, dstrect->x, dstrect->y, \
					 srcrect->x, srcrect->y, srcrect->w, srcrect->h);
}

/*
void c------------------------------() {}
*/

// draw the entire surface to the screen at the given coordinates.
void Graphics::DrawSurface(NXSurface *src, int x, int y)
{
	drawtarget->DrawSurface(src, x, y);
}


// blit the specified portion of the surface to the screen
void Graphics::DrawSurface(NXSurface *src, \
						   int dstx, int dsty, int srcx, int srcy, int wd, int ht)
{
	drawtarget->DrawSurface(src, dstx, dsty, srcx, srcy, wd, ht);
}


// blit the specified surface across the screen in a repeating pattern
void Graphics::BlitPatternAcross(NXSurface *sfc, int x_dst, int y_dst, int y_src, int height)
{
	drawtarget->BlitPatternAcross(sfc, x_dst, y_dst, y_src, height);
}

/*
void c------------------------------() {}
*/

void Graphics::DrawRect(int x1, int y1, int x2, int y2, NXColor color)
{
	drawtarget->DrawRect(x1, y1, x2, y2, color);
}

void Graphics::FillRect(int x1, int y1, int x2, int y2, NXColor color)
{
	drawtarget->FillRect(x1, y1, x2, y2, color);
}

void Graphics::DrawPixel(int x, int y, NXColor color)
{
	drawtarget->DrawPixel(x, y, color);
}

/*
void c------------------------------() {}
*/

void Graphics::DrawRect(int x1, int y1, int x2, int y2, uint8_t r, uint8_t g, uint8_t b)
{
	drawtarget->DrawRect(x1, y1, x2, y2, r, g, b);
}

void Graphics::FillRect(int x1, int y1, int x2, int y2, uint8_t r, uint8_t g, uint8_t b)
{
	drawtarget->FillRect(x1, y1, x2, y2, r, g, b);
}

void Graphics::DrawPixel(int x, int y, uint8_t r, uint8_t g, uint8_t b)
{
	drawtarget->DrawPixel(x, y, r, g, b);
}

/*
void c------------------------------() {}
*/

void Graphics::DrawLine(int x1, int y1, int x2, int y2, uint8_t r, uint8_t g, uint8_t b)
{
int i;               // loop counter
int ystep, xstep;    // the step on y and x axis
int error;           // the error accumulated during the increment
int errorprev;       // *vision the previous value of the error variable
int y = y1, x = x1;  // the line points
int ddy, ddx;        // compulsory variables: the double values of dy and dx
int dx = x2 - x1;
int dy = y2 - y1;
	
	#define POINT(y,x) { DrawPixel(x, y, r, g, b); }
	
	POINT(y1, x1);      // first point
	
	// NB the last point can't be here, because of its previous point (which has to be verified)
	if (dy < 0)
	{
		ystep = -1;
		dy = -dy;
	}
	else
	{
		ystep = 1;
	}
	
	if (dx < 0)
	{
		xstep = -1;
		dx = -dx;
	}
	else
	{
		xstep = 1;
	}
	
	ddy = 2 * dy;       // work with double values for full precision
	ddx = 2 * dx;
  
	if (ddx >= ddy)
	{
		// first octant (0 <= slope <= 1)
		// compulsory initialization (even for errorprev, needed when dx==dy)
		errorprev = error = dx;  // start in the middle of the square
		
		for (i=0 ; i < dx ; i++)
		{  // do not use the first point (already done)
			x += xstep;
			error += ddy;
			if (error > ddx)
			{  // increment y if AFTER the middle ( > )
				y += ystep;
				error -= ddx;
				// three cases (octant == right->right-top for directions below):
				if (error + errorprev < ddx)  // bottom square also
				{
					POINT (y-ystep, x);
				}
				else if (error + errorprev > ddx)  // left square also
				{
					POINT (y, x-xstep);
				}
				else
				{  // corner: bottom and left squares also
					POINT (y-ystep, x);
					POINT (y, x-xstep);
				}
			}
			
			POINT (y, x);
			errorprev = error;
		}
	}
	else
	{  // the same as above
		errorprev = error = dy;
		for (i=0 ; i < dy ; i++)
		{
			y += ystep;
			error += ddx;
			if (error > ddy)
			{
				x += xstep;
				error -= ddy;
				if (error + errorprev < ddy)
				{
					POINT (y, x-xstep);
				}
				else if (error + errorprev > ddy)
				{
					POINT (y-ystep, x);
				}
				else
				{
					POINT (y, x-xstep);
					POINT (y-ystep, x);
				}
			}
			
			POINT (y, x);
			errorprev = error;
		}
	}
}

/*
void c------------------------------() {}
*/

void Graphics::set_clip_rect(int x, int y, int w, int h)
{
	drawtarget->set_clip_rect(x, y, w, h);
}

void Graphics::set_clip_rect(NXRect *rect)
{
	drawtarget->set_clip_rect(rect);
}

void Graphics::clear_clip_rect()
{
	drawtarget->clear_clip_rect();
}

/*
void c------------------------------() {}
*/

// change the target surface of operation like DrawRect to something
// other than the screen.
void Graphics::SetDrawTarget(NXSurface *surface)
{
	drawtarget = surface;
}








