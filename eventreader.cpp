
#include "ika.h"
#include "event.h"
#include "eventreader.h"
#include "eventreader.fdh"

static struct
{
	char *data;
	int pos;
	int len;
	int saved_pos;
} file;

// used to match "bracketed" opcodes after a '<' is seen
static struct {
	const char *name;
	int op;
}
optable[] = {
	{ "pc", OP_PC },
	{ "end", OP_END },
	{ "se", OP_SE },
	{ "wa", OP_WA },
	{ "yn", OP_YN },

	{ "f+", OP_FPLUS },
	{ "fj", OP_FJ },
	{ "ij", OP_IJ },
	{ "e+", OP_EPLUS },

	{ "bo", OP_BO },
	{ "ce", OP_CE },
	{ "cm", OP_CM },
	{ "dl", OP_DL },
	{ "ds", OP_DS },
	{ "ec", OP_EC },
	{ "ej", OP_EJ },
	{ "ek", OP_EK },
	{ "ex", OP_EX },
	{ "fi", OP_FI },
	{ "fo", OP_FO },
	{ "ft", OP_FT },
	{ "ge", OP_GE },
	{ "i-", OP_IMINUS },
	{ "i+", OP_IPLUS },
	{ "le", OP_LE },

	{ "ma", OP_MA },
	{ "nm", OP_NM },

	{ "pd", OP_PD },
	{ "pf", OP_PF },
	{ "ph", OP_PH },
	{ "pn", OP_PN },
	{ "pp", OP_PP },
	{ "ps", OP_PS },

	{ "re", OP_RE },
	{ "*", OP_CRE },

	{ "s", OP_END },			// Japanese version of staff.ptx ends with "<s"
	{ "<<", OP_BRBR },			// hack around translator mistake in staff.ptx
	
	{ nullptr, -1 }
};


bool event_compile(const char *fname, Script *script)
{
FILE *fp;
DBuffer *program = &script->program;
ScriptCommand cmd;
int epcount = 0;

	fp = fopen(fname, "rb");
	if (!fp)
	{
		staterr("failed to open '%s'", fname);
		return 1;
	}
	
	file.len = filesize(fp);
	file.pos = 0;
	file.data = (char *)malloc(file.len);
	
	fread(file.data, file.len, 1, fp);
	fclose(fp);
	
	stat("parsing %s...", fname);
	bool in_text = false;
	int textlen_index = 0;
	
	while(file.pos < file.len)
	{
		char ch = get();
		
		switch(ch)
		{
			case '\r': case '\n': continue;
			
			case '+': AddCommand(program, OP_NEWLINE); in_text = false; break;
			case '/': AddCommand(program, OP_NOD); in_text = false; break;
			
			case '#':
			{
				epcount++;
				if (ReadEventDefinition(script))
				{
					free(file.data);
					return 1;
				}
				
				in_text = false;
			}
			break;
			
			case '<':
			{
				if (ReadCommand(&cmd))
				{
					free(file.data);
					return 1;
				}
				
				// hack to work around translator mishap
				if (cmd.op == OP_BRBR)
				{
					textlen_index = (program->Length() + 2);
					in_text = true;
					
					cmd.op = OP_TEXT;
					cmd.nparams = 1;
					cmd.p1 = 3;
					AddCommand(program, &cmd);
					
					program->Append8('<');
					program->Append8('<');
					program->Append8('<');
				}
				else
				{
					AddCommand(program, &cmd);
					in_text = false;
				}
			}
			break;
			
			default:
			{
				if (!in_text)
				{
					// get pointer to parm0--text length
					textlen_index = (program->Length() + 2);
					
					cmd.op = OP_TEXT;
					cmd.nparams = 1;
					cmd.parm[0] = 1;		// text length
					AddCommand(program, &cmd);
				}
				else
				{	// increment text length
					uint16_t *textlen = (uint16_t *)(program->Data() + textlen_index);
					(*textlen)++;
				}
				
				program->Append8(ch);
				in_text = true;
			}
			break;
		}
	}
	
	stat("%s: %d events available, %d total bytes", fname, epcount, program->Length());
	
	AddCommand(program, OP_END);
	free(file.data);
	return 0;
}

static bool ReadEventDefinition(Script *script)
{
int eventno;
DString epname;

	if (get() != 'e')
	{
		staterr("expected 'e' after '#'");
		return 1;
	}
	
	eventno = ReadNumber();
	if (eventno >= MAX_EVENTS)
	{
		staterr("invalid event number %d: maximum event # is %d", eventno, MAX_EVENTS - 1);
		return 1;
	}
	
	// read the description while skipping to the end of the line
	while(file.pos < file.len)
	{
		char ch = get();
		switch(ch)
		{
			case '\r': case '\n':
			{
				//stat("read entrypoint %d, position $%04x = '%s'", eventno, script->program.Length(), epname.String());
				script->entrypoints[eventno] = script->program.Length();
				script->epnames[eventno] = strdup(epname.String());
				return 0;
			}
			break;
			
			case '-': break;
			
			default:
			{
				epname.AppendChar(ch);
			}
			break;
		}
	}
	
	staterr("unexpected end of file reading event definition %04d", eventno);
	return 1;
}

/*
void c------------------------------() {}
*/

static void AddCommand(DBuffer *program, int opcode)
{
ScriptCommand cmd;

	cmd.op = opcode;
	cmd.nparams = 0;
	AddCommand(program, &cmd);
}

static void AddCommand(DBuffer *program, ScriptCommand *cmd)
{
	program->Append8(cmd->op);
	program->Append8(cmd->nparams);
	
	for(int i=0;i<cmd->nparams;i++)
		program->Append16(cmd->parm[i]);
}

/*
void c------------------------------() {}
*/

// identify and read in a bracketed ('<'-prefixed) command
static bool ReadCommand(ScriptCommand *cmd)
{
const char *cmdname = &file.data[file.pos];
int i;

	for(i=0;;i++)
	{
		if (!optable[i].name)
		{
			char buf[3] = { 0 };
			buf[0] = get();
			buf[1] = get();
			
			staterr("could not identify script instruction: '<%s'", buf);
			return 1;
		}
		
		if (strbegin(cmdname, optable[i].name))
		{
			file.pos += strlen(optable[i].name);
			break;
		}
	}
	
	cmd->op = optable[i].op;
	cmd->nparams = 0;
	
	// read in any parameters
	if (isdigit(peek()))
	{
		for(i=0;i<MAX_PARAMS;i++)
		{
			cmd->parm[i] = ReadNumber();
			cmd->nparams++;
			
			if (get() != '-')
			{
				unget();
				break;
			}
		}
	}
	
	return 0;
}

static int ReadNumber(void)
{
	int num = atoi(&file.data[file.pos]);
	while(isdigit(get())) ;
	unget();
	
	if (num < 0 || num > 9999)
	{
		staterr("expected 4-digit positive value here, got '%d'", num);
		num = 0;
	}
	
	return num;
}

/*
void c------------------------------() {}
*/

const char *GetOpName(int opcode)
{
static char buffer[1024];

	if (opcode == OP_TEXT) return "TEXT";
	if (opcode == OP_NEWLINE) return "NEWLINE";
	if (opcode == OP_NOD) return "NOD";
	
	for(int i=0;optable[i].name;i++)
	{
		if (optable[i].op == opcode)
			return optable[i].name;
	}
	
	sprintf(buffer, "<Unknown Opcode %02x>", opcode);
	return buffer;
}

/*
void c------------------------------() {}
*/

static char get()
{
	if (file.pos >= file.len)
		return 0;
	
	return file.data[file.pos++];
}

static void unget()
{
	if (file.pos > 0)
		file.pos--;
}

static char peek()
{
	if (file.pos >= file.len)
		return 0;
	
	return file.data[file.pos];
}

static void push_pos()
{
	file.saved_pos = file.pos;
}

static void pop_pos()
{
	file.pos = file.saved_pos;
}

/*
void c------------------------------() {}
*/

void Script::init()
{
	program.Clear();
	for(int i=0;i<MAX_EVENTS;i++)
		epnames[i] = NULL;
}

void Script::freedata()
{
	program.Clear();
	
	for(int i=0;i<MAX_EVENTS;i++)
	{
		if (epnames[i])
		{
			free(epnames[i]);
			epnames[i] = NULL;
		}
	}
}




