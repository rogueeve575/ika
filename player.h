
#ifndef _PLAYER_H
#define _PLAYER_H

#define PLAYER_WIDTH	TILE_W
#define PLAYER_HEIGHT	TILE_H

// settings for player's blockflag
#define BLOCK_LEFT		0x01
#define BLOCK_ABOVE		0x02
#define BLOCK_RIGHT		0x04
#define BLOCK_BELOW		0x08

#define INDEX_UL		0
#define INDEX_UR		1
#define INDEX_LL		2
#define INDEX_LR		3

#define NUM_INVENTORY_SLOTS		10

struct Player
{
	bool enabled;
	bool died;
	
	uint8_t controlmode;
	uint8_t equipmask;
	uint16_t itemmask;
	
	int x, y;
	int xinertia, yinertia;
	int frame, sprite;
	
	uint16_t thrustanimtimer;
	uint16_t hurtflashtime;
	uint16_t canattacktime;
	
	int16_t hp;
	int16_t xp;
	uint8_t level;
	
	uint16_t canthrusttime;
	uint16_t captimer;
	
	uint8_t dir;
	uint8_t inair;				// if 1, gets less friction
	uint8_t blockflag;
	
	uint8_t inventory[NUM_INVENTORY_SLOTS];
	
// ---------------------------------------
	
	int Width();
	int Height();
	
	int CenterX();
	int CenterY();
	
	int Left();
	int Right();
	int Top();
	int Bottom();
};

extern Player *player;

#define EQUIP_HARDHAT		0x01
#define EQUIP_CAPACITOR		0x02
#define EQUIP_KANI_SPRAY	0x04	// What is this??
#define EQUIP_SHIP			0x08

enum
{
	ITEM_NULL,
	ITEM_HARDHAT	= 1,
	ITEM_SUKA		= 2,
	ITEM_HERBAL		= 3,
	ITEM_CODE		= 4,
	ITEM_PEARL		= 5,
	ITEM_SHRIMP		= 6,
	ITEM_CRABSOUP	= 7,
	ITEM_CAPACITOR	= 8,
	ITEM_GLOBEPLATR	= 9,
	ITEM_BEN		= 10,
	ITEM_SHIP		= 11,
	
	NUM_ITEMS
};

enum ControlModes
{
	CONTROL_NORMAL,
	CONTROL_CAPACITOR,
	CONTROL_ESCAPING,
	
	NUM_CONTROL_MODES
};

bool PHasItem(int item);


inline int Player::Width()			{ return (sprites[this->sprite].w << CSF); }
inline int Player::Height()			{ return (sprites[this->sprite].h << CSF); }

inline int Player::CenterX()		{ return (this->x + ((sprites[this->sprite].w << CSF) / 2)); }
inline int Player::CenterY()		{ return (this->y + ((sprites[this->sprite].h << CSF) / 2)); }

inline int Player::Left()			{ return (this->x + (sprites[this->sprite].bbox.x1 << CSF)); }
inline int Player::Right()			{ return (this->x + (sprites[this->sprite].bbox.x2 << CSF)); }
inline int Player::Top()			{ return (this->y + (sprites[this->sprite].bbox.y1 << CSF)); }
inline int Player::Bottom()			{ return (this->y + (sprites[this->sprite].bbox.y2 << CSF)); }


void InitPlayer();
void PRunCurrentControlMode();
void PRunObjectInteraction(void);
void PRunMapInteraction(void);
void handle_item(int xtile, int ytile);
void hurtplayer(int amount);
void PGiveHP(int amt);
void MapMover(void);
bool PHasItem(int item);
void PGiveItem(int item);
void PTakeItem(int item);
void PTakeAllItems(void);
void PUpdateEquip();
void PDrawStatusBar();
void DrawNumber(int x, int y, uint16_t num, int s);
void PDrawPlayer();

#endif
