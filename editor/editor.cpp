
#include "../ika.h"
#include "editor.h"
#include "editor.fdh"

Editor editor;

const int valid_tiles[] =
{
	0x86, 0x87, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
	0x20, 0x21, 0x22,
	0xE0,
	0x40,
	0x00,
	0x23,
	0x60, 0x61, 0x62, 0x63,
	0xA0, 0xA1,
	0xff
};

static const int default_ty[] =
{
	TY_ENEMY, TY_SOLIDEVENT,
	TY_ENEMY, TY_EVENT,
	TY_EVENT, TY_ENEMY,
	TY_EVENT, TY_SOLIDEVENT,
	TY_ENEMY, TY_EVENT
};


bool Editor::Init(void)
{
int i;

	//map.xscroll = 0xCA000;
	//map.yscroll = 0x162000;
	map_centeron(player->CenterX(), player->CenterY());
	
	// create dropdowns
	fTileMenu.Init(SCREEN_WIDTH - 8, 8);
	{
		for(i=0;valid_tiles[i]!=0xff;i++)
			fTileMenu.AddItem(valid_tiles[i], false);
	}
	fTileMenu.AddFinished();
	
	fSpriteMenu.Init(SCREEN_WIDTH - 8, 40);
	{
		for(i=0;i<NUM_OBJECT_TYPES;i++)
			fSpriteMenu.AddItem(i, true);
	}
	fSpriteMenu.AddFinished();
	
	// init modes
	fCurMode = NULL;
	SetMode(&fTileMode);
	
	Object *o;
	FOREACH_OBJECT(o)
		ai_init_frame(o);
	
	// misc
	textbox.SetVisible(false);
	fGameRunning = false;
	fSettingMapSize = false;
	music(-1, false);
	return 0;
}

void Editor::Close()
{
	fTileMenu.Empty();
	fSpriteMenu.Empty();
	
	fCurMode->OnExit();
}

/*
void c------------------------------() {}
*/

void Editor::Tick(void)
{
	if (fGameRunning)
	{
		RunGame();
		return;
	}
	
	KeyInput();
	MouseInput();
	
	if (fTileMenu.fHasNewSelection)
	{
		fTileMenu.fHasNewSelection = false;
		SetMode(&fTileMode);
	}
	
	fCurMode->Run();
	textbox.Run();
	
	Draw();
}


void Editor::Draw(void)
{
int x, y;

	Tileset::move_bubbles();
	map_draw_backdrop();
	map_draw(0);
	
	// draw objects
	Object *o;
	FOREACH_OBJECT(o)
	{
		o->Draw();
		
		// make invisible triggers visible
		if (o->type == OBJ_SIGN && o->frame == 0)
		{
			Rect rect;
			GetScreenCoords(o, &rect.x1, &rect.y1);
			rect.x2 = rect.x1 + 15;
			rect.y2 = rect.y1 + 15;
			
			DrawRect(rect.x1, rect.y1, rect.x2, rect.y2, 255, 255, 255);
		}
	}
	
	// draw player
	GetScreenCoords(player->x, player->y, SPR_MYCHAR2, 0, UP, &x, &y);
	draw_sprite(x, y, SPR_MYCHAR2, 0, UP);
	
	map_draw(TA_FOREGROUND);
	
	fCurMode->Draw();
	
	fSpriteMenu.Draw();
	fTileMenu.Draw();
	textbox.Draw();
	
	// just "refresh" the console display so it doesn't go away
	if (fSettingMapSize)
	{
		if (--fMapSizeTimer < 0)
		{
			console.Print("Setting map size: click to set or M to cancel");
			fMapSizeTimer = 40;
		}
	}
}

/*
void c------------------------------() {}
*/

void Editor::KeyInput(void)
{
int x, y;

	if (textbox.IsVisible()) return;
	
	int speed = (inputs & SHIFTKEY) ? 0x4000 : 0x2000;
	if (inputs & LEFTKEY)	map.xscroll -= speed;
	if (inputs & RIGHTKEY)	map.xscroll += speed;
	
	if (inputs & UPKEY)		map.yscroll -= speed;
	if (inputs & DOWNKEY)	map.yscroll += speed;
	
	// disable trimming scroll to map size while changing size, so map can be enlarged
	if (fSettingMapSize)
	{
		#define MAXIMUM_X	 (((MAP_MAXSIZEX * TILE_W) - SCREEN_WIDTH) << CSF)
		#define MAXIMUM_Y	 (((MAP_MAXSIZEY * TILE_H) - SCREEN_HEIGHT) << CSF)
		
		if (map.xscroll < 0) map.xscroll = 0;
		if (map.yscroll < 0) map.yscroll = 0;
		if (map.xscroll > MAXIMUM_X) map.xscroll = MAXIMUM_X;
		if (map.yscroll > MAXIMUM_Y) map.yscroll = MAXIMUM_Y;
		
		inputs &= (LEFTKEY | RIGHTKEY | UPKEY | DOWNKEY | MKEY | ESCKEY);
		newinputs &= (LEFTKEY | RIGHTKEY | UPKEY | DOWNKEY | MKEY | ESCKEY);
	}
	else
	{
		map_sanitycheck();
	}
	
	// tile pickup
	if (newinputs & TABKEY)
	{
		editor.fTileMenu.SetSelectedItem(map.tiles[mouse.xtile][mouse.ytile]);
		SetMode(&fTileMode);
		sound(SND_MESSAGE);
	}
	
	// set player
	if (newinputs & PKEY)
	{
		player->x = MAPX(mouse.xtile);
		player->y = MAPY(mouse.ytile);
	}
	
	// start game
	if (newinputs & SPACEKEY)
	{
		StartGame();
		return;
	}
	
	// add object / save
	if (newinputs & SKEY)
	{
		if (inputs & SHIFTKEY)
		{
			Save();
		}
		else
		{
			AddObject();
		}
	}
	
	// delete object
	if (newinputs & DELKEY)
	{
		int object = FindObjectUnderMouse();
		
		if (object != -1)
		{
			objects[object].exists = false;
		}
		else
		{
			map.tiles[mouse.xtile][mouse.ytile] = 0;
		}
	}
	
	// new map
	if (newinputs & NKEY)
	{
		textbox.EditorPrompt("Create a new map?", NO, Editor::NewMap);
	}
	
	// set map size
	if (newinputs & MKEY)
	{
		fSettingMapSize ^= 1;
		
		if (fSettingMapSize)
		{
			fMapSizeTimer = 0;
			
			// clear out unused areas
			for(y=0;y<MAP_MAXSIZEY;y++)
			for(x=map.xsize;x<MAP_MAXSIZEX;x++)
				map.tiles[x][y] = 0;
			
			for(y=map.ysize;y<MAP_MAXSIZEY;y++)
			for(x=0;x<MAP_MAXSIZEX;x++)
				map.tiles[x][y] = 0;
		}
		else
		{
			console.Print("");
		}
	}
}

/*
void c------------------------------() {}
*/

void Editor::MouseInput()
{
int max_x, max_y;

	// mouse can be "moved" by changing the scrolling without moving the cursor
	int ox = mouse.xtile;
	int oy = mouse.ytile;
	mouse.xtile = (((mouse.x << CSF) + map.xscroll) >> CSF) / TILE_W;
	mouse.ytile = (((mouse.y << CSF) + map.yscroll) >> CSF) / TILE_H;
	if (mouse.xtile < 0) mouse.xtile = 0;
	if (mouse.ytile < 0) mouse.ytile = 0;
	
	if (fSettingMapSize)
	{
		if (mouse.xtile >= MAP_MAXSIZEX) mouse.xtile = (MAP_MAXSIZEX - 1);
		if (mouse.ytile >= MAP_MAXSIZEY) mouse.ytile = (MAP_MAXSIZEY - 1);
	}
	else
	{
		if (mouse.xtile >= map.xsize) mouse.xtile = (map.xsize - 1);
		if (mouse.ytile >= map.ysize) mouse.ytile = (map.ysize - 1);
	}
	
	mouse.moved |= ((ox != mouse.xtile) || (oy != mouse.ytile));
	
	if (mouse.moved)
	{
		MouseMoved();
		mouse.moved = false;
	}
	
	if (mouse.btndown)
	{
		MouseDown();
		mouse.btndown = false;
	}
	
	if (mouse.btnup)
	{
		MouseUp();
		mouse.btnup = false;
	}
	
	if (fTileMenu.fExpanded)
		fSpriteMenu.fExpanded = false;
}

void Editor::MouseMoved()
{
	fTileMenu.MouseMoved();
	
	if (!fTileMenu.fExpanded)
		fSpriteMenu.MouseMoved();
	
	fCurMode->MouseMoved();
}

void Editor::MouseDown()
{
	fSpriteMode.EndNumberEdit();
	
	// final click to change map size
	if (fSettingMapSize)
	{
		int xt = (mouse.xtile + 1);
		int yt = (mouse.ytile + 1);
		if (xt <= (SCREEN_WIDTH / TILE_W)) xt = (SCREEN_WIDTH / TILE_W) + 1;
		if (yt <= (SCREEN_HEIGHT / TILE_H)) yt = (SCREEN_HEIGHT / TILE_H) + 1;
		
		map_resize(xt, yt);
		
		fSettingMapSize = false;
		map_sanitycheck();
		console.Print("");
		
		KillObjectsOutsideOfMap();
		mouse.lbutton = false;
		mouse.rbutton = false;
		return;
	}
	
	fTileMenu.MouseDown();
	
	if (!fTileMenu.fExpanded)
		fSpriteMenu.MouseDown();
	
	// menus may have absorbed the click...
	if (mouse.lbutton || mouse.rbutton)
	{
		// selecting an object (entry into object mode)
		if (mouse.lbutton || fCurMode != &fSpriteMode)
		{
			int o = Editor::FindObjectUnderMouse();
			if (o != -1)
			{
				editor.SetMode(&editor.fSpriteMode);
				editor.fSpriteMode.fObjectIndex = o;
				fSpriteMenu.SetSelectedItem(objects[o].type);
				sound(SND_MESSAGE);
				return;
			}
		}
		
		fCurMode->MouseDown();
	}
}

void Editor::MouseUp()
{
	fCurMode->MouseUp();
}

/*
void c------------------------------() {}
*/

int Editor::FindObjectUnderMouse()
{
	Object *o;
	Rect rect;
	
	FOREACH_OBJECT(o)
	{
		GetScreenCoords(o, &rect.x1, &rect.y1);
		rect.x2 = rect.x1 + (sprites[objprop[o->type].sprite].w - 1);
		rect.y2 = rect.y1 + (sprites[objprop[o->type].sprite].h - 1);
		
		if (MouseInRect(&rect))
			return ITERATOR;
	}
	
	return -1;
}

int Editor::FindObjectX2UnderMouse()
{
	Object *o;
	Rect rect;
	
	FOREACH_OBJECT(o)
	{
		rect.x1 = (o->x2 >> CSF) - (map.xscroll >> CSF);
		rect.y1 = (o->y2 >> CSF) - (map.yscroll >> CSF);
		rect.x2 = rect.x1 + 15;
		rect.y2 = rect.y1 + 15;
		
		if (MouseInRect(&rect))
			return ITERATOR;
	}
	
	return -1;
}

void Editor::SetMode(EditorMode *mode)
{
	if (fCurMode != mode)
	{
		if (fCurMode) fCurMode->OnExit();
		fCurMode = mode;
		if (mode) mode->OnEntry();
	}
}

void Editor::AddObject()
{
	int type = editor.fSpriteMenu.GetSelectedItem();
	Object *o = CreateObject(MAPX(mouse.xtile), MAPY(mouse.ytile), type);
	if (o != &dummy_object)
	{
		o->x2 = o->x;
		o->y2 = o->y;
		o->ty = default_ty[o->type];
		
		SetMode(&fSpriteMode);
		fSpriteMode.fObjectIndex = GetObjectIndex(o);
	}
	else
	{	// object list full
		sound(SND_NODMG);
		console.Print("Out of object slots.");
	}
}

void Editor::KillObjectsOutsideOfMap()
{
	Object *o;
	FOREACH_OBJECT(o)
	{
		if (o->x < 0 || o->y < 0 || \
			o->x >= MAPX(map.xsize) || \
			o->y >= MAPY(map.ysize))
		{
			o->exists = false;
		}
	}
}

/*
void c------------------------------() {}
*/

void Editor::NewMap()
{
int x, y, i;

	for(y=0;y<map.ysize;y++)
	for(x=0;x<map.xsize;x++)
		map.tiles[x][y] = 0;
	
	for(i=0;i<MAX_OBJECTS;i++)
		objects[i].exists = false;
}

void Editor::Save()
{
	if (objects_save())
	{
		console.Print("Object save failure");
		sound(SND_NODMG);
		return;
	}
	
	if (map_save())
	{
		console.Print("Map save failure");
		sound(SND_NODMG);
		return;
	}
	
	sound(SND_HITHEAD);
	console.Print("Map saved.");
}

/*
void c------------------------------() {}
*/

// convert CSF'd coords to screen coords
void GetScreenCoords(int x, int y, int sprite, int frame, int dir, int *scrxout, int *scryout)
{
	*scrxout = (x >> CSF) - (map.xscroll >> CSF);
	*scryout = (y >> CSF) - (map.yscroll >> CSF);
	*scrxout -= sprites[sprite].frame[frame].dir[dir].drawpoint.x;
	*scryout -= sprites[sprite].frame[frame].dir[dir].drawpoint.y;
}

void GetScreenCoords(Object *o, int *scrxout, int *scryout)
{
	GetScreenCoords(o->x, o->y, objprop[o->type].sprite, o->frame, o->dir, scrxout, scryout);
}

/*
void c------------------------------() {}
*/

void Editor::StartGame()
{
	// reload scripts, in case they were editing them externally
	script_close();
	if (script_init())
	{
		console.Print("Scripting error.");
		return;
	}
	
	newinputs = inputs = 0;
	fGameRunning = true;
	
	game.hidescene = false;
	game.entcount = 0;
	map.focustype = FOCUS_PLAYER;
	memset(game.flagdata, 0, sizeof(game.flagdata));
	
	start_effect(EFFECT_NONE);
	
	PGiveHP(9999);	// full hp
	PUpdateEquip();
	player->enabled = true;
	player->hurtflashtime = 40;
	player->canattacktime = 0;
	player->xinertia = player->yinertia = 0;
	
	Object *o;
	FOREACH_OBJECT(o)
	{
		o->state = 0;
		o->xinertia = 0;
		o->yinertia = 0;
	}
	
	fGameState.TakeSnapshot();
	music(0, false);
}

void Editor::RunGame()
{
	if (newinputs & SPACEKEY)
	{
		StopGame();
		return;
	}
	
	normal_tick();
}

void Editor::StopGame()
{
	StopScript();
	DeleteCarets();
	textbox.SetVisible(false);
	itembox.SetVisible(false);
	fCurMode = &fTileMode;
	
	fGameRunning = false;
	fGameState.Restore();
	music(-1, false);
}

/*
void c------------------------------() {}
*/

void GameState::TakeSnapshot()
{
	memcpy(fTiles, map.tiles, sizeof(fTiles));
	
	for(int i=0;i<MAX_OBJECTS;i++)
		fObjects[i] = objects[i];
}

void GameState::Restore()
{
	memcpy(map.tiles, fTiles, sizeof(fTiles));
	
	for(int i=0;i<MAX_OBJECTS;i++)
		objects[i] = fObjects[i];
}


/*
void c------------------------------() {}
*/

bool editor_init(void)
{
	return editor.Init();
}

void editor_close()
{
	editor.Close();
}

void editor_tick()
{
	editor.Tick();
}




