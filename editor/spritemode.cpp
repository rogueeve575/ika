
#include "../ika.h"
#include "../eventreader.h"
#include "editor.h"
#include "spritemode.fdh"

static const char *ty_desc[] =
{
	"EVENT: runs event, not solid",
	"NULL: no interaction",
	"ENEMY: damaging, runs event on death",
	"BLOCK: runs event, solid"
};

void Spritemode::OnEntry()
{
	// fObjectIndex should be set externally after entry
	fObjectIndex = -1;
	fEditTarget = NULL;
}

void Spritemode::OnExit()
{
}

/*
void c------------------------------() {}
*/

void Spritemode::Run()
{
	fObject = &objects[fObjectIndex];

	if (fObjectIndex == -1 || !fObject->exists)
	{
		editor.SetMode(&editor.fTileMode);
		return;
	}
	
	RunNumberEdit();
}


void Spritemode::Draw()
{
int x, y;

	// draw selector and X2
	DrawSelector(fObject->x, fObject->y, 1);
	DrawSelector(fObject->x2, fObject->y2, 2);

	// draw panel
	draw_sprite(8, 8, SPR_PANEL);
	DrawPanelNumber(0, 0, fObjectIndex);
	DrawPanelNumber(0, 1, fObject->type);
	DrawPanelNumber(1, 0, (fObject->x >> CSF) / TILE_W);
	DrawPanelNumber(1, 1, (fObject->y >> CSF) / TILE_H);
	DrawPanelNumber(2, 0, CountUnusedObjects());
	DrawPanelNumber(2, 1, fObject->event);
	DrawNumberEdit();
	
	GetPanelXY(3, 0, &x, &y);
	draw_sprite(x-19, y, SPR_NPCTYPE, fObject->ty);
}


/*
void c------------------------------() {}
*/

void Spritemode::MouseDown()
{
int xa, ya;
int x, y;

	if (!GetPanelUnderMouse(&xa, &ya))
	{
		if (mouse.rbutton)
		{
			fObject->x2 = MAPX(mouse.xtile);
			fObject->y2 = MAPX(mouse.ytile);
			ai_init_frame(fObject);
		}
		else
		{
			editor.SetMode(&editor.fTileMode);
		}
		
		return;
	}
	
	int dir = mouse.rbutton ? -1 : 1;
	
	switch(xa)
	{
		case 0:
		{
			if (ya == 0)	// objindex
			{
				for(;;)
				{
					fObjectIndex += dir;
					if (fObjectIndex < 0) fObjectIndex = MAX_OBJECTS-1;
					if (fObjectIndex >= MAX_OBJECTS) fObjectIndex = 0;
					
					fObject = &objects[fObjectIndex];
					if (fObject->exists) break;
				}
			}
			else			// type
			{
				fObject->type += dir;
				Wrap(&fObject->type, 0, NUM_OBJECT_TYPES-1);
			}
		}
		break;
		
		case 1:
		{
			if (ya == 0)	// X
			{
				fObject->x += (TILE_W<<CSF) * dir;
				Wrap(&fObject->x, 0, MAPX(map.xsize));
			}
			else			// Y
			{
				fObject->y += (TILE_H<<CSF) * dir;
				Wrap(&fObject->y, 0, MAPY(map.ysize));
			}
		}
		break;
		
		case 2:
		{
			if (ya == 1)	// event
			{
				//fObject->event += dir;
				//Wrap(&fObject->event, 0, MAX_EVENTS-1);
				GetPanelXY(xa, ya, &x, &y);
				BeginNumberEdit(&fObject->event, x, y);
			}
		}
		break;
		
		case 3:
		{
			if (ya == 0)	// ty
			{
				fObject->ty += dir;
				Wrap(&fObject->ty, 0, NUM_TY-1);
				console.Print(ty_desc[fObject->ty]);
			}
		}
		break;
	}
	
	mouse.lbutton = false;
	mouse.rbutton = false;
}

/*
void c------------------------------() {}
*/

void Spritemode::BeginNumberEdit(uint16_t *target, int x, int y)
{
	fEditX = x+5;
	fEditY = y+1;
	fEditTarget = target;
	fEditPlace = 0;
	fEditFlash = true;
	
	last_sdl_key = -1;
}

void Spritemode::EndNumberEdit()
{
	fEditTarget = NULL;
}


void Spritemode::RunNumberEdit()
{
int key;

	if (!fEditTarget)
		return;
	
	if (last_sdl_key != -1)
	{
		key = last_sdl_key;
		last_sdl_key = -1;
	}
	else return;
	
	if (key >= SDLK_KP0 && key <= SDLK_KP9)
		key -= (SDLK_KP0 - SDLK_0);
	
	if (key == SDLK_RETURN || key == SDLK_KP_ENTER)
	{
		EndNumberEdit();
	}
	else if (key == SDLK_BACKSPACE)
	{
		*fEditTarget /= 10;
	}
	else if (key >= SDLK_0 && key <= SDLK_9)
	{
		if (*fEditTarget == 0 || *fEditTarget >= 1000)
		{
			*fEditTarget -= (*fEditTarget % 10);
			*fEditTarget += (key - SDLK_0);
		}
		else
		{
			*fEditTarget *= 10;
			*fEditTarget += (key - SDLK_0);
		}
	}
}


void Spritemode::DrawNumberEdit()
{
	if (fEditTarget)
	{
		if (fEditFlash)
		{
			int x = fEditX - (fEditPlace * sprites[SPR_FIGURE_SMALL].w);
			FillRect(x, fEditY, \
				x + (sprites[SPR_FIGURE_SMALL].w - 2), \
				fEditY + (sprites[SPR_FIGURE_SMALL].h - 2),
				255, 0, 0);
		}
		
		fEditFlash ^= 1;
	}
}

/*
void c------------------------------() {}
*/

void Spritemode::DrawSelector(int x, int y, int frame)
{
	x = (x - map.xscroll) >> CSF;
	y = (y - map.yscroll) >> CSF;
	draw_sprite(x, y, SPR_SELECTOR, frame);
}


void Spritemode::DrawPanelNumber(int xa, int ya, int num)
{
int x, y;

	GetPanelXY(xa, ya, &x, &y);
	DrawNumber(x, y, num, SPR_FIGURE_SMALL);
}

void Spritemode::GetPanelXY(int xa, int ya, int *xout, int *yout)
{
	*xout = 49 + (xa * 44);
	*yout = 16 + (ya * 12);
}

void Spritemode::GetPanelRect(int xa, int ya, Rect *out)
{
	GetPanelXY(xa, ya, &out->x2, &out->y1);
	
	out->x2 += 4;
	out->x1 = (out->x2 - 24);
	out->y2 = (out->y1 + 8);
}

bool Spritemode::GetPanelUnderMouse(int *xout, int *yout)
{
int xa, ya;
Rect rect;

	for(ya=0;ya<=1;ya++)
	for(xa=0;xa<=3;xa++)
	{
		GetPanelRect(xa, ya, &rect);
		if (MouseInRect(&rect))
		{
			*xout = xa;
			*yout = ya;
			return 1;
		}
	}
	
	return 0;
}

/*
void c------------------------------() {}
*/

void Spritemode::Wrap(int *value, int min, int max)
{
	if (*value < min) *value = max;
	if (*value > max) *value = 0;
}

void Spritemode::Wrap(uint8_t *value, int min, int max)
{
	if (*value == 255) *value = max;
	if (*value > max) *value = 0;
}

void Spritemode::Wrap(uint16_t *value, int min, int max)
{
	if (*value == 65535) *value = max;
	if (*value > max) *value = 0;
}


int Spritemode::CountUnusedObjects()
{
	int count = 0;
	for(int i=0;i<MAX_OBJECTS;i++)
	{
		if (!objects[i].exists)
			count++;
	}
	
	return count;
}


