
#include "../ika.h"
#include "dropdown.h"
#include "dropdown.fdh"


void Dropdown::Init(int right, int top)
{
	Empty();
	
	fWidth = fHeight = 0;
	fNextX = fNextY = 0;
	fRight = right;
	fTop = top;
	
	fExpanded = false;
	fDontExpand = false;
	fHasNewSelection = false;
	fSelectedItem = 0;
	fTimer = 0;
}

void Dropdown::Empty()
{
	for(auto *item : fItems) delete item;
	fItems.clear();
}

/*
void c------------------------------() {}
*/

void Dropdown::AddItem(int index, bool is_sprite)
{
DropdownItem *item = new DropdownItem;

	item->index = index;
	item->is_sprite = is_sprite;
	item->x = fNextX;
	item->y = fNextY;
	
	if (is_sprite)
	{
		fWidth = 64;
		fHeight = 144;
		item->sprite = objprop[index].sprite;
		
		item->w = sprites[item->sprite].w;
		item->h = sprites[item->sprite].h;
		item->wspac = 24;
		item->hspac = 24;
		
		fNextX += (item->wspac + 2);
		if (fNextX >= fWidth - 32)
		{
			fNextX = 0;
			fNextY += (item->hspac + 2);
		}
		
		if (index < 6)
			item->x += 8;
		else if (index < 9)
			item->x += 4;
		
		item->y += 2;
	}
	else
	{
		item->w = item->wspac = TILE_W;
		item->h = item->hspac = TILE_H;
		
		fWidth = (TILE_W * 8);
		fHeight = (fNextY + item->h);
		
		fNextX += (item->wspac + 2);
		if (fNextX >= fWidth - 8)
		{
			fNextX = 0;
			fNextY += (item->hspac + 2);
		}
	}
	
	fItems.push_back(item);
}

void Dropdown::AddFinished()
{
	fWidth += 2;
	fHeight += 5;
}


void Dropdown::Draw(void)
{
int x, y;
Rect rect;

	if (fExpanded)
	{
		x = (fRight - fWidth);
		y = fTop;
		
		FillRect(x, y, x+(fWidth-1), y+(fHeight-1), 0, 0, 0);
		DrawRect(x, y, x+(fWidth-1), y+(fHeight-1), 255, 0, 0);
		
		x += 2;
		y += 2;
		for(auto *item : fItems)
			DrawItem(x+item->x, y+item->y, item);
		
		// un-expanding
		rect.x1 = (fRight - fWidth);
		rect.y1 = fTop;
		rect.x2 = rect.x1 + (fWidth - 1);
		rect.y2 = rect.y1 + (fHeight - 1);
		
		if (!MouseInRect(&rect))
		{
			if (++fTimer > 16)
				fExpanded = false;
		}
		else
			fTimer = 0;
	}
	else
	{
		if (fSelectedItem < 0 && fSelectedItem >= fItems.size()) return;
		DropdownItem *item = fItems[fSelectedItem];
		
		if (!GetSelectedItemRect(&rect)) {
			FillRect(rect.x1, rect.y1, rect.x2, rect.y2, 0, 0, 0);
			DrawRect(rect.x1, rect.y1, rect.x2, rect.y2, 240, 24, 24);
		}
		
		DrawItem((fRight - item->w)-1, fTop+1, item);
	}
}

bool Dropdown::GetSelectedItemRect(Rect *rect)
{
	if (fSelectedItem < 0 && fSelectedItem >= fItems.size()) return 1;
	auto *item = fItems[fSelectedItem];
	
	int w = item->w;
	int h = item->h;
	if (w < 16) w = 16;
	if (h < 16) h = 16;
	
	rect->x1 = (fRight - w - 2);
	rect->y1 = fTop;
	rect->x2 = (rect->x1 + w + 1);
	rect->y2 = (rect->y1 + h + 2);
	return 0;
}

void Dropdown::DrawItem(int x, int y, DropdownItem *item)
{
	if (!item->is_sprite)
	{
		draw_tile(x, y, item->index);
	}
	else
	{
		int f = (item->sprite == SPR_SIGN) ? 1 : 0;
		draw_sprite(x, y, item->sprite, f);
	}
}

/*
void c------------------------------() {}
*/

int Dropdown::GetSelectedItem() {
	if (fSelectedItem < 0 && fSelectedItem >= fItems.size()) return -1;
	auto *item = fItems[fSelectedItem];
	return item->index;
}

bool Dropdown::SetSelectedItem(int index) {
	int i = 0;
	for(auto *item : fItems) {
		if (item->index == index) {
			fSelectedItem = i;
			return 0;
		}
		i++;
	}

	return 1;
}

/*
void c------------------------------() {}
*/

void Dropdown::MouseMoved()
{
Rect rect;

	if (!fExpanded)
	{
		if (!GetSelectedItemRect(&rect) && MouseInRect(&rect))
		{
			if (!fDontExpand)
			{
				fExpanded = true;
			}
		}
		else
		{
			fDontExpand = false;
		}
	}
}

void Dropdown::MouseDown()
{
Rect rect;

	if (!mouse.lbutton)
		return;
	
	if (!fExpanded)
	{
		if (!GetSelectedItemRect(&rect) && MouseInRect(&rect))
		{
			fExpanded = true;
			mouse.lbutton = false;
		}
		
		return;
	}
	
	// selecting item from expanded list
	int x = (fRight - fWidth) + 2;
	int y = fTop + 2;

	int i = 0;
	for(auto *item : fItems) {
		rect.x1 = x + item->x;
		rect.y1 = y + item->y;
		rect.x2 = rect.x1 + (item->wspac - 1);
		rect.y2 = rect.y1 + (item->hspac - 1);
		
		if (MouseInRect(&rect)) {
			fSelectedItem = i;
			fExpanded = false;
			fDontExpand = true;
			fHasNewSelection = true;
			sound(SND_MESSAGE);
			
			mouse.lbutton = false;
			return;
		}

		i++;
	}
}









