
#ifndef _TILEMODE_H
#define _TILEMODE_H


class Tilemode : public EditorMode
{
public:
	void OnEntry();
	void OnExit();
	
	void Run();
	void Draw();
	
	void MouseDown();
	void MouseMoved();
	void MouseUp();

private:
	void BeginRecting(int mode);
	void TriggerRecting();
	int SelectRandomTile();

	int fRecting;
	int fRectAnchorX, fRectAnchorY;
	int fTileToReplace;
	bool fReadyForReplace;
	
	bool fCanTriggerRectingOnMouseUp;
};

#endif
