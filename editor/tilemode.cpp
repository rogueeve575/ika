
#include "../ika.h"
#include "editor.h"
#include "tilemode.fdh"

enum
{
	R_NONE,
	R_BLOCKFILL,
	R_CHANGE,
	R_RANDOM
};

// ---------------------------------------


void Tilemode::OnEntry()
{
	fReadyForReplace = false;
	fCanTriggerRectingOnMouseUp = false;
}

void Tilemode::OnExit()
{
}

/*
void c------------------------------() {}
*/

void Tilemode::Run()
{
	if (newinputs & BKEY)
	{
		if (fRecting != R_BLOCKFILL)
		{
			console.Print("Block fill");
			BeginRecting(R_BLOCKFILL);
		}
		else
		{
			fRecting = 0;
			console.Print("");
		}
	}
	
	if (newinputs & CKEY)
	{
		if ((fRecting != R_CHANGE) && !fReadyForReplace)
		{
			console.Print("Block tile change - ready");
			fTileToReplace = map.tiles[mouse.xtile][mouse.ytile];
			fReadyForReplace = true;
		}
		else
		{
			fRecting = 0;
			fReadyForReplace = false;
			console.Print("");
		}
	}
	
	if (newinputs & RKEY)
	{
		if (fRecting != R_BLOCKFILL)
		{
			console.Print("Random fill");
			BeginRecting(R_RANDOM);
		}
		else
		{
			fRecting = 0;
			console.Print("");
		}
	}
	
}

void Tilemode::Draw()
{
int x, y;

	// tile selector
	x = (MAPX(mouse.xtile) - map.xscroll) >> CSF;
	y = (MAPX(mouse.ytile) - map.yscroll) >> CSF;
	
	draw_sprite(x, y, SPR_SELECTOR, 0);
	
	DrawNumber(x - 6, y-1, mouse.xtile, SPR_FIGURE_SMALL);
	DrawNumber(x - 6, y+7, mouse.ytile, SPR_FIGURE_SMALL);
	
	if (fRecting)
	{
		Rect rect;
		rect.x1 = mouse.xtile;
		rect.y1 = mouse.ytile;
		rect.x2 = fRectAnchorX;
		rect.y2 = fRectAnchorY;
		if (rect.x1 > rect.x2) SWAP(rect.x1, rect.x2);
		if (rect.y1 > rect.y2) SWAP(rect.y1, rect.y2);
		
		rect.x1 = (MAPX(rect.x1) - map.xscroll) >> CSF;
		rect.y1 = (MAPX(rect.y1) - map.yscroll) >> CSF;
		rect.x2 = ((MAPX(rect.x2) - map.xscroll) >> CSF) + 15;
		rect.y2 = ((MAPX(rect.y2) - map.yscroll) >> CSF) + 15;
		
		DrawRect(rect.x1, rect.y1, rect.x2, rect.y2, 255, 255, 0);
	}
}

/*
void c------------------------------() {}
*/

void Tilemode::MouseMoved()
{
	if (fRecting)
	{
		if (mouse.lbutton && \
			(mouse.xtile != fRectAnchorX || mouse.ytile != fRectAnchorY))
		{
			fCanTriggerRectingOnMouseUp = true;
		}
	}
	else
	{
		if (mouse.lbutton)
			map.tiles[mouse.xtile][mouse.ytile] = editor.fTileMenu.GetSelectedItem();
	}
}

void Tilemode::MouseDown()
{
	if (!mouse.lbutton && mouse.rbutton)
	{
		editor.AddObject();
		return;
	}
	
	// selecting start point of 'C' change-tile command
	if (fReadyForReplace)
	{
		fReadyForReplace = false;
		fRecting = R_CHANGE;
		fRectAnchorX = mouse.xtile;
		fRectAnchorY = mouse.ytile;
		return;
	}
	
	// recting command
	if (fRecting)
	{
		TriggerRecting();
		fRecting = 0;
	}
	else
	{
		// plot tiles (share code with drag)
		MouseMoved();
	}
}


void Tilemode::MouseUp()
{
	if (fRecting && fCanTriggerRectingOnMouseUp)
	{
		TriggerRecting();
		
		fCanTriggerRectingOnMouseUp = false;
		fRecting = 0;
	}
}

/*
void c------------------------------() {}
*/

void Tilemode::BeginRecting(int mode)
{
	fRecting = mode;
	fCanTriggerRectingOnMouseUp = false;
	fRectAnchorX = mouse.xtile;
	fRectAnchorY = mouse.ytile;
}

void Tilemode::TriggerRecting()
{
int x, y;

	Rect rect;
	rect.x1 = mouse.xtile;
	rect.y1 = mouse.ytile;
	rect.x2 = fRectAnchorX;
	rect.y2 = fRectAnchorY;
	if (rect.x1 > rect.x2) SWAP(rect.x1, rect.x2);
	if (rect.y1 > rect.y2) SWAP(rect.y1, rect.y2);
	
	int newtile = editor.fTileMenu.GetSelectedItem();
	sound(SND_GO);
	
	switch(fRecting)
	{
		case R_BLOCKFILL:
		{
			for(y=rect.y1;y<=rect.y2;y++)
			for(x=rect.x1;x<=rect.x2;x++)
			{
				map.tiles[x][y] = newtile;
			}
		}
		break;
		
		case R_CHANGE:
		{
			int count = 0;
			
			for(y=rect.y1;y<=rect.y2;y++)
			for(x=rect.x1;x<=rect.x2;x++)
			{
				if (map.tiles[x][y] == fTileToReplace)
				{
					map.tiles[x][y] = newtile;
					count++;
				}
			}
			
			console.Print("%d tiles replaced.", count);
		}
		break;
		
		case R_RANDOM:
		{
			for(y=rect.y1;y<=rect.y2;y++)
			for(x=rect.x1;x<=rect.x2;x++)
			{
				map.tiles[x][y] = SelectRandomTile();
			}
		}
		break;
	}
	
}

int Tilemode::SelectRandomTile()
{
int i, j;

	if (randrange(0, 10) < 2)
		return 0;
	
	do
	{
		for(i=0;valid_tiles[i] != 0xff;i++)
			;
		
		i = valid_tiles[randrange(0, i - 1)];
		j = i >> 4;
	}
	while(j != 8 && j != 0x0E);
	
	return i;
}










