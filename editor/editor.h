
#ifndef _EDITOR_H
#define _EDITOR_H

#include "dropdown.h"

class EditorMode
{
public:
	virtual void OnEntry() { }
	virtual void OnExit()  { }
	
	virtual void Run()	{ }
	virtual void Draw() { }
	
	virtual void MouseMoved() { }
	virtual void MouseDown()  { }
	virtual void MouseUp()    { }
};

#include "tilemode.h"
#include "spritemode.h"

struct GameState
{
	void TakeSnapshot();
	void Restore();
	
	uint8_t fTiles[MAP_MAXSIZEX][MAP_MAXSIZEY];
	Object fObjects[MAX_OBJECTS];
};


class Editor
{
public:
	bool Init(void);
	void Close();
	
	void Tick(void);
	void Draw(void);
	
	void KeyInput(void);

	void MouseInput();
	void MouseMoved();
	void MouseDown();
	void MouseUp();
	
	static int FindObjectUnderMouse();
	static int FindObjectX2UnderMouse();
	void SetMode(EditorMode *mode);
	void AddObject();
	void KillObjectsOutsideOfMap();
	
	static void NewMap();
	static void Save();
	
	void StartGame();
	void RunGame();
	void StopGame();

//private:
	Tilemode fTileMode;
	Spritemode fSpriteMode;
	EditorMode *fCurMode;
	
	int fCurTile;
	int fCurSprite;
	
	Dropdown fTileMenu;
	Dropdown fSpriteMenu;
	
	bool fGameRunning;
	GameState fGameState;
	
	bool fSettingMapSize;
	int fMapSizeTimer;
};


bool editor_init();
void editor_tick();
void editor_close();

extern Editor editor;

void GetScreenCoords(int x, int y, int sprite, int frame, int dir, int *scrxout, int *scryout);
void GetScreenCoords(Object *o, int *scrxout, int *scryout);

extern const int valid_tiles[];

#endif




