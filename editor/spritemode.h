
#ifndef _SPRITEMODE_H
#define _SPRITEMODE_H


class Spritemode : public EditorMode
{
public:
	void OnEntry();
	void OnExit();
	
	void Run();
	void Draw();
	
	void MouseDown();
	//void MouseMoved();
	//void MouseUp();

//private:
	void BeginNumberEdit(uint16_t *target, int x, int y);
	void EndNumberEdit();
	void RunNumberEdit();
	void DrawNumberEdit();
	
	int fEditX;
	int fEditY;
	uint16_t *fEditTarget;
	int fEditPlace;
	bool fEditFlash;

	void DrawSelector(int x, int y, int frame);
	void Wrap(int *value, int min, int max);
	void Wrap(uint8_t *value, int min, int max);
	void Wrap(uint16_t *value, int min, int max);

	void GetPanelXY(int xa, int ya, int *xout, int *yout);
	void GetPanelRect(int xa, int ya, Rect *out);
	bool GetPanelUnderMouse(int *xout, int *yout);
	void DrawPanelNumber(int xa, int ya, int num);
	
	int CountUnusedObjects();
	
	Object *fObject;
	int fObjectIndex;
};

#endif
