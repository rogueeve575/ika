
#ifndef _DROPDOWN_H
#define _DROPDOWN_H


struct DropdownItem
{
	int index, sprite;
	bool is_sprite;
	
	int x, y, w, h;
	int wspac, hspac;
};


class Dropdown
{
public:
	void Init(int right, int top);
	void Empty();
	
	void Draw();
	void DrawItem(int x, int y, DropdownItem *item);
	bool GetSelectedItemRect(Rect *rect);
	
	int GetSelectedItem();
	bool SetSelectedItem(int index);
	
	void MouseMoved();
	void MouseDown();
	
	void AddItem(int index, bool is_sprite);
	void AddFinished();
	
	int selection;
	
private:
	friend class Editor;
	
	int fWidth, fHeight;
	int fNextX, fNextY;
	int fRight, fTop;
	
	std::vector<DropdownItem *> fItems;
	bool fExpanded;
	bool fDontExpand;
	
	int fSelectedItem;
	bool fHasNewSelection;
	int fTimer;
};


#endif
