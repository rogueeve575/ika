
#include "ika.h"
#include <stdarg.h>

static void cmd_impl_script(StringList *args, int num);
static void cmd_impl_flag(StringList *args, int num);
static void cmd_impl_hp(StringList *args, int num);
static void cmd_impl_item(StringList *args, int num);
static void cmd_impl_music(StringList *args, int num);
static void cmd_impl_hello(StringList *args, int num);

static CommandEntry commands[] =
{
	"script", cmd_impl_script, 1, 1,
	"flag", cmd_impl_flag, 1, 1,
	"hp", cmd_impl_hp, 1, 1,
	"item", cmd_impl_item, 1, 2,
	"music", cmd_impl_music, 1, 1,
	
	"hello", cmd_impl_hello, 0, 0,
	"hi", cmd_impl_hello, 0, 0,
};

DebugConsole::DebugConsole()
{
	fVisible = false;
	fLineLen = 0;
	fCursorTimer = 0;
	fResponseTimer = 0;
	
	fLine[0] = 0;
	fResponse[0] = 0;
	
	fKeyDown = 0;
	fRepeatTimer = 0;
}

/*
void c------------------------------() {}
*/

void DebugConsole::SetVisible(bool newstate)
{
	//stat("DebugConsole::SetVisible(%s)", newstate?"true":"false");
	
	if (fVisible != newstate)
	{
		fVisible = newstate;
		fKeyDown = 0;
		fRepeatTimer = 0;
		
		if (newstate)
		{
			fLine[0] = 0;
			fLineLen = 0;
			fBrowsingExpansion = false;
			fBackIndex = fBackBuffer.CountItems();
			
			fResponse[0] = 0;
			fCursorTimer = 0;
		}
	}
}

bool DebugConsole::IsVisible()
{
	return fVisible;
}

bool DebugConsole::HandleKey(int key)
{
	if (!fVisible) return 0;
	if (key != 9) fBrowsingExpansion = false;
	
	if (key != fKeyDown)
	{
		fKeyDown = key;
		fRepeatTimer = 25;
	}
	else
	{
		fRepeatTimer = 1;
	}
	
	if (key != 27 && key != '`')
	{
		sound(SND_MESSAGE);
		fCursorTimer = 0;
	}
	
	switch(key)
	{
		case 27:
		case '`':
		{
			SetVisible(false);
		}
		break;
		
		case 13:
		case 271:	// numeric enter
		{
			SetVisible(false);
			
			fLine[fLineLen] = 0;
			Execute(fLine);
			fLineLen = 0;
		}
		break;
		
		case 10: break;
		
		case 8:
		{
			if (fLineLen > 0)
				fLineLen--;
		}
		break;
		
		case 9:		// command completion
		{
			fLine[fLineLen] = 0;
			ExpandCommand();
			
			fBrowsingExpansion = true;
			fExpandIndex++;
		}
		break;
		
		// command backbuffer
		case SDLK_UP:
		case SDLK_DOWN:
		{
			if (fBackBuffer.CountItems() > 0)
			{
				fBackIndex += (key == SDLK_UP) ? -1 : 1;
				if (fBackIndex < 0) fBackIndex = (fBackBuffer.CountItems() - 1);
				else fBackIndex %= fBackBuffer.CountItems();
				
				const char *str = fBackBuffer.StringAt(fBackIndex);
				
				maxcpy(fLine, str, sizeof(fLine) - 1);
				fLineLen = strlen(str);
			}
		}
		break;
		
		default:
		{
			if (fLineLen < (sizeof(fLine) - 1))
				fLine[fLineLen++] = key;
		}
		break;
	}
	
	return 1;
}

void DebugConsole::HandleKeyRelease(int key)
{
	if (key == fKeyDown)
	{
		fKeyDown = 0;
		fRepeatTimer = 0;
	}
}

/*
void c------------------------------() {}
*/

void DebugConsole::Draw()
{
	if (fResponse[0])
	{
		this->DrawText(fResponse);
		
		if (--fResponseTimer <= 0)
			fResponse[0] = 0;
	}
	else if (fVisible)
	{
		// key-repeat
		if (fKeyDown)
		{
			if (--fRepeatTimer < 0)
				HandleKey(fKeyDown);
		}
		
		char buffer[CONSOLE_MAXCMDLEN + 10];
		fLine[fLineLen] = 0;
		
		sprintf(buffer, "-> %s%c",
			fLine, (fCursorTimer < 20) ? '_' : ' ');
		
		this->DrawText(buffer);
		
		if (++fCursorTimer > 30)
			fCursorTimer = 0;
	}
}

void DebugConsole::DrawText(const char *text)
{
	font_draw_shaded(4, (SCREEN_HEIGHT - 16), text);
}

/*
void c------------------------------() {}
*/

void DebugConsole::Print(const char *fmt, ...)
{
va_list ar;

	va_start(ar, fmt);
	vsnprintf(fResponse, sizeof(fResponse), fmt, ar);
	va_end(ar);
	
	stat("%s", fResponse);
	fResponseTimer = 60;
}

/*
void c------------------------------() {}
*/

bool DebugConsole::Execute(const char *line)
{
	stat("DebugConsole::Execute('%s')", line);
	
	// record command in backbuffer
	if (fBackBuffer.CountItems() >= CONSOLE_MAX_BACK)
		fBackBuffer.RemoveString(0);
	fBackBuffer.AddString(line);
	
	// split command into arguments
	StringList args;
	char *cmd = SplitCommand(line, &args);
	
	if (cmd)
	{
		std::vector<CommandEntry *> matches;
		MatchCommand(cmd, &matches);
		free(cmd);

		if (matches.size() == 1)
		{
			auto *command = *(matches.begin());
			
			if (args.CountItems() < command->minArgs || \
				args.CountItems() > command->maxArgs)
			{
				if (command->minArgs == command->maxArgs)
				{
					Print("'%s' requires %d argument%s", \
						command->name, command->minArgs, \
						(command->minArgs == 1) ? "":"s");
				}
				else if (args.CountItems() < command->minArgs)
				{
					Print("'%s' requires at least %d argument%s",
						command->name, command->minArgs,
						(command->minArgs == 1) ? "":"s");
				}
				else
				{
					Print("'%s' requires no more than %d arguments",
						command->name, command->maxArgs);
				}
			}
			else
			{
				void (*handler)(StringList *, int) = command->handler;
				int num = (args.CountItems() > 0) ? atoi(args.StringAt(0)) : 0;
				
				(*handler)(&args, num);
				return 1;
			}
		}
		else if (matches.size() == 0)
		{
			Print("I don't understand");
		}
		else
		{
			Print("Ambiguous command");
		}
	}
	
	return 0;
}

// returns all commands that could match the specified command --
// i.e. this makes it so you can just type "mu" instead of full "music"
// if it returns anything other than 1 then the command is unmatched or ambiguous.
int DebugConsole::MatchCommand(const char *usercmd, std::vector<CommandEntry *> *matches_out)
{
	for(auto &cmd : commands) {
		if (strcasebegin(cmd.name, usercmd))
			matches_out->push_back(&cmd);
	}

	return matches_out->size();
}

// split an input line into command and arguments
// returns the command portion of the line. you must free this buffer.
char *DebugConsole::SplitCommand(const char *line_in, StringList *args)
{
	while(*line_in == ' ' || *line_in == '\t') line_in++;
	char *line = strdup(line_in);
	
	char *cmd = strtok(line, " \t");
	if (cmd && cmd[0])
	{
		while(const char *arg = strtok(NULL, " \t"))
		{
			args->AddString(arg);
		}
		
		return line;
	}
	
	free(line);
	return NULL;
}

// tab-expand the current command
void DebugConsole::ExpandCommand()
{
StringList args;
std::vector<CommandEntry *> matches;
char *cmd = nullptr;

	fLine[fLineLen] = 0;
	
	if (!fBrowsingExpansion)
	{
		maxcpy(fLineToExpand, fLine, sizeof(fLineToExpand));
		fExpandIndex = 0;
	}
	
	cmd = SplitCommand(fLineToExpand, &args);
	if (cmd)
	{
		MatchCommand(cmd, &matches);
		free(cmd);
		
		if (matches.size() > 0)
		{
			if (fExpandIndex >= matches.size())
				fExpandIndex = 0;
		
			auto *command = matches.at(fExpandIndex);
			DString newCommand(command->name);
			
			for(int i=0;;i++)
			{
				const char *arg = args.StringAt(i);
				if (!arg) break;
				
				newCommand.AppendChar(' ');
				newCommand.AppendString(arg);
			}
			
			if (args.CountItems() < command->minArgs)
				newCommand.AppendChar(' ');
			
			maxcpy(fLine, newCommand.String(), sizeof(fLine));
			fLineLen = strlen(fLine);
		}
	}

	// error beep if not matched or ambiguous
	if (matches.size() != 1)
		sound(SND_NODMG);
}

/*
void c------------------------------() {}
*/

#define Respond		console.Print

static void cmd_impl_script(StringList *args, int num)
{
	StartScript(num);
	Respond("Script %d started.", num);
}

static void cmd_impl_flag(StringList *args, int num)
{
	int val = game.readflag(num) ^ 1;
	game.setflag(num, val);
	
	Respond("Flag %04d: %s", num, val ? "SET" : "CLEARED");
}

static void cmd_impl_hp(StringList *args, int num)
{
	player->hp = num;
}

static void cmd_impl_item(StringList *args, int num)
{
	if (num < 0 || num >= NUM_ITEMS)
	{
		Respond("Invalid item #.");
		return;
	}
	
	if (args->CountItems() == 1 || atoi(args->StringAt(1)))
	{
		PGiveItem(num);
		Respond("Gave item %d.", num);
	}
	else
	{
		if (PHasItem(num))
		{
			PTakeItem(num);
			Respond("Took item %d.", num);
		}
		else
		{
			Respond("You don't have item %d.", num);
		}
	}
}

static void cmd_impl_music(StringList *args, int num) {
	music(num, true);
}

/*
void c------------------------------() {}
*/

static void cmd_impl_hello(StringList *args, int num)
{
	Respond("Howdy, whassup?");
}


///////////////////////////////////////////////////////////////////////////////

