
#include "ika.h"
#include "player.h"
#include "player.fdh"

Player *player = NULL;

static void handle_dir(int xtile, int ytile, uint8_t tileno);
static void handle_dmg(int xtile, int ytile);
static void handle_snack(int xtile, int ytile, uint8_t checkindex);
static void handle_solid(int xtile, int ytile, uint8_t checkindex,
		bool stopplayer, int bonksound);

void InitPlayer()
{
	if (player) delete player;
	player = new Player;
	memset(player, 0, sizeof(Player));
	
	PUpdateEquip();
	
	player->x = 0xa0000;
	player->y = 0x1a0000;
	player->hp = 4;
	player->enabled = true;
	
	map_centeron(player->CenterX(), player->CenterY());
}

// thrust speeds
static const Point thrust_table[] =
{
	{ -0x200, -0x200 },
	{ 0x0200, -0x200 },
	{ 0x000,  -0x2D4 }
};

// capacitor thrust speeds
static const Point capacitor_table[] =
{
	{ -0xC00, 0 },
	{ 0x0C00, 0 },
	{ 0, -0xC00 }
};

// spawn points for bubbles generated from thrusting,
// relative to player XY
static const Point bubble_pos[] =
{
	{ 14<<CSF, 12<<CSF },
	{ 3<<CSF,  12<<CSF },
	{ 9<<CSF,  14<<CSF }
};

#define NUM_LEVELS		6
// max HP's at each level
static const uint16_t maxhp[] =	{ 4, 8,  12, 18, 26,  34,  62  };
// XP required to level up
static const uint16_t maxxp[] =	{ 8, 28, 52, 74, 102, 360, 852 };


static void PControlNormal(Player *o);
static void PControlCapacitor(Player *o);
static void PControlEscaping(Player *o);

static void (*ControlFuncs[NUM_CONTROL_MODES])(Player *) = {
	PControlNormal,
	PControlCapacitor,
	PControlEscaping
};


void PRunCurrentControlMode()
{
	//debug("x: %x", player->x);
	//debug("y: %x", player->y);
	//debug("ec: %d", game.entcount);
	
	(*ControlFuncs[player->controlmode])(player);
}

/*
void c------------------------------() {}
*/

static void PControlNormal(Player *o)
{
Caret *bubble;

	o->dir = UP;
	if (inputs & LEFTKEY) o->dir = LEFT;
	if (inputs & RIGHTKEY) o->dir = RIGHT;
	
	// thrusting
	if ((newinputs & ZKEY) && !o->canthrusttime)
	{
		// create bubble
		if ((bubble = CreateCaret(CAR_BUBBLE)))
		{
			sound(SND_PROPEL);
			bubble->xinertia = -thrust_table[o->dir].x;
			bubble->yinertia = -thrust_table[o->dir].y;
			
			bubble->x = o->x + bubble_pos[o->dir].x;
			bubble->y = o->y + bubble_pos[o->dir].y;
		}
		
		if (o->thrustanimtimer < 100)
			o->thrustanimtimer += 10;
		
		o->canthrusttime = 8;
		o->frame = 1;
		
		// thrust
		o->xinertia += thrust_table[o->dir].x;
		o->yinertia += thrust_table[o->dir].y;
	}
	
	// more thrusting and capacitor
	if ((inputs & ZKEY) && (o->equipmask & EQUIP_CAPACITOR))
	{
		if (o->captimer == 0x1F)
			sound(SND_READY);
		
		o->captimer++;
		if (o->captimer > 0x20)
			o->captimer = 0x20;
	}
	else
	{
		if (o->captimer == 0x20)
		{
			// create bubble
			if ((bubble = CreateCaret(CAR_BUBBLE)))
			{
				sound(SND_GO);
				bubble->xinertia = -(capacitor_table[o->dir].x / 8);
				bubble->yinertia = -(capacitor_table[o->dir].y / 8);
				
				bubble->x = o->x + bubble_pos[o->dir].x;
				bubble->y = o->y + bubble_pos[o->dir].y;
			}
			
			// thrust
			o->controlmode = CONTROL_CAPACITOR;
			o->xinertia = capacitor_table[o->dir].x;
			o->yinertia = capacitor_table[o->dir].y;
		}
		else
		{
			o->captimer = 0;
		}
	}
	
	if (o->thrustanimtimer > 0)
		o->thrustanimtimer--;
	else
		o->frame = 0;
	
	if (o->canthrusttime > 0)
		o->canthrusttime--;
	
	if (o->captimer == 0x20)
		o->frame = 2;
	
	// gravity
	if (o->yinertia < 0x800)
		o->yinertia += 0x14;
	
	LIMITY(0x800);
	
	// friction
	LIMITX(0x800);
	if (o->xinertia > 0) o->xinertia -= 0x08;
	if (o->xinertia < 0) o->xinertia += 0x08;
	
	if (!o->inair)
	{
		if (o->xinertia > 0) o->xinertia -= 0x18;
		if (o->xinertia < 0) o->xinertia += 0x18;
	}
	
	if (o->xinertia >= 8 || o->xinertia <= -8)
		o->x += o->xinertia;
	
	o->y += o->yinertia;
	
	if (o->hurtflashtime) o->hurtflashtime--;
	if (o->canattacktime) o->canattacktime--;
}


static void PControlCapacitor(Player *o)
{
	if (--o->captimer <= 0)
		o->controlmode = CONTROL_NORMAL;
	
	if ((o->captimer % 4) == 0)
	{
		Caret *bubble;
		if ((bubble = CreateCaret(CAR_BUBBLE)))
		{
			bubble->xinertia = randrange(-0x200, 0x200) - (capacitor_table[o->dir].x / 8);
			bubble->yinertia = randrange(-0x200, 0x200) - (capacitor_table[o->dir].y / 8);
			
			bubble->x = o->x + 0x2000;
			bubble->y = o->y + 0x2000;
		}
	}
	
	o->x += o->xinertia;
	o->y += o->yinertia;
	
	if (o->hurtflashtime) o->hurtflashtime--;
	o->frame = 3;
}

// escaping in ship (in ending sequence)
static void PControlEscaping(Player *o)
{
	CaretMaker *cm = CreateCaretMaker(CAR_STAR);
	{
		cm->x = player->x + 0x2000;
		cm->y = player->y + 0x6000;
		
		cm->min_xinertia = -0xC00;
		cm->max_xinertia = 0xC00;
		
		cm->min_yinertia = 0;
		cm->max_yinertia = 0xC00;
		
		cm->xrange = 1;
	}
	
	player->yinertia -= 0x10;
	player->y += player->yinertia;
	
	player->frame = 3;
	player->dir = UP;
	
	player->hurtflashtime = 0;
}


/*
void c------------------------------() {}
*/

// each index corresponds to the object id of that number
static const uint8_t level_for_defeating[] =
{
	1,	// hari
	4,	// isogin
	2,	// kani
	0,	// save
	3,	// chibi
	2,	// hoshi
	0,	// sign
	0,	// carry
	0,	// juel
	0,	// ship
};

static const uint8_t xp_for_defeating[] =
{
	1,	// hari
	0,	// isogin
	3,	// kani
	0,	// save
	0,	// chibi
	3,	// hoshi
	0,	// sign
	0,	// carry
	0,	// juel
	0,	// ship
};

// * hit detection with objects
// * damage to player
// * starting of scripts
void PRunObjectInteraction(void)
{
bool added_blockflag;
CaretMaker *cm;

	#define START_EVENT		\
	{						\
		if (o->event != 0)		\
		{							\
			StartScript(o->event);			\
			player->canattacktime = 100;	\
		}	\
	}
	
	Object *o;
	FOREACH_OBJECT(o)
	{
		if (!o->exists) continue;
		added_blockflag = false;
		
		// triggering events by non-blocking objects
		if (o->ty == TY_EVENT && !player->canattacktime)
		{
			// check for collision with player
			if (player->x < o->x + (4<<CSF) && \
				player->x > o->x - (4<<CSF) && \
				player->y < o->y + (4<<CSF) && \
				player->y > o->y - (4<<CSF))
			{
				START_EVENT;
				continue;
			}
		}
		
		// handle solidevents and enemy damages
		// (provided hurtflashtime is not in effect)
		if ((o->ty == TY_ENEMY && !player->hurtflashtime) || \
			o->ty == TY_SOLIDEVENT)
		{
			if (player->y < (o->y + 0x3000) && \
				player->y > (o->y - 0x3000))
			{
				// object pushing player left
				if (player->x > o->x - 0x3400 && \
					player->x + 0x3ff < o->x - 0x2000)
				{
					player->x = o->x - 0x3400;
					player->xinertia = 0;
					player->blockflag |= BLOCK_RIGHT;
					added_blockflag = true;
				}
				
				// object pushing player right
				if (player->x < o->x + 0x3400 && \
					player->x > o->x + 0x2000)
				{
					player->x = o->x + 0x3400;
					player->xinertia = 0;
					player->blockflag |= BLOCK_LEFT;
					added_blockflag = true;
				}
			}
			
			if (player->x < (o->x + 0x3000) && \
				player->x > (o->x - 0x3000))
			{
				// bopping head on object
				if (player->y < o->y + 0x3400 && \
					player->y > o->y + 0x2000)
				{
					if (player->yinertia < -100)
						sound(SND_HITHEAD);
					
					player->y = o->y + 0x3400;
					player->yinertia = 0;
					player->blockflag |= BLOCK_ABOVE;
					added_blockflag = true;
				}
				
				// standing on object
				if (player->y >= (o->y - 0x3400) && \
					player->y < (o->y - 0x2000))
				{
					player->inair = false;
					player->y = (o->y - 0x3400);
					
					if (player->yinertia > 0)
						player->yinertia = 0;
					
					player->blockflag |= BLOCK_BELOW;
					added_blockflag = true;
				}
			}
			
			if (added_blockflag && player->canattacktime == 0)
			{
				switch(o->ty)
				{
					case TY_SOLIDEVENT:
					{
						START_EVENT;
					}
					break;
					
					case TY_ENEMY:
					{
						if (o->state == 1)	// attacking
						{
							if (player->x < o->x) player->xinertia = -0x400;
							if (player->x > o->x) player->xinertia = 0x400;
							
							hurtplayer(objtable[o->type].damage);
						}
						
						// we can kill the enemy if...
						// * we are capacitoring, and we hit something other
						//   than the top of it.
						//
						// * or if we hit the bottom of it, and have the hardhat.
						bool tryAttack = false;
						
						if (player->controlmode == CONTROL_CAPACITOR && \
							player->blockflag != BLOCK_BELOW)
							tryAttack = true;
						
						if ((player->blockflag & BLOCK_ABOVE) && \
							(player->equipmask & EQUIP_HARDHAT))
							tryAttack = true;
						
						if (tryAttack)
						{
							if (player->level >= level_for_defeating[o->type])
							{
								sound(SND_KILL_ENEMY);
								player->xp += xp_for_defeating[o->type];
								o->Delete();
								
								cm = CreateCaretMaker(CAR_NUMBERS, 1);
								cm->frame = xp_for_defeating[o->type];
								cm->dir = RIGHT;	// blue numbers
								cm->x = o->x + 0x2000;
								cm->y = o->y - 0x1000;
								cm->xrange = 1;
								
								cm = CreateCaretMaker(CAR_STAR, 6);
								cm->x = o->x + 0x2000;
								cm->y = o->y + 0x2000;
								
								cm->min_xinertia = -0x800;
								cm->max_xinertia = 0x800;
								cm->min_yinertia = -0x800;
								cm->max_xinertia = -0x200;
								cm->xrange = 8;
								cm->yrange = 8;
								
								START_EVENT;
							}
							else
							{
								if (!player->canattacktime)
									sound(SND_NODMG);
								
								player->canattacktime = 100;
							}
						}
					}
					break;
				}
				break;
			}
		}
	}
}

void PRunMapInteraction(void)
{
int tiles_supporting;
int i;

	// get player X & Y tile
	int pxtile = (player->x >> CSF) / TILE_W;
	int pytile = (player->y >> CSF) / TILE_H;
	
	#define NUM_CHECKTILES			4
	static const struct
	{
		int xtoff;					// x offset from pxtile, in tiles
		int ytoff;					// y offset from pytile, in tiles
		bool cansupportplayer;		// 1 if this index is one of the ones player can stand on
	}
	tile[] =
	{
		0, 0, 0,
		1, 0, 0,
		0, 1, 1,
		1, 1, 1,
	};
	
	player->blockflag = 0x00;
	tiles_supporting = 2;
	
	// check each tile the player could be occupying.
	// because the player x,y represents the upper-left corner of his sprite,
	// he may be touching up to four--the one exactly under his X,Y, and the
	// three to the right and below that one (forms a 4-tile grid around the player).
	for(i=0;i<NUM_CHECKTILES;i++)
	{
		int xtile = (pxtile + tile[i].xtoff);
		int ytile = (pytile + tile[i].ytoff);
		int t = map.tiles[xtile][ytile];
		
		if (t >= 0x80 && t < 0xA0)			// PrtSolid
		{
			handle_solid(xtile, ytile, i, true, SND_HITHEAD);
			
			if (!(player->blockflag & BLOCK_BELOW))
				tiles_supporting -= tile[i].cansupportplayer;
		}
		else if (t >= 0xE0)					// PrtSnack
		{
			handle_snack(xtile, ytile, i);
			
			if (!(player->blockflag & BLOCK_BELOW))
				tiles_supporting -= tile[i].cansupportplayer;
		}
		else
		{
			tiles_supporting -= tile[i].cansupportplayer;
		}
		
		if (t >= 0x60 && t < 0x80)			// PrtDir
			handle_dir(xtile, ytile, t);
		
		if (t >= 0xA0 && t < 0xC0)			// PrtDmg
			handle_dmg(xtile, ytile);
		
		if (t >= 0x40 && t < 0x60)			// PrtItem
			handle_item(xtile, ytile);
	}
	
	// handle leveling up
	if (player->xp >= maxxp[player->level])
	{
		//stat("level up from level %d: xp = %d, max = %d",
			//player->level, player->xp, maxxp[player->level]);
		
		player->xp -= maxxp[player->level];
		player->hp = maxhp[player->level];
		player->level++;
		
		if (player->level > NUM_LEVELS)
			player->level = NUM_LEVELS;
		else
			sound(SND_LEVELUP);
		
		CaretMaker *cm = CreateCaretMaker(CAR_LEVELUP);
		{
			cm->x = player->CenterX();
			cm->y = player->y - (4<<CSF);
			cm->xrange = 1;
			cm->yrange = 0;
		}
	}
	
	if (tiles_supporting == 0)
		player->inair = true;
	
}


// handles "PrtBlock" solid tiles. also used by handle_snack.
// stopplayer: whether or not to zero inertia on player running into this.
// bonksound: pass SND_HITHEAD if you want it to tink on hitting, -1 for quiet.
static void handle_solid(int xtile, int ytile, uint8_t checkindex,
						bool stopplayer, int bonksound)
{
	if (checkindex == INDEX_UL)
	{
		// check if blocked to left by tile above
		if ((player->x >> CSF) < (xtile * 16) + 15)
		{
			if ((player->y >> CSF) < (ytile * 16) + 12)
			{
				if (stopplayer)
				{
					if (player->xinertia < -0x400)
						sound(bonksound);
					
					player->xinertia = 0;
				}
				
				player->x = MAPX(xtile) + (15 << CSF);
				player->blockflag |= BLOCK_LEFT;
			}
		}
		
		// check if blocked above by tile to left
		if ((player->y >> CSF) < (ytile * 16) + 15)
		{
			if ((player->x >> CSF) < (xtile * 16) + 12)
			{
				if (stopplayer)
				{
					if (player->yinertia < -0x400)
						sound(bonksound);
					
					player->yinertia = 0;
				}
				
				player->y = MAPY(ytile + 1);
				player->blockflag |= BLOCK_ABOVE;
			}
		}
	}
	else if (checkindex == INDEX_UR)
	{
		// check if blocked to right by tile above
		int px = (player->x + ((1 << CSF) - 1));
		if ((px >> CSF) > (xtile * 16) - 14)
		{
			if ((player->y >> CSF) < (ytile * 16) + 12)
			{
				if (stopplayer)
				{
					if (player->xinertia > 0x400)
						sound(bonksound);
					
					player->xinertia = 0;
				}
				
				player->x = MAPX(xtile) - (14 << CSF);
				player->blockflag |= BLOCK_RIGHT;
			}
		}
		
		// check if blocked above by tile to right
		if ((player->y >> CSF) < (ytile * 16) + 15)
		{
			if ((player->x >> CSF) > (xtile * 16) - 12)
			{
				if (stopplayer)
				{
					if (player->yinertia < -0x400)
						sound(bonksound);
					
					player->yinertia = 0;
				}
				
				player->y = MAPY(ytile + 1);
				player->blockflag |= BLOCK_ABOVE;
			}
		}
	}
	else if (checkindex == INDEX_LL)
	{
		// check if blocked to left by lower tile
		if ((player->x >> CSF) < (xtile * 16) + 15)
		{
			if ((player->y >> CSF) > (ytile * 16) - 12)
			{
				if (stopplayer)
				{
					if (player->xinertia < -0x400)
						sound(bonksound);
					
					player->xinertia = 0;
				}
				
				player->x = MAPX(xtile) + (15 << CSF);
				player->blockflag |= BLOCK_LEFT;
			}
		}
		
		// check if blocked below by tile to left
		if ((player->y >> CSF) >= (ytile * 16) - 16)
		{
			if ((player->x >> CSF) < (xtile * 16) + 12)
			{
				if (stopplayer)
				{
					if (player->yinertia > 0)
						player->yinertia = 0;
					
					player->inair = false;
				}
				
				player->y = MAPY(ytile - 1);
				player->blockflag |= BLOCK_BELOW;
			}
		}
	}
	else if (checkindex == INDEX_LR)
	{
		// check if blocked to right by lower tile
		int px = (player->x + ((1 << CSF) - 1));
		if ((px >> CSF) > ((xtile * 16) - 14))
		{
			if ((player->y >> CSF) > (ytile * 16) - 12)
			{
				if (stopplayer)
				{
					if (player->xinertia > 0x400)
						sound(bonksound);
					
					player->xinertia = 0;
				}
				
				player->x = MAPX(xtile) - (14 << CSF);
				player->blockflag |= BLOCK_RIGHT;
			}
		}
		
		// check if blocked below by tile to right
		if ((player->y >> CSF) >= (ytile * 16) - 16)
		{
			if ((player->x >> CSF) > (xtile * 16) - 12)
			{
				if (stopplayer)
				{
					if (player->yinertia > 0)
						player->yinertia = 0;
					
					player->inair = false;
				}
				
				player->y = MAPY(ytile - 1);
				player->blockflag |= BLOCK_BELOW;
			}
		}
	}
}


// handle PrtSnack (breakable blocks)
static void handle_snack(int xtile, int ytile, uint8_t checkindex)
{
	// ensure reset as this affects whether the block is broken at the end.
	// it's a cheesy way of ensuring any blockflags we act on came from checking
	// this particular tile.
	player->blockflag = 0;
	
	// besides the breakability, snack tiles work almost exactly like
	// regular solid tiles, except for that if the capacitor is on they
	// don't stop us, and they never make the "hithead" sound.
	bool stopplayer = (player->controlmode != CONTROL_CAPACITOR);
	handle_solid(xtile, ytile, checkindex, stopplayer, -1);
	
	// now break blocks if needed
	if (player->blockflag)
	{
		bool break_block = false;
		
		if (player->controlmode == CONTROL_CAPACITOR)
		{
			// capacitor doesn't break blocks that we're just flying over
			if (player->blockflag != BLOCK_BELOW)
				break_block = true;
		}
		else if (player->equipmask & EQUIP_HARDHAT)
		{
			if (player->blockflag & BLOCK_ABOVE)
				break_block = true;
		}
		
		if (break_block)
		{
			sound(SND_BREAK_BLOCK);
			map.tiles[xtile][ytile] = 0;
			
			// make some bubbles
			CaretMaker *cm = CreateCaretMaker(CAR_BUBBLE, 5);
			{
				cm->x = MAPX(xtile) + ((TILE_W / 2) << CSF);
				cm->y = MAPY(ytile) + ((TILE_H / 2) << CSF);
				
				cm->min_xinertia = -0x400;
				cm->min_yinertia = -0x400;
				cm->max_xinertia = 0x400;
				cm->max_yinertia = 0x400;
				
				cm->xrange = (TILE_W / 2);
				cm->yrange = (TILE_H / 2);
			}
		}
	}
}


// handle PrtDir (moving bubbles/current)
static void handle_dir(int xtile, int ytile, uint8_t tileno)
{
	if (((player->x >> CSF) > (MAPPX(xtile) - 8)) && \
		((player->x >> CSF) < (MAPPX(xtile) + 8)))
	{
		if (((player->y >> CSF) > (MAPPY(ytile) - 8)) && \
			((player->y >> CSF) < (MAPPY(ytile) + 8)))
		{
			switch(tileno)
			{
				case 0x60: player->xinertia -= 0x32; break;
				case 0x61: player->xinertia += 0x32; break;
				case 0x62: player->yinertia -= 0x32; break;
				case 0x63: player->yinertia += 0x32; break;
			}
		}
	}
}


// handle PrtDmg (spikes)
static void handle_dmg(int xtile, int ytile)
{
	if (((player->x >> CSF) > (MAPPX(xtile) - 10)) && \
		((player->x >> CSF) < (MAPPX(xtile) + 10)))
	{
		if (((player->y >> CSF) > (MAPPY(ytile) - 10)) && \
			((player->y >> CSF) < (MAPPY(ytile) + 10)))
		{
			hurtplayer(3);
		}
	}
}


// handle PrtItem (fish)
void handle_item(int xtile, int ytile)
{
CaretMaker *cm;

	if (((player->x >> CSF) > (MAPPX(xtile) - 8)) && \
		((player->x >> CSF) < (MAPPX(xtile) + 8)))
	{
		if (((player->y >> CSF) > (MAPPY(ytile) - 8)) && \
			((player->y >> CSF) < (MAPPY(ytile) + 8)))
		{
			map.tiles[xtile][ytile] = 0;
			sound(SND_ITEM);
			
			player->xp++;
			player->hp++;
			
			// note you don't get HP for the point that levels you up.
			// (leveling-up is done once all tiles have been handled).
			if (player->hp > maxhp[player->level])
				player->hp = maxhp[player->level];
			
			// "+1" rising number
			if ((cm = CreateCaretMaker(CAR_NUMBERS)))
			{
				cm->dir = RIGHT;	// blue "+" numbers
				cm->frame = 1;
				cm->amt = 1;
				cm->x = player->CenterX();
				cm->y = player->y - (4<<CSF);
				cm->xrange = 1;
				cm->yrange = 0;
			}
			
			// some stars
			if ((cm = CreateCaretMaker(CAR_STAR)))
			{
				cm->amt = 4;
				cm->x = player->CenterX();
				cm->y = player->CenterY();
				
				cm->min_xinertia = -0x800;
				cm->max_xinertia = 0x800;
				cm->min_yinertia = -0x800;
				cm->max_yinertia = 0;
				cm->xrange = 1;
				cm->yrange = 0;
			}
		}
	}
}

/*
void c------------------------------() {}
*/

// damage player by given amount
void hurtplayer(int amount)
{
CaretMaker *cm;

	if (player->hurtflashtime != 0)
		return;
	
	player->hp -= amount;
	player->hurtflashtime = 100;
	
	if (player->hp < 0)
		player->hp = 0;
	
	// generates the floattext....
	if ((cm = CreateCaretMaker()))
	{
		cm->in_use = true;
		cm->type = CAR_NUMBERS;
		cm->frame = (10 - amount);
		cm->amt = 1;
		cm->x = player->x + (8<<CSF);
		cm->y = player->y - (4<<CSF);
		cm->xrange = 1;
		cm->yrange = 0;
	}
	
	if (player->hp == 0)
	{
		sound(SND_DEAD);
		
		// generate explode star effect
		if ((cm = CreateCaretMaker(CAR_STAR)))
		{
			cm->amt = 30;
			cm->x = player->x + (8<<CSF);
			cm->y = player->y + (8<<CSF);
			
			cm->max_xinertia = 0xC00;
			cm->min_xinertia = -0xC00;
			cm->max_yinertia = 0x200;
			cm->min_yinertia = -0xC00;
			cm->xrange = 1;
			cm->yrange = 0;
		}
		
		player->enabled = false;
		player->died = true;
	}
	else
	{
		sound(SND_OUCH);
	}
}


void PGiveHP(int amt)
{
	player->hp += amt;
	if (player->hp > maxhp[player->level])
		player->hp = maxhp[player->level];
}

/*
void c------------------------------() {}
*/

void MapMover(void)
{
	int speed = 0x2000;
	
	if (inputs & LEFTKEY)	player->x -= speed;
	if (inputs & RIGHTKEY)	player->x += speed;
	
	if (inputs & UPKEY)		player->y -= speed;
	if (inputs & DOWNKEY)	player->y += speed;
	
	map_centeron(player->x, player->y);
	player->xinertia = 0;
	player->yinertia = 0;
	player->dir = UP;
	player->hurtflashtime = 0;
}

/*
void c------------------------------() {}
*/

bool PHasItem(int item)
{
	for(int i=0;i<NUM_INVENTORY_SLOTS;i++)
		if (player->inventory[i] == item) return true;
	
	return false;
}

void PGiveItem(int item)
{
	for(int i=0;i<NUM_INVENTORY_SLOTS;i++)
	{
		if (player->inventory[i] == ITEM_NULL)
		{
			player->inventory[i] = item;
			break;
		}
	}
	
	PUpdateEquip();
}

void PTakeItem(int item)
{
	for(int i=0;i<NUM_INVENTORY_SLOTS;i++)
	{
		if (player->inventory[i] == item)
		{
			for(;i<NUM_INVENTORY_SLOTS-1;i++)
				player->inventory[i] = player->inventory[i+1];
			
			player->inventory[NUM_INVENTORY_SLOTS-1] = ITEM_NULL;
			break;
		}
	}
	
	PUpdateEquip();
}

void PTakeAllItems(void)
{
	memset(player->inventory, 0, sizeof(player->inventory));
	PUpdateEquip();
}

// bit of a hack
void PUpdateEquip()
{
	player->equipmask = 0;
	if (PHasItem(ITEM_HARDHAT))	  player->equipmask = EQUIP_HARDHAT;
	if (PHasItem(ITEM_CAPACITOR)) player->equipmask |= EQUIP_CAPACITOR;
	if (PHasItem(ITEM_SHIP))	  player->equipmask |= EQUIP_SHIP;
	
	player->sprite = SPR_MYCHAR2;
	if (player->equipmask & EQUIP_HARDHAT) player->sprite = SPR_MYCHAR;
	if (player->equipmask & EQUIP_SHIP)	   player->sprite = SPR_MYCHAR3;
}

/*
void c------------------------------() {}
*/

void PDrawStatusBar()
{
	//font_draw_shaded(4, 4, stprintf("hp: %d / %d", player->hp, maxhp[player->level]), 0, &greenfont);
	//font_draw_shaded(4, 15, stprintf("xp: %d / %d", player->xp, maxxp[player->level]), 0, &greenfont);
	//font_draw_shaded(4, 26, stprintf("level: %d", player->level), 0, &greenfont);
	if (game.hidestatusbar)
		return;
	
	int x = 8;
	int y = 8;
	draw_sprite(x, y, SPR_STATUS);
	
	DrawNumber(x+48, y, player->level, SPR_FIGURE);
	DrawNumber(x+48, y+8, player->xp, SPR_FIGURE);
	DrawNumber(x+80, y+8, maxxp[player->level], SPR_FIGURE);
	DrawNumber(x+48, y+16, player->hp, SPR_FIGURE);
	DrawNumber(x+80, y+16, maxhp[player->level], SPR_FIGURE);
}

// draws a given number using sprite 's' as the font
// the numbers are drawn right-aligned to "x".
void DrawNumber(int x, int y, uint16_t num, int s)
{
int i, places;

	if (num > 999)
	{
		places = 4;
		if (num > 9999) num = 9999;
	}
	else if (num > 99) places = 3;
	else if (num > 9) places = 2;
	else places = 1;
	
	for(i=0;i<places;i++)
	{
		draw_sprite(x, y, s, num % 10);
		num /= 10;
		
		x -= sprites[s].w;
	}
}


void PDrawPlayer()
{
	int scr_x = (player->x >> CSF) - (map.xscroll >> CSF);
	int scr_y = (player->y >> CSF) - (map.yscroll >> CSF);
	
	if (player->sprite == SPR_MYCHAR3)
	{
		scr_x -= 12;
		scr_y -= 12;
	}
	
	draw_sprite(scr_x, scr_y, player->sprite, player->frame, player->dir);
}


