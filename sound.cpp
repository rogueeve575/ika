
#include <SDL/SDL.h>
#include "common/basics.h"
#include "sslib.h"
#include "sound.h"
#include "datadirs.h"
static void ResampleSound(int s, int freq);

static struct Sound
{
	signed short *samples;
	uint32_t nsamples;
	int channel;
	
	uint8_t *rawdata;
	bool double_samples;
}
sounds[NUM_SOUNDS];

#include "sound.fdh"

const static struct {
	int id;
	const char *name;	// name without extension (.wav is added by GetSoundFilename)
}
soundtable[] = {
	SND_NODMG,			"nodmg",
	SND_PROPEL,			"dash",
	SND_OUCH,			"ouch",
	SND_ITEM,			"item",
	SND_HITHEAD,		"hithead",
	SND_BREAK_BLOCK,	"crash",
	SND_READY,			"ready",
	SND_GO,				"go",
	SND_MESSAGE,		"message",
	SND_DEAD,			"dead",
	SND_KILL_ENEMY,		"win",
	SND_LEVELUP,		"levelup",
	SND_LIFEUP,			"lifeup",
	SND_SAVE,			"save",
	SND_YESNO,			"yesno",
	SND_QUAKE,			"quake",
	
	SND_BOSSOUCH, 		"bossouch",
	-1,					NULL
};

static const char *music_files[] =
{
	"Ikachan",
	"Magirete",
	"Buriki",
	"Mizuno",
	"Quake",
	"Tidepool"
};

#define MUSIC_NUM_TRACKS		(sizeof(music_files) / sizeof(music_files[0]))


bool sound_init()
{
	memset(sounds, 0, sizeof(sounds));
	for(int i=0;i<NUM_SOUNDS;i++)
		sounds[i].channel = -1;
	
	if (SSInit()) return 1;
	
	for(int i=0;soundtable[i].name;i++) {
		if (LoadSound(soundtable[i].name, &sounds[soundtable[i].id])) {
			staterr("failed to load sound #%d ('%s')", i, soundtable[i].name);
			return 1;
		}
	}
	
	ResampleSound(SND_QUAKE, 7100);
	ResampleSound(SND_NODMG, 12100);
	ResampleSound(SND_BOSSOUCH, 12100);
	
	for(int i=0;i<NUM_SOUNDS;i++) {
		if (sounds[i].rawdata) {
			Upconvert(&sounds[i]);
			free(sounds[i].rawdata);
		}
	}
	
	return 0;
}

static void ResampleSound(int s, int freq)
{
	#define SSF		15
	uint32_t inc = (freq << SSF) / SAMPLE_RATE;
	uint32_t factor = (SAMPLE_RATE << SSF) / freq;
	uint32_t newnsamples = (sounds[s].nsamples * factor) >> SSF;
	
	uint8_t *newsamples = (uint8_t *)malloc(newnsamples);
	uint32_t maxpos = (sounds[s].nsamples << SSF);
	uint32_t pos = 0;
	
	for(int i=0;i<newnsamples;i++)
	{
		newsamples[i] = sounds[s].rawdata[pos >> SSF];
		
		pos += inc;
		if (pos > maxpos) pos = maxpos;
	}
	
	free(sounds[s].rawdata);
	sounds[s].rawdata = newsamples;
	sounds[s].nsamples = newnsamples;
}


void sound_close()
{
	for(int i=0;i<NUM_SOUNDS;i++)
	{
		if (sounds[i].samples)
			free(sounds[i].samples);
	}
}

/*
void c------------------------------() {}
*/

static bool LoadSound(const char *sndname, Sound *snd)
{
	char fname[MAXPATHLEN];
	GetSoundFilename(sndname, fname, sizeof(fname));
	stat("LoadSound('%s')", fname);

	// ok, so this is a little odd. If the specified sound file isn't found,
	// then SDL_LoadWAV is locking up instead of returning an error, at least
	// for me. but SDL_LoadWAV is actually a macro that combines the below two
	// functions directly into each other; so we can just split them out ourselves
	// so we can check the return value both times before proceeding.
	auto *rw = SDL_RWFromFile(fname, "rb");
	if (!rw) {
		staterr("failed to open sound file '%s': %s", fname, strerror(errno));
		return 1;
	}

	SDL_AudioSpec spec;
	auto *result = SDL_LoadWAV_RW(rw, 1, &spec, &snd->rawdata, &snd->nsamples);
	if (result == nullptr) {
		staterr("failed to load sound %s", fname);
		return 1;
	}
	
	// some of the wav's are at 11025 instead of 22050, but we can fix that.
	if (spec.freq == SAMPLE_RATE / 2)
		snd->double_samples = true;
	else
		snd->double_samples = false;
	
	// if it's anything else though, we're toast.
	if (spec.format != AUDIO_U8 || \
		(spec.freq != SAMPLE_RATE && !snd->double_samples))
	{
		staterr("unexpected audio format while loading %s:", fname);
		staterr("  freq=%d format=%d", spec.freq, spec.format);
		return 1;
	}
	
	return 0;
}

// upconvert the sounds "rawdata", 8-bit mono audio at either 11025 or 22050hz,
// into the 16-bit stereo audio we use with SSLib.
static void Upconvert(Sound *snd)
{
	int length = snd->nsamples;
	bool double_samples = snd->double_samples;
	
	if (double_samples) snd->nsamples *= 2;
	snd->samples = (signed short *)malloc(snd->nsamples * 2 * sizeof(short));
	
	int outpos = 0;
	for(int i=0;i<length;i++)
	{
		int audioval = (int8_t)(snd->rawdata[i] - 0x80);
		
		audioval *= 256;
		if (audioval > 32767)  audioval = 32767;
		if (audioval < -32768) audioval = -32768;
		
		snd->samples[outpos++] = audioval;
		snd->samples[outpos++] = audioval;
		
		if (double_samples)
		{
			snd->samples[outpos++] = audioval;
			snd->samples[outpos++] = audioval;
		}
	}
}
	
/*
void c------------------------------() {}
*/

void sound(int no)
{
	if (no < 0) return;		// used by handle_snack
	
	sound_stop(no);
	sounds[no].channel = SSPlayChunk(-1, sounds[no].samples, \
				sounds[no].nsamples, (void *)(size_t)no, sound_finished_cb);
}

static void sound_finished_cb(int channel, void *sndno)
{
	int no = (int)(size_t)sndno;
	sounds[no].channel = -1;
}

/*
void c------------------------------() {}
*/

void sound_stop(int no)
{
	if (sounds[no].channel != -1)
	{
		SSAbortChannel(sounds[no].channel);
		sounds[no].channel = -1;
	}
}

bool sound_is_playing(int no)
{
	return (sounds[no].channel != -1);
}

/*
void c------------------------------() {}
*/

int lastsong = 0;

void music(int songno, bool show_loading)
{
	pmd_stop();
	if (songno == -1) return;		// -1 means stop
	
	if (show_loading)
		start_loading();

	// validate input as invalid numbers can be entered from console
	if (songno < 0 || songno >= MUSIC_NUM_TRACKS) {
		staterr("invalid music track #%d (valid range 0-%d)", songno, MUSIC_NUM_TRACKS);
		return;
	}
	
	char fname[MAXPATHLEN];
	GetMusicFilename(music_files[songno], fname, sizeof(fname));
	strcat(fname, ".pmd");
	
	if (!pmd_load(fname))
	{
		lastsong = songno;
		pmd_start();
	}
	
	if (show_loading)
		end_loading();
}

int getlastsong(void)
{
	return lastsong;
}

void setlastsong(int no)
{
	lastsong = no;
}


