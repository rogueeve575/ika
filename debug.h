
#ifndef _DEBUG_H
#define _DEBUG_H

enum
{
	DM_PIXEL,
	DM_CROSSHAIR,
	DM_XLINE,
	DM_YLINE,
	DM_BOX
};

void debug(const char *fmt, ...);
void debug_draw(void);
void debug_clear();
const char *strhex(int value);
void DrawDebugMarks(void);
void AddDebugMark(int x, int y, int x2, int y2, char type, uchar r, uchar g, uchar b);
void DebugPixel(int x, int y, uchar r, uchar g, uchar b);
void DebugCrosshair(int x, int y, uchar r, uchar g, uchar b);
void crosshair(int x, int y);
void DebugPixelNonCSF(int x, int y, uchar r, uchar g, uchar b);
void DebugCrosshairNonCSF(int x, int y, uchar r, uchar g, uchar b);
void debugVline(int x, uchar r, uchar g, uchar b);
void debugHline(int y, uchar r, uchar g, uchar b);
void DebugBox(int x1, int y1, int x2, int y2, uchar r, uchar g, uchar b);
void debugtile(int x, int y, uchar r, uchar g, uchar b);
void debugtile(int x, int y);

#endif
