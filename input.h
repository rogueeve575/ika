
#ifndef _INPUT_H
#define _INPUT_H

enum INPUTS
{
	LEFTKEY			= 0x01,
	RIGHTKEY		= 0x02,
	UPKEY			= 0x04,
	DOWNKEY			= 0x08,
	SPACEKEY		= 0x10,				// unfreeze single-step mode / scroll faster in title screen
	XKEY			= 0x20,				// nudge player to right (weird debug control left in original)
	ZKEY			= 0x40,				// thrust etc
	SKEY			= 0x80,				// show itembox / enter editor (from title screen)
	
	ESCKEY			= 0x100,			// quit
	F1KEY			= 0x200,			// apparently unused
	F2KEY			= 0x400,			// free-scroll map (debug mode)
	F3KEY			= 0x800,			// cycle player levelup (debug mode)
	F4KEY			= 0x1000,			// save anywhere (debug mode)
	
	CKEY			= 0x2000,			// single-step frames (debug mode)
	PKEY			= 0x4000,			// set player pos in level editor
	BKEY			= 0x8000,			// Block fill (level editor)
	NKEY			= 0x10000,			// New map (level editor)
	MKEY			= 0x20000,			// Set map size (level editor)
	RKEY			= 0x40000,			// Random fill (level editor)
	DELKEY			= 0x80000,			// Delete object (level editor)
	TABKEY			= 0x100000,			// Tile pickup (level editor)
	SHIFTKEY		= 0x200000,			// Add object / Save (when combined with S) (level editor)
};

extern uint32_t inputs;
extern uint32_t newinputs;
extern int last_sdl_key;

struct Mouse
{
	int	x, y;
	int xrel, yrel;
	
	int xtile, ytile;
	bool lbutton, rbutton;
	
	bool moved;
	bool btndown;
	bool btnup;
};

extern Mouse mouse;


#endif
