
#include "ika.h"
#include <stdexcept>
#include "main.fdh"

const char *data_dir = "data";
const char *graphics_dir = "data/Pbm";
const char *music_dir = "data/Pmd";
const char *sound_dir = "sound";

Game game;
DebugConsole console;

#define GAME_WAIT			(1000/50)	// sets framerate
#define VISFLAGS			(SDL_APPACTIVE | SDL_APPINPUTFOCUS)

// try to determine a reasonable default scaling factor for the
// graphicr given the user's current display resolution
int getDefaultScaleFactor() {
	auto *screen = SDL_GetVideoInfo();
	int screen_w = screen->current_w, screen_h = screen->current_h;
	stat("found display resolution %dx%d", screen_w, screen_h);

	// find the biggest possible scale that would fit on the screen
	int scale = MAXSCALE;
	while(scale > 1) {
		int effective_wd = SCREEN_WIDTH * scale,
			effective_ht = SCREEN_HEIGHT * scale;

		if (effective_wd <= screen_w && effective_ht <= screen_h)
			break;
	}

	stat("using default scaling factor %dx", scale);
	return scale;
}

///////////////////////////////////////////////////////////////////////////////

void usage() {
	stat("usage:\n\n"
" --editor, --title, --credits,        skip directly to specified game mode\n"
" --newgame, --loadgame\n"
"\n"
" -s, --scale [factor]                 set graphics scaling factor\n"
" -d, --debug                          enable debug keys\n"
"                                         C      single-step frames\n"
"                                         space  release single-step mode\n"
"                                         F2     free-scroll map\n"
"                                         F3     cycle player levelup\n"
"                                         F4     save anywhere\n"
"                                         S      enter level editor\n"
" -h, -?, --help                       this help\n"
"\n");
}

class ArgScanner {
public:
	ArgScanner(int argc, char **argv) {
		this->argc = argc;
		this->argv = argv;
	}
	const char *next() {
		return argat(cursor++);
	}
	const char *back() {
		return argat(--cursor);
	}
	const char *peek() {
		return argat(cursor);
	}
	const char *argat(int idx) {
		return (idx >= 1 && idx < argc) ? argv[idx] : nullptr;
	}
	int nextint() {
		const char *arg = next();
		if (is_string_numeric(arg, ISN_ALLOW_NEGATIVE))
			return atoi(arg);
		else
			throw std::runtime_error("numeric argument expected");
	}

private:
	char **argv = nullptr;
	int argc;
	int cursor = 1;
};

struct StartupOptions {
	int initialGameMode = GM_TITLE;
	bool loadGame = false;
	int gfxscale = 0;					// auto
	bool debugMode = false;
};

bool parseargs(int argc, char **argv, StartupOptions *opt) {
	ArgScanner args(argc, argv);

	for(;;) {
		const char *sw = args.next();
		if (!sw) break;

		if (sw[0] == '-') {		// a switch?
			do { sw++; } while(sw[0] == '-');	// remove leading dashes

			if (!strcmp(sw, "editor"))
				opt->initialGameMode = GM_EDITOR;
			else if (!strcmp(sw, "title"))		// the default
				opt->initialGameMode = GM_TITLE;
			else if (!strcmp(sw, "credits"))
				opt->initialGameMode = GM_ROLL;
			else if (!strcmp(sw, "newgame") || !strcmp(sw, "ng")) {
				// go directly into new game
				opt->initialGameMode = GM_NORMAL;
				opt->loadGame = false;
			}
			else if (!strcmp(sw, "loadgame") || !strcmp(sw, "lg")) {
				// go directly into saved game
				opt->initialGameMode = GM_NORMAL;
				opt->loadGame = true;
			}
			else if (!strcmp(sw, "scale") || !strcmp(sw, "s")) {
				opt->gfxscale = args.nextint();
				if (opt->gfxscale < 0 || opt->gfxscale > MAXSCALE) {
					staterr("invalid scaling factor %d (range 1-%d or 0 for auto)",
							opt->gfxscale, MAXSCALE);
					return 1;
				}
			}
			else if (!strcmp(sw, "d") || !strcmp(sw, "debug")) {
				opt->debugMode = true;
				stat("** Debug Mode Enabled.");
			}
			else if (!strcmp(sw, "help") || !strcmp(sw, "h") || !strcmp(sw, "?")) {
				usage();
				return 1;
			}
			else {
				staterr("unrecognized command-line switch '-%s'", sw);
				return 1;
			}
		}
		else {
			staterr("unrecognized command-line argument '%s'", sw);
			return 1;
		}
	}

	return 0;
}

StartupOptions sopt;
bool is_debugmode() {
	return sopt.debugMode;
}

int main(int argc, char **argv)
{
	try {
		if (parseargs(argc, argv, &sopt)) {
			usage();
			return 1;
		}
	}
	catch(std::runtime_error &e) {
		staterr("error parsing arguments: %s", e.what());
		return 1;
	}

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
		staterr("ack, sdl_init failed: %s.", SDL_GetError());
		return 1;
	}
	atexit(SDL_Quit);

	if (sopt.gfxscale == 0)
		sopt.gfxscale = getDefaultScaleFactor();

	if (Graphics::init(sopt.gfxscale)) return 1;
	if (input_init()) return 1;
	if (font_init()) return 1;
	if (sound_init()) return 1;
	if (pmd_init()) return 1;
	
	if (Sprites::Init()) return 1;
	if (map_init()) return 1;
	if (game.init()) return 1;
	if (script_init()) return 1;
	if (effect_init()) return 1;
	
	if (game.initlevel())
		return 1;
	
	game.running = true;
	game.setmode(sopt.initialGameMode);

	switch(sopt.initialGameMode) {
		case GM_NORMAL:
		{
			if (!sopt.loadGame)
				BeginNewGame();
			else {
				staterr("attempting to load directly into saved game");
				if (game_load()) {
					staterr("failed to load game, is there a savegame yet?");
					return 1;
				}
			}
		}
		break;

		case GM_ROLL: BeginMaruAme(); break;
		default: break;
	}
	
	gameloop();
	
	game.close();
	script_close();
	font_close();
	Graphics::close();
	pmd_close();
	sound_close();
	return 0;
}

// "MaruAme" refers to the intro credit-type scroll on new game
void BeginMaruAme() {
	StopScript();
	
	if (roll_init("Words.ptx", "MaruAme.pbm", 1, true))
	{
		staterr("failed to initilize roll");
		game.running = false;
	}
	
	game.setmode(GM_ROLL);
}

void BeginNewGame() {
	game.setmode(GM_NORMAL);
	pmd_stop();
	setlastsong(0);		// prime for "pp" command
	
	InitPlayer();
	map_reload();
	objects_load();
	game.hidescene = true;
	game.hidestatusbar = false;
	
	StartScript(2);
}

/*
void c------------------------------() {}
*/

static void run_tick()
{
static bool can_tick = true;
static bool freezeframe = false;
static int framecount = 0;

	newinputs = 0;
	input_poll();
	
	if (inputs & ESCKEY)
		game.running = false;

	if (is_debugmode()) {
		// space: release freeze-frame
		if (newinputs & SPACEKEY) {
			if (freezeframe) {
				freezeframe = false;
				can_tick = true;
			}
		}
		
		// C: frame-by-frame mode
		if ((newinputs & CKEY) && game.mode != GM_EDITOR) {
			can_tick = true;
			if (!freezeframe)
			{
				freezeframe = 1;
				framecount = 0;
			}
		}
	}
	
	if (can_tick) {
		game.tick();
		
		if (freezeframe)
		{
			char buf[1024];
			sprintf(buf, "[] Tick %d", framecount++);
			font_draw_shaded(4, (SCREEN_HEIGHT-GetFontHeight()-4), buf, 0, &greenfont);
			can_tick = false;
		}
		
		screen->Flip();
	}
	
	SDL_Delay(1);
	pmd_run();
}

void gameloop(void)
{
	uint32_t gametimer = -GAME_WAIT*10;
	while(game.running) {
		uint32_t curtime = SDL_GetTicks();
		
		if ((curtime - gametimer) >= GAME_WAIT) {
			run_tick();
			
			// try to "catch up" if something else on the system bogs us down for a moment.
			// but if we get really far behind, it's ok to start dropping frames
			if ((curtime - gametimer > (GAME_WAIT * 3)))
				gametimer = curtime;
			else
				gametimer += GAME_WAIT;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

// helper functions for various places where we need to obtain paths to various
// game data files.
static const char *get_gamefile(const char *dir,
								const char *fname, const char *ext,
								char *buffer_out, int bufsz) {
	if (!buffer_out)
		buffer_out = GetStaticStr(&bufsz);

	if (!dir[0]) dir = "./";
	const char *lastch = strchr(dir, 0) - 1;
	const char *sep = (*lastch == '/') ? "" : "/";

	snprintf(buffer_out, bufsz, "%s%s%s%s", dir, sep, fname, ext ? ext : "");
	stat("get_gamefile('%s')", buffer_out);
	return buffer_out;
}

const char *GetDataFilename(const char *fname, char *buffer, int bufsz) {
	return get_gamefile(data_dir, fname, nullptr, buffer, bufsz);
}
const char *GetGraphicsFilename(const char *fname, char *buffer, int bufsz) {
	return get_gamefile(graphics_dir, fname, nullptr, buffer, bufsz);
}
const char *GetMusicFilename(const char *fname, char *buffer, int bufsz) {
	return get_gamefile(music_dir, fname, nullptr, buffer, bufsz);
}
const char *GetSoundFilename(const char *fname, char *buffer, int bufsz) {
	return get_gamefile(sound_dir, fname, ".wav", buffer, bufsz);
}

