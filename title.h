
#ifndef _TITLE_H
#define _TITLE_H


struct Title
{
	int logo_y;
	int presented_x;
	int scroller_y;
	int scroller_x;
};


void title_tick(void);
bool title_init(void);
void title_close(void);

#endif
