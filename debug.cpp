
#include "ika.h"
#include <stdarg.h>
#include <string.h>
#include "debug.h"

#define MAX_DEBUG_MARKS		800
static struct
{
	int x, y, x2, y2;
	char type;
	uchar r, g, b;
} debugmarks[MAX_DEBUG_MARKS];

static int ndebugmarks = 0;
static StringList DebugList;
static int nextcolor = 0;

#define NUM_COLORS		6
static int colors[] = \
{
	0, 255, 0,
	255, 0, 0,
	255, 255, 0,
	0, 255, 255,
	255, 255, 255,
	255, 0, 255
};


// debug text display debug() useful for reporting game vars etc
void debug(const char *fmt, ...)
{
char buffer[128];
va_list ar;

	va_start(ar, fmt);
	vsnprintf(buffer, sizeof(buffer), fmt, ar);
	va_end(ar);
	
	DebugList.AddString(buffer);
}

void debug_draw(void)
{
	DrawDebugMarks();
	
	for(int i=0;;i++)
	{
		const char *text = DebugList.StringAt(i);
		if (!text) break;
		
		int x = (SCREEN_WIDTH - 4) - GetFontWidth(text, 0);
		int y = 4 + (i * (GetFontHeight() + 1));
		font_draw_shaded(x, y, text, 0, &greenfont);
	}
}

void debug_clear()
{
	DebugList.MakeEmpty();
	ndebugmarks = 0;
	nextcolor = 0;
}

/*
void c------------------------------() {}
*/

const char *strhex(int value)
{
	if (value < 0)
		return stprintf("-0x%02x", -value);
	else
		return stprintf("0x%02x", value);
}


/*
void c------------------------------() {}
*/

void DrawDebugMarks(void)
{
int i;
int x, y, x2, y2;
uchar r, g, b;

	for(i=0;i<ndebugmarks;i++)
	{
		x = (debugmarks[i].x >> CSF) - (map.xscroll >> CSF);
		y = (debugmarks[i].y >> CSF) - (map.yscroll >> CSF);
		x2 = (debugmarks[i].x2 >> CSF) - (map.xscroll >> CSF);
		y2 = (debugmarks[i].y2 >> CSF) - (map.yscroll >> CSF);
		r = debugmarks[i].r;
		g = debugmarks[i].g;
		b = debugmarks[i].b;
		
		switch(debugmarks[i].type)
		{
			case DM_PIXEL:
				DrawPixel(x, y, r, g, b);
			break;
			
			case DM_CROSSHAIR:
				DrawPixel(x, y, r, g, b);
				DrawPixel(x+1, y, r, g, b);
				DrawPixel(x-1, y, r, g, b);
				DrawPixel(x, y+1, r, g, b);
				DrawPixel(x, y-1, r, g, b);
			break;
			
			case DM_XLINE:
				FillRect(x, 0, x, SCREEN_HEIGHT, r, g, b);
			break;
			
			case DM_YLINE:
				FillRect(0, y, SCREEN_WIDTH, y, r, g, b);
			break;
			
			case DM_BOX:
				DrawRect(x, y, x2, y2, r, g, b);
			break;
		}
	}
}

void AddDebugMark(int x, int y, int x2, int y2, char type, uchar r, uchar g, uchar b)
{
	if (ndebugmarks >= MAX_DEBUG_MARKS)
		return;
	
	debugmarks[ndebugmarks].x = x;
	debugmarks[ndebugmarks].y = y;
	debugmarks[ndebugmarks].x2 = x2;
	debugmarks[ndebugmarks].y2 = y2;
	debugmarks[ndebugmarks].r = r;
	debugmarks[ndebugmarks].g = g;
	debugmarks[ndebugmarks].b = b;
	debugmarks[ndebugmarks].type = type;
	ndebugmarks++;
}

// draw a pixel of the specified color at [x,y] in object coordinates
void DebugPixel(int x, int y, uchar r, uchar g, uchar b)
{
	AddDebugMark(x, y, 0, 0, DM_PIXEL, r, g, b);
}

void DebugCrosshair(int x, int y, uchar r, uchar g, uchar b)
{
	AddDebugMark(x, y, 0, 0, DM_CROSSHAIR, r, g, b);
}

void crosshair(int x, int y)
{
	debugVline(x, 255, 0, 0);
	debugHline(y, 0, 255, 0);
}

void DebugPixelNonCSF(int x, int y, uchar r, uchar g, uchar b) { DebugPixel(x<<CSF,y<<CSF,r,g,b); }
void DebugCrosshairNonCSF(int x, int y, uchar r, uchar g, uchar b) { DebugCrosshair(x<<CSF,y<<CSF,r,g,b); }

void debugVline(int x, uchar r, uchar g, uchar b)
{
	AddDebugMark(x, 0, 0, 0, DM_XLINE, r, g, b);
}

void debugHline(int y, uchar r, uchar g, uchar b)
{
	AddDebugMark(0, y, 0, 0, DM_YLINE, r, g, b);
}

void DebugBox(int x1, int y1, int x2, int y2, uchar r, uchar g, uchar b)
{
	if (x2 < x1) SWAP(x1, x2);
	if (y2 < y1) SWAP(y1, y2);
	AddDebugMark(x1, y1, x2, y2, DM_BOX, r, g, b);
}

void debugtile(int x, int y, uchar r, uchar g, uchar b)
{
int x1, y1, x2, y2;

	x *= (TILE_W << CSF);
	y *= (TILE_H << CSF);
	
	x1 = x; y1 = y;
	x2 = x1 + (TILE_W << CSF);
	y2 = y1 + (TILE_H << CSF);
	AddDebugMark(x1, y1, x2, y2, DM_BOX, r, g, b);
}

void debugtile(int x, int y)
{
	uint8_t r = colors[(nextcolor * 3) + 0];
	uint8_t g = colors[(nextcolor * 3) + 1];
	uint8_t b = colors[(nextcolor * 3) + 2];
	if (++nextcolor > NUM_COLORS)
		nextcolor = 0;
	
	debugtile(x, y, r, g, b);
}








