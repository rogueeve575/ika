
#ifndef _EVENTREADER_H
#define _EVENTREADER_H

// affects highest possible event # (which is 4000, the ending script)
#define MAX_EVENTS			6001

struct Script
{
	void init();
	void freedata();
	
	// the entire "assembled" script, in bytecode
	DBuffer program;
	
	// offsets of each event. i.e. execution is started at this offset
	// within program to begin the event.
	int entrypoints[MAX_EVENTS];
	
	// the textual description that sometimes goes along with the
	// event declaration (in the dashes), for debugging.
	char *epnames[MAX_EVENTS];
};


const char *GetOpName(int opcode);

#endif
