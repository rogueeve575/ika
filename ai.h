
#ifndef _AI_H
#define _AI_H

struct ObjTable {
	void (*airoutine)(Object *o);
	int damage;
};

extern ObjTable objtable[NUM_OBJECT_TYPES];


#define pdistlx(DIST)	\
	(((o->x - (DIST)) < player->x) && (o->x + (DIST)) > player->x)

#define pdistly(DIST)	\
	(((o->y - (DIST)) < player->y) && (o->y + (DIST)) > player->y)


#define ANIMATE(SPEED, FIRSTFRAME, LASTFRAME)	do {													\
	if (++o->animtimer > SPEED)						\
	{												\
		o->animtimer = 0;							\
		 											\
		if (++o->frame > LASTFRAME)					\
			o->frame = FIRSTFRAME;					\
	}												\
} while(0)

#define ANIMATE_FWD(SPEED)	do {													\
	if (++o->animtimer > SPEED)						\
	{												\
		o->animtimer = 0;							\
		o->frame++;									\
	}	\
} while(0)

#define FACEPLAYER	do { \
	if (o->x < player->x) o->dir = RIGHT;	\
	if (o->x > player->x) o->dir = LEFT;	\
} while(0)


#define LIMITX(K)	do {						\
	if (o->xinertia > (K)) o->xinertia = (K);		\
	if (o->xinertia < -(K)) o->xinertia = -(K);		\
} while(0)

#define LIMITY(K)	do {	\
	if (o->yinertia > (K)) o->yinertia = (K);		\
	if (o->yinertia < -(K)) o->yinertia = -(K);		\
} while(0)

#define LIMIT(K)	do {	\
	LIMITX(K);		\
	LIMITY(K);		\
} while(0)

#define WENT_OFF_MAP(o)	\
	( o->x < 0 || o->x >= MAPX(map.xsize) || \
	  o->y < 0 || o->y >= MAPY(map.ysize) )


#endif
