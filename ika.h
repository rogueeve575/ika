
#ifndef _IKA_H
#define _IKA_H

#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <cassert>

#include "common/stat.h"
#include "common/misc.h"
#include "common/DBuffer.h"
#include "common/DString.h"
#include "common/StringList.h"
#include "common/InitList.h"
#include "common/llist.h"

#include "graphics/graphics.h"
#include "graphics/nxsurface.h"
#include "graphics/tileset.h"
#include "graphics/sprites.h"
#include "graphics/font.h"
#include "graphics/dirs.h"
#include "siflib/sif.h"
#include "autogen/sprites.h"
using namespace Graphics;
using Tileset::draw_tile;
using Sprites::draw_sprite;
using Sprites::draw_sprite_at_dp;
using Sprites::draw_sprite_clipped;
using Sprites::draw_sprite_clip_width;
using Sprites::draw_sprite_chopped;
using Sprites::draw_sprite_repeating_x;
#pragma GCC poison random		// changed to randrange(), so ensure we don't use the stdlib function by accident

struct Rect
{
	int x1, y1, x2, y2;
};

#include "input.h"
#include "sound.h"
#include "debug.h"
#include "console.h"
#include "game.h"
#include "map.h"
#include "object.h"
#include "caret.h"
#include "effect.h"
#include "player.h"
#include "ai.h"
#include "textbox.h"
#include "itembox.h"
#include "ironhead.h"
#include "event.h"
#include "pmd.h"
#include "datadirs.h"

void BeginMaruAme();
void BeginNewGame();
bool is_debugmode();

#endif
