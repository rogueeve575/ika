
#ifndef _MAP_H
#define _MAP_H

#define MAP_MAXSIZEX				256
#define MAP_MAXSIZEY				256

#define MAPPX(X)	( (X) * TILE_W )
#define MAPPY(Y)	( (Y) * TILE_H )
#define MAPX(X)		( MAPPX(X) << CSF )
#define MAPY(Y)		( MAPPY(Y) << CSF )

struct Map
{
	uint32_t xsize, ysize;
	int maxxscroll, maxyscroll;
	
	int xscroll, yscroll;
	
	Point target;
	bool scroll_locked;
	
	uint8_t focustype;
	uint16_t focusparm;
	
	uint8_t tiles[MAP_MAXSIZEX][MAP_MAXSIZEY];
};

enum FOCUSTYPE
{
	FOCUS_PLAYER,
	FOCUS_OBJECT,
	FOCUS_BOSS
};

extern Map map;
extern uint8_t tileattr[256];


#define TA_SOLID			0x01
#define TA_FOREGROUND		0x02

bool map_save(const char *fname);

#endif
