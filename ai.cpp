
#include "ika.h"
#include "ai.h"
#include "ai.fdh"

extern ObjTable objtable[NUM_OBJECT_TYPES];

void run_ai(void)
{
	Object *o;
	FOREACH_OBJECT(o)
	{
		if (objtable[o->type].airoutine)
			(*objtable[o->type].airoutine)(o);
	}
}

// for editor
void ai_init_frame(Object *o)
{
	if (objtable[o->type].airoutine)
	{
		Object save = *o;
		
		o->state = 0;
		o->frame = 0;
		
		(*objtable[o->type].airoutine)(o);
		int frame = o->frame;
		
		*o = save;
		o->frame = frame;
	}
}

int getx2frame(Object *o, int factor)
{
	int frame = (((o->x2 - o->x) >> CSF) / 16) * factor;
	
	if (frame < 0) frame = 0;
	if (frame >= sprites[objprop[o->type].sprite].nframes)
		frame = sprites[objprop[o->type].sprite].nframes - 1;
	
	return frame;
}

/*
void c------------------------------() {}
*/

static void ai_hari(Object *o)
{
	switch(o->state)
	{
		case 0:
		{
			if (o->x > o->x2) o->xinertia -= 6;
			if (o->x < o->x2) o->xinertia += 6;
			if (o->y > o->y2) o->yinertia -= 4;
			if (o->y < o->y2) o->yinertia += 4;
			
			if (o->xinertia < 0) o->dir = LEFT;
			if (o->xinertia > 0) o->dir = RIGHT;
			
			ANIMATE(60, 0, 1);
			
			LIMITX(0x800);
			LIMITY(0x800);
			
			o->Move();
			
			if (o->timer > 0)
				o->timer--;
			
			if (o->timer == 0 && !(player->equipmask & EQUIP_KANI_SPRAY))
			{
				if (pdistlx(40<<CSF) && pdistly(40<<CSF))
				{
					if (o->ty == TY_ENEMY)
					{
						o->state = 1;
						o->timer = 300;
					}
				}
			}
		}
		break;
		
		case 1:
		{
			FACEPLAYER;
			ANIMATE(2, 2, 3);
			
			o->timer--;
			if (o->timer < 200)
			{
				o->state = 0;
				o->frame = 0;
			}
		}
		break;
	}
}



static void ai_isogin(Object *o)
{
	if (o->state == 0)
	{
		o->state = 2;	// because state 1 means 'attacking'
		o->frame = getx2frame(o, 2);
	}
	
	if (o->yinertia < 0x800)
	{
		o->yinertia += 20;
	}
	
	o->Move();
	
	if (o->x < player->x) o->dir = RIGHT;
	if (o->x > player->x) o->dir = LEFT;
	
	if (++o->animtimer > 40)
	{
		o->animtimer = 0;
		o->frame ^= 1;
	}
}

static void ai_carry(Object *o)
{
	FACEPLAYER;
	ANIMATE(40, 0, 1);
}


static void ai_kani(Object *o)
{
	//debug("%s: %d", strhex(o->yinertia), o->state);
	switch(o->state)
	{
		case 0:
		{
			if (o->x < (o->x2 - (40 << CSF)))
				o->dir = RIGHT;
			
			if (o->x > (o->x2 + (40 << CSF)))
				o->dir = LEFT;
			
			if (o->dir == LEFT)
			{
				if (o->xinertia > -0x200)
					o->xinertia -= 0x10;
			}
			
			if (o->dir == RIGHT)
			{
				if (o->xinertia < 0x200)
					o->xinertia += 0x10;
			}
			
			if (o->blockd)
				o->timer++;
			
			if (o->timer > 80)
			{
				o->timer = 0;
				
				if (o->ty == TY_ENEMY && !(player->equipmask & EQUIP_KANI_SPRAY))
				{
					o->yinertia = -0x600;
					o->state = 1;
				}
				else
				{
					o->yinertia = -0x400;
				}
			}
			
			if (++o->animtimer > 10)
			{
				o->animtimer = 0;
				o->frame++;
			}
			if (o->frame > 1) o->frame = 0;
			
			if (++o->yinertia < 0x800)
				o->yinertia += 0x14;
			
			o->Move();
			
			if (!o->blockd)
				o->frame = 2;
		}
		break;
		
		case 1:		// jumping/flashing
		{
			ANIMATE(2, 2, 3);
			
			if (o->x < (o->x2 - (40 << CSF)))
				o->dir = RIGHT;
			
			if (o->x > (o->x2 + (40 << CSF)))
				o->dir = LEFT;
			
			if (o->dir == LEFT)
			{
				if (o->xinertia > -0x200)
					o->xinertia -= 0x10;
			}
			
			if (o->dir == RIGHT)
			{
				if (o->xinertia < 0x200)
					o->xinertia += 0x10;
			}
			
			if (++o->yinertia < 0x800)
				o->yinertia += 0x14;
			
			o->Move();
			
			if (o->yinertia >= 0)
				o->state = 0;
		}
		break;
	}
	
}


static void ai_chibi(Object *o)
{
	if (o->x > player->x) o->xinertia -= 0x18;
	if (o->x < player->x) o->xinertia += 0x18;
	if (o->y > player->y) o->yinertia -= 0x10;
	if (o->y < player->y) o->yinertia += 0x10;
	
	FACEPLAYER;
	if (pdistlx(0x8000) && pdistly(0x8000))
		o->dir = UP;
	
	ANIMATE(8, 0, 1);
	
	LIMIT(0x400);
	o->Move();
}


static void ai_hoshi(Object *o)
{
	ai_chibi(o);
	
	// set attacking...
	if ((o->y + (8<<CSF)) > player->y)
	{
		o->state = 1;
	}
	else
	{
		o->state = 0;
	}
}



static void ai_juel(Object *o)
{
	if (o->state != 0) return;
	
	if (o->x > o->x2) o->xinertia -= 0x0C;
	if (o->x < o->x2) o->xinertia += 0x0C;
	
	if (o->y > o->y2) o->yinertia -= 0x08;
	if (o->y < o->y2) o->yinertia += 0x08;
	
	if (o->xinertia < 0) o->dir = LEFT;
	if (o->xinertia > 0) o->dir = RIGHT;
	
	if (++o->animtimer > 30)
	{
		o->animtimer = 0;
		o->frame ^= 1;
	}
	
	LIMITX(0x800);
	LIMITY(0x800);
	
	o->Move();
}

// signs
static void ai_sign(Object *o)
{
	if (o->state == 0)
	{
		o->state = 2;	// because state 1 means 'attacking'
		o->frame = getx2frame(o, 1);
	}
	
	if (o->yinertia < 0x800)
		o->yinertia += 20;
	
	o->Move();
}


static void ai_save(Object *o)
{
	if (o->yinertia < 0x800)
		o->yinertia += 20;
	
	o->Move();
	
	if (pdistlx(32<<CSF) && pdistly(32<<CSF))
	{
		if (++o->animtimer > 2)
		{
			o->animtimer = 0;
			if (++o->frame > 3) o->frame = 3;
		}
	}
	else if (o->frame > 0)
	{
		if (++o->animtimer > 5)
		{
			o->animtimer = 0;
			o->frame--;
		}
	}
	else
	{
		o->animtimer = 0;
	}
}

/*
void c------------------------------() {}
*/

ObjTable objtable[NUM_OBJECT_TYPES] =
{
//	AI			DMG
	ai_hari,	1,
	ai_isogin,	0,
	ai_kani,	2,
	ai_save,	0,
	ai_chibi,	4,
	ai_hoshi,	2,
	ai_sign,	0,
	ai_carry,	0,
	ai_juel,	1,
	NULL,		0	// ship
};


