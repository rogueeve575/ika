
#ifndef _TEXTBOX_H
#define _TEXTBOX_H

#define MSG_W				300
#define MSG_H				48
#define MSG_X				8
#define MSG_Y				((SCREEN_HEIGHT - MSG_H) - 8)

#define MSG_NLINES			2
#define MSG_MAXCHARS		34

enum
{
	YES,
	NO
};

struct TextLine
{
	char text[MSG_MAXCHARS];
	int len;
};

class TextBox
{
public:
	bool Init();
	void Reset();
	
	void SetVisible(bool enable);
	
	void AddText(const char *str);
	void SetText(const char *str);
	void ClearText();
	
	bool IsVisible();
	bool IsBusy();
	
	void Run();
	void Draw();
	
	void WaitForKey(void);
	void Newline(void);
	void ShowYNPrompt(int scriptOnYes, int initialSel);
	
	void EditorPrompt(const char *text, bool initialSel, void (*OnYes)());
	void (*fCallbackOnYes)();
	
private:
	void AddNextChar(void);
	void DrawYesNo(void);
	void RunYesNo(void);
	
	bool fVisible;
	
	// currently visible lines
	TextLine fLine[MSG_NLINES];
	int fCurLine;
	
	// for when lines are scrolling
	bool fScrolling;
	int fTextYOffset;
	
	// chars queued waiting to be visible
	char fCharsWaiting[256];
	uint8_t fCWHead, fCWTail;
	
	int fTextTimer;
	bool fSpedUp;
	
	bool fWaitingKey;
	bool fClearAfterNOD;
	int fAnimTimer;
	int fAnimFrame;
	
	bool fYNPrompt;
	bool fYNSel;
	int fScriptOnYes;
	
	// editor prompt stuff
	bool fEditorPrompt;
};

extern TextBox textbox;

#endif

