
#include "ika.h"
#include "map.h"
#include "map.fdh"

Map map;
NXSurface *backdrop;
uint8_t tileattr[256];
SDL_Color map_palette[256] =
{
	#include "palette.inc"
};

static const char *get_map_fname() {
	static const char *MAPFILE_NAME = "Map1.pbm";
	static char fname[MAXPATHLEN] = {0};
	if (!fname[0]) {
		// yes...the map is located in the graphics directory, because it's
		// actually stored as a bitmap image
		GetGraphicsFilename(MAPFILE_NAME, fname, sizeof(fname));
	}
	return fname;
}

bool map_init(void)
{
static bool notfirsttime = false;

	memset(&map, 0, sizeof(map));
	if (!notfirsttime) {
		notfirsttime = true;
		create_attr_table();
	}
	
	Tileset::Load(0x20, "Prtfilt.pbm");
	Tileset::Load(0x40, "PrtItem.pbm");
	Tileset::Load(0x60, "PrtDir.pbm");
	Tileset::Load(0x80, "PrtBlock.pbm");
	Tileset::Load(0xA0, "PrtDmg.pbm");
	Tileset::Load(0xE0, "PrtSnack.pbm");
	
	if (map_reload())
		return 1;

	// load backdrop image
	char fname[MAXPATHLEN];
	GetGraphicsFilename("Back.pbm", fname, sizeof(fname));
	backdrop = NXSurface::FromFile(fname);
	if (!backdrop)
	{
		staterr("failed to load backdrop!");
		return 1;
	}
	
	return 0;
}

void create_attr_table(void)
{
uint8_t tiletype = 0;

	for(;;)
	{
		switch(tiletype & 0xF0)
		{
			default: tileattr[tiletype] = 0; break;
			
			case 0x20: tileattr[tiletype] = TA_FOREGROUND; break;
			case 0x60: tileattr[tiletype] = TA_FOREGROUND; break;
			case 0x80: tileattr[tiletype] = TA_SOLID; break;
			case 0xE0: tileattr[tiletype] = TA_SOLID; break;
		}
		
		if (++tiletype == 0)
			break;
	}
}

bool map_reload(void)
{
SDL_Surface *mapimage;

	const char *fname = get_map_fname();
	mapimage = SDL_LoadBMP(fname);
	if (!mapimage)
	{
		staterr("map_load: failed to open %s", fname);
		return 1;
	}
	
	map.xsize = mapimage->w;
	map.ysize = mapimage->h;
	stat("map: %dx%d, %dbpp", map.xsize, map.ysize, mapimage->format->BitsPerPixel);
	
	for(int y=0;y<map.ysize;y++)
	{
		uint8_t *ptr = (uint8_t *)mapimage->pixels + (y * mapimage->pitch);
		
		for(int x=0;x<map.xsize;x++)
		{
			map.tiles[x][y] = *(ptr++);
		}
	}
	
	map_resize(map.xsize, map.ysize);
	SDL_FreeSurface(mapimage);
	return 0;
}

bool map_save(void)
{
SDL_Surface *mapimage;
char fname[MAXPATHLEN];
	
	stat("Saving map...");
	mapimage = SDL_CreateRGBSurface(SDL_SWSURFACE, map.xsize, map.ysize, 8, 0, 0, 0, 0);
	if (!mapimage)
	{
		staterr("Failed to create surface!");
		return 1;
	}
	
	SDL_SetColors(mapimage, map_palette, 0, 256);
	
	for(int y=0;y<map.ysize;y++)
	{
		uint8_t *ptr = (uint8_t *)mapimage->pixels + (y * mapimage->pitch);
		
		for(int x=0;x<map.xsize;x++)
		{
			*(ptr++) = map.tiles[x][y];
		}
	}
	
	if (SDL_SaveBMP(mapimage, get_map_fname()))
	{
		staterr("Failed to save surface!");
		SDL_FreeSurface(mapimage);
		return 1;
	}
	
	SDL_FreeSurface(mapimage);
	return 0;
}

/*
void c------------------------------() {}
*/

// draw the map.
// 	if foreground = TA_FOREGROUND, draws the foreground tile layer.
//  if foreground = 0, draws backdrop and background tiles.
void map_draw(uint8_t foreground)
{
int x, y;
int mapx, mapy;
int blit_x, blit_y, blit_x_start;
int scroll_x, scroll_y;
	
	scroll_x = (map.xscroll >> CSF);
	scroll_y = (map.yscroll >> CSF);
	
	mapx = (scroll_x / TILE_W);
	mapy = (scroll_y / TILE_H);
	
	blit_y = -(scroll_y % TILE_H);
	blit_x_start = -(scroll_x % TILE_W);
	
	for(y=0; y <= (SCREEN_HEIGHT / TILE_H); y++)
	{
		blit_x = blit_x_start;
		
		for(x=0; x <= (SCREEN_WIDTH / TILE_W); x++)
		{
			int t = map.tiles[mapx+x][mapy+y];
			if (t)
			{
				if ((tileattr[t] & foreground) == foreground)
					draw_tile(blit_x, blit_y, t);
			}
			
			blit_x += TILE_W;
		}
		
		blit_y += TILE_H;
	}
}


void map_draw_backdrop(void)
{
int x, y;
int parscroll_x, parscroll_y;

	int w = backdrop->Width();
	int h = backdrop->Height();
	
	parscroll_x = (map.xscroll >> CSF) / 2;
	parscroll_y = (map.yscroll >> CSF) / 2;
	parscroll_x %= w;
	parscroll_y %= h;
	
	for(y=0;y<SCREEN_HEIGHT+parscroll_y; y+=h)
	{
		for(x=0;x<SCREEN_WIDTH+parscroll_x; x+=w)
		{
			DrawSurface(backdrop, x - parscroll_x, y - parscroll_y);
		}
	}
	
}

/*
void c------------------------------() {}
*/

void map_resize(int xa, int ya)
{
	map.xsize = xa;
	map.ysize = ya;
	map.maxxscroll = (((map.xsize * TILE_W) - SCREEN_WIDTH) - 8) << CSF;
	map.maxyscroll = (((map.ysize * TILE_H) - SCREEN_HEIGHT) - 8) << CSF;
}

static void update_scroll_target(void)
{
	switch(map.focustype)
	{
		case FOCUS_PLAYER:
			map.target.x = player->x;
			map.target.y = player->y;
		break;
		
		case FOCUS_OBJECT:
			map.target.x = objects[map.focusparm].x;
			map.target.y = objects[map.focusparm].y;
		break;
		
		case FOCUS_BOSS:
			map.target.x = ironh.x;
			map.target.y = ironh.y;
		break;
	}
	
	map.target.x += (8 << CSF);
	map.target.y += (8 << CSF);
	
	map.target.x -= ((SCREEN_WIDTH / 2) << CSF);
	map.target.y -= ((SCREEN_HEIGHT / 2) << CSF);
}


void map_scroll_do(void)
{
	update_scroll_target();
	
	if ((abs(map.target.x - map.xscroll) >= (1 << CSF)) || \
		(abs(map.target.y - map.yscroll) >= (1 << CSF)))
	{
		map.xscroll += (map.target.x - map.xscroll) / 16;
		map.yscroll += (map.target.y - map.yscroll) / 16;
	}
	
	//run_phase_compensator();
	map_sanitycheck();
}


// scroll position sanity checking
void map_sanitycheck(void)
{
	#define MAP_BORDER_AMT		(8<<CSF)
	
	if (map.xscroll < MAP_BORDER_AMT) map.xscroll = MAP_BORDER_AMT;
	if (map.xscroll > map.maxxscroll) map.xscroll = map.maxxscroll;
	
	if (map.yscroll < MAP_BORDER_AMT) map.yscroll = MAP_BORDER_AMT;
	if (map.yscroll > map.maxyscroll) map.yscroll = map.maxyscroll;
}


void map_scroll_jump(int x, int y)
{
	map.target.x = x;
	map.target.y = y;
	map.xscroll = x;
	map.yscroll = y;
	
	map_sanitycheck();
}

void map_centeron(int x, int y)
{
	map_scroll_jump(x - ((SCREEN_WIDTH / 2) << CSF),
					y - ((SCREEN_HEIGHT / 2) << CSF));
}

// lock the scroll in it's current position. the target position will not change,
// however if the scroll is moved off the target (really only a quake could do this)
// the map will still seek it's old position.
void map_scroll_lock(bool lockstate)
{
	map.scroll_locked = lockstate;
	if (lockstate)
	{
		map.xscroll = map.target.x;
		map.yscroll = map.target.y;
	}
}


