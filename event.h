
#ifndef _EVENT_H
#define _EVENT_H

#define MAX_PARAMS		3

struct ScriptCPU
{
	int curscript;
	uint8_t *program;		// pointer to program code. NULL if no script is running.
	int ip;					// program counter
	
	int waittime;			// ticks remaining to wait on a <wa
};

struct ScriptCommand
{
	int op;
	int nparams;
	char textbuffer[10000];
	
	union
	{
		uint16_t parm[MAX_PARAMS];
		struct { uint16_t p1, p2, p3; };
	};
};

enum Opcodes
{
	OP_TEXT,		// psuedo-op, added by reader
	OP_NEWLINE,		// '+', text newline
	OP_NOD,			// '/', text newline + wait for keypress
	
	// the rest of the events are bracketed (prefixed by '<')
	OP_PC,			// change tile at X,Y to Z
	OP_END,			// end event
	OP_SE,			// play sound X
	OP_WA,			// wait X ticks
	OP_YN,			// prompt yes/no, go to event X if yes
	
	OP_FPLUS,		// set flag X
	OP_IJ,			// jump to event Y if have item X
	OP_EPLUS,		// restore energy and play sound SND_LIFEUP
	OP_FJ,			// jump to event Y if flag X is set
	
	OP_BO,			// start boss fight XXXX
	OP_CE,			// reset enemies (reload NPChar.dat)
	OP_CM,			// move object X to coordinates Y:Z
	OP_DL,			// load game and play SND_SAVE
	OP_DS,			// save game and play SND_SAVE
	OP_EC,			// increment entity counter and play SND_ITEM
	OP_EJ,			// if entity counter == X, jump to event Y
	OP_EK,			// earthquake
	OP_EX,			// exit game
	OP_FI,			// fade in
	OP_FO,			// fade out
	OP_FT,			// set scroll focus
	OP_GE,			// blast up (play sound 11), ignoring solid tiles (rocket)
	OP_IMINUS,		// remove item X
	OP_IPLUS,		// give item X and play SND_ITEM
	OP_LE,			// long earthquake
	OP_MA,			// disable scene rendering (all except statusbar & textbox)
	OP_NM,			// enable scene rendering
	
	OP_PD,			// set music to X
	OP_PF,			// fade music to silence
	OP_PH,			// fade music to quiet
	OP_PN,			// upfade music to loud
	OP_PP,			// start/reset music
	OP_PS,			// stop music
	
	OP_RE,			// restore character
	OP_CRE,			// '*', go to credits
	
	OP_BRBR,		// "<<" erroneous command in credits
};


bool script_init(void);
void script_close(void);
void StartScript(int no);
void StopScript(void);
void JumpScript(int no);
void WaitScript(int time);
int GetCurrentScript();
void run_events(void);
bool ExecCommand(ScriptCommand *cmd);
void PrintCommand(ScriptCommand *cmd, int ip);
int FetchCommand(const uint8_t *program, int pos, ScriptCommand *cmd);
void fix_spelling(char *buffer);

#endif
