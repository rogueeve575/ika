
#ifndef _SOUND_H
#define _SOUND_H

enum
{
	SND_NODMG			= 0,
	SND_PROPEL			= 1,
	SND_OUCH			= 2,
	SND_ITEM			= 3,
	SND_HITHEAD			= 4,
	SND_BREAK_BLOCK		= 5,
	SND_READY			= 6,
	SND_GO				= 7,
	SND_MESSAGE			= 8,
	SND_DEAD 			= 9,
	SND_KILL_ENEMY		= 10,
	SND_LEVELUP			= 11,
	SND_LIFEUP			= 12,
	SND_SAVE			= 13,
	SND_YESNO			= 14,
	SND_QUAKE			= 15,
	
	SND_BOSSOUCH		= 20,
	NUM_SOUNDS
};

bool sound_init();
void sound_close();
void sound(int no);
void sound_stop(int no);
bool sound_is_playing(int no);
void music(int no, bool show_loading);
int getlastsong(void);
void setlastsong(int no);

#endif
