
all:	ika

ika:  main.o game.o object.o map.o \
	 input.o player.o ai.o caret.o event.o \
	 eventreader.o textbox.o itembox.o effect.o ironhead.o \
	 roll.o title.o editor/editor.o editor/dropdown.o editor/tilemode.o \
	 editor/spritemode.o graphics/graphics.o graphics/font.o graphics/nxsurface.o graphics/tileset.o \
	 graphics/sprites.o sslib.o sound.o pmd.o console.o \
	 debug.o autogen/AssignSprites.o siflib/sif.o siflib/sifloader.o siflib/sectSprites.o \
	 siflib/sectStringArray.o common/InitList.o common/StringList.o common/DBuffer.o common/DString.o \
	 common/bufio.o common/stat.o common/misc.o
	g++ -o ika \
	 main.o game.o object.o map.o \
	 input.o player.o ai.o caret.o event.o \
	 eventreader.o textbox.o itembox.o effect.o ironhead.o \
	 roll.o title.o editor/editor.o editor/dropdown.o editor/tilemode.o \
	 editor/spritemode.o graphics/graphics.o graphics/font.o graphics/nxsurface.o graphics/tileset.o \
	 graphics/sprites.o sslib.o sound.o pmd.o console.o \
	 debug.o autogen/AssignSprites.o siflib/sif.o siflib/sifloader.o siflib/sectSprites.o \
	 siflib/sectStringArray.o common/InitList.o common/StringList.o common/DBuffer.o common/DString.o \
	 common/bufio.o common/stat.o common/misc.o \
	 `sdl-config --libs` -lSDL_ttf

main.o:	main.cpp main.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c main.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o main.o

game.o:	game.cpp game.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		roll.h eventreader.h title.h \
		editor/editor.h editor/dropdown.h editor/tilemode.h \
		editor/spritemode.h
	g++ -g -O2 -c game.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o game.o

object.o:	object.cpp object.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c object.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o object.o

map.o:	map.cpp map.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		palette.inc
	g++ -g -O2 -c map.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o map.o

input.o:	input.cpp input.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c input.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o input.o

player.o:	player.cpp player.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c player.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o player.o

ai.o:	ai.cpp ai.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c ai.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o ai.o

caret.o:	caret.cpp caret.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c caret.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o caret.o

event.o:	event.cpp event.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		eventreader.h
	g++ -g -O2 -c event.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o event.o

eventreader.o:	eventreader.cpp eventreader.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		eventreader.h
	g++ -g -O2 -c eventreader.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o eventreader.o

textbox.o:	textbox.cpp textbox.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c textbox.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o textbox.o

itembox.o:	itembox.cpp itembox.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c itembox.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o itembox.o

effect.o:	effect.cpp effect.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c effect.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o effect.o

ironhead.o:	ironhead.cpp ironhead.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c ironhead.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o ironhead.o

roll.o:	roll.cpp roll.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		roll.h eventreader.h
	g++ -g -O2 -c roll.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o roll.o

title.o:	title.cpp title.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		title.h
	g++ -g -O2 -c title.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o title.o

editor/editor.o:	editor/editor.cpp editor/editor.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		editor/editor.h editor/dropdown.h editor/tilemode.h \
		editor/spritemode.h
	g++ -g -O2 -c editor/editor.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o editor/editor.o

editor/dropdown.o:	editor/dropdown.cpp editor/dropdown.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		editor/dropdown.h
	g++ -g -O2 -c editor/dropdown.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o editor/dropdown.o

editor/tilemode.o:	editor/tilemode.cpp editor/tilemode.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		editor/editor.h editor/dropdown.h editor/tilemode.h \
		editor/spritemode.h
	g++ -g -O2 -c editor/tilemode.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o editor/tilemode.o

editor/spritemode.o:	editor/spritemode.cpp editor/spritemode.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h \
		eventreader.h editor/editor.h editor/dropdown.h \
		editor/tilemode.h editor/spritemode.h
	g++ -g -O2 -c editor/spritemode.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o editor/spritemode.o

graphics/graphics.o:	graphics/graphics.cpp graphics/graphics.fdh graphics/graphics.h graphics/nxsurface.h \
		common/basics.h common/stat.h
	g++ -g -O2 -c graphics/graphics.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o graphics/graphics.o

graphics/font.o:	graphics/font.cpp graphics/font.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c graphics/font.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o graphics/font.o

graphics/nxsurface.o:	graphics/nxsurface.cpp graphics/nxsurface.fdh graphics/nxsurface.h common/basics.h \
		common/stat.h
	g++ -g -O2 -c graphics/nxsurface.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o graphics/nxsurface.o

graphics/tileset.o:	graphics/tileset.cpp graphics/tileset.fdh graphics/graphics.h graphics/nxsurface.h \
		common/basics.h common/stat.h graphics/dirs.h \
		datadirs.h graphics/tileset.h
	g++ -g -O2 -c graphics/tileset.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o graphics/tileset.o

graphics/sprites.o:	graphics/sprites.cpp graphics/sprites.fdh graphics/graphics.h graphics/nxsurface.h \
		common/basics.h common/stat.h siflib/sif.h \
		siflib/sifloader.h common/BList.h siflib/sectSprites.h \
		siflib/sectStringArray.h common/StringList.h common/DString.h \
		common/DBuffer.h datadirs.h graphics/sprites.h
	g++ -g -O2 -c graphics/sprites.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o graphics/sprites.o

sslib.o:	sslib.cpp sslib.fdh common/basics.h common/stat.h \
		sslib.h
	g++ -g -O2 -c sslib.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o sslib.o

sound.o:	sound.cpp sound.fdh common/basics.h common/stat.h \
		sslib.h sound.h datadirs.h
	g++ -g -O2 -c sound.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o sound.o

pmd.o:	pmd.cpp pmd.fdh common/basics.h common/stat.h \
		datadirs.h sslib.h pmd.h
	g++ -g -O2 -c pmd.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o pmd.o

console.o:	console.cpp console.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c console.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o console.o

debug.o:	debug.cpp debug.fdh ika.h common/stat.h \
		common/misc.h common/DBuffer.h common/basics.h \
		common/DString.h common/StringList.h common/BList.h \
		common/InitList.h common/llist.h graphics/graphics.h \
		graphics/nxsurface.h graphics/tileset.h graphics/sprites.h \
		siflib/sif.h graphics/font.h graphics/dirs.h \
		autogen/sprites.h input.h sound.h \
		debug.h console.h game.h \
		map.h object.h caret.h \
		effect.h player.h ai.h \
		textbox.h itembox.h ironhead.h \
		event.h pmd.h datadirs.h
	g++ -g -O2 -c debug.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o debug.o

autogen/AssignSprites.o:	autogen/AssignSprites.cpp autogen/AssignSprites.fdh autogen/asdefs.h ika.h \
		common/stat.h common/misc.h common/DBuffer.h \
		common/basics.h common/DString.h common/StringList.h \
		common/BList.h common/InitList.h common/llist.h \
		graphics/graphics.h graphics/nxsurface.h graphics/tileset.h \
		graphics/sprites.h siflib/sif.h graphics/font.h \
		graphics/dirs.h autogen/sprites.h input.h \
		sound.h debug.h console.h \
		game.h map.h object.h \
		caret.h effect.h player.h \
		ai.h textbox.h itembox.h \
		ironhead.h event.h pmd.h \
		datadirs.h
	g++ -g -O2 -c autogen/AssignSprites.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o autogen/AssignSprites.o

siflib/sif.o:	siflib/sif.cpp siflib/sif.fdh siflib/sif.h common/stat.h \
		siflib/sifloader.h common/BList.h siflib/sectSprites.h \
		siflib/sectStringArray.h
	g++ -g -O2 -c siflib/sif.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o siflib/sif.o

siflib/sifloader.o:	siflib/sifloader.cpp siflib/sifloader.fdh common/basics.h common/stat.h \
		siflib/sifloader.h common/BList.h
	g++ -g -O2 -c siflib/sifloader.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o siflib/sifloader.o

siflib/sectSprites.o:	siflib/sectSprites.cpp siflib/sectSprites.fdh common/DBuffer.h common/basics.h \
		common/stat.h common/bufio.h siflib/sectSprites.h \
		siflib/sif.h
	g++ -g -O2 -c siflib/sectSprites.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o siflib/sectSprites.o

siflib/sectStringArray.o:	siflib/sectStringArray.cpp siflib/sectStringArray.fdh common/DBuffer.h common/basics.h \
		common/stat.h common/DString.h common/bufio.h \
		common/StringList.h common/BList.h siflib/sectStringArray.h
	g++ -g -O2 -c siflib/sectStringArray.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o siflib/sectStringArray.o

common/InitList.o:	common/InitList.cpp common/InitList.fdh common/InitList.h common/BList.h
	g++ -g -O2 -c common/InitList.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o common/InitList.o

common/StringList.o:	common/StringList.cpp common/StringList.fdh common/DString.h common/basics.h \
		common/stat.h common/DBuffer.h common/StringList.h \
		common/BList.h
	g++ -g -O2 -c common/StringList.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o common/StringList.o

common/DBuffer.o:	common/DBuffer.cpp common/DBuffer.fdh common/DBuffer.h common/basics.h \
		common/stat.h
	g++ -g -O2 -c common/DBuffer.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o common/DBuffer.o

common/DString.o:	common/DString.cpp common/DString.fdh common/DString.h common/basics.h \
		common/stat.h common/DBuffer.h
	g++ -g -O2 -c common/DString.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o common/DString.o

common/bufio.o:	common/bufio.cpp common/bufio.fdh common/DBuffer.h common/basics.h \
		common/stat.h
	g++ -g -O2 -c common/bufio.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o common/bufio.o

common/stat.o:	common/stat.cpp common/stat.fdh common/basics.h common/stat.h \
		common/misc.h
	g++ -g -O2 -c common/stat.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o common/stat.o

common/misc.o:	common/misc.cpp common/misc.fdh common/basics.h common/stat.h \
		common/misc.h
	g++ -g -O2 -c common/misc.cpp `sdl-config --cflags` -Wmain -Wsequence-point -Wswitch -Wtrigraphs -Wuninitialized -Wmaybe-uninitialized -Wno-unused-result -Wno-multichar -Wunused-variable -Wunused-but-set-variable -Wunused-label -Wstrict-overflow -Wpointer-arith -Wtype-limits -Wno-empty-body -Wparentheses -Werror=return-type -Wcomment -Wdeprecated-declarations -Wformat -no-pie -Wreturn-type -Wno-unused-variable -Wno-multichar -o common/misc.o

.FORCE:

clean:
	rm -f main.o
	rm -f game.o
	rm -f object.o
	rm -f map.o
	rm -f input.o
	rm -f player.o
	rm -f ai.o
	rm -f caret.o
	rm -f event.o
	rm -f eventreader.o
	rm -f textbox.o
	rm -f itembox.o
	rm -f effect.o
	rm -f ironhead.o
	rm -f roll.o
	rm -f title.o
	rm -f editor/editor.o
	rm -f editor/dropdown.o
	rm -f editor/tilemode.o
	rm -f editor/spritemode.o
	rm -f graphics/graphics.o
	rm -f graphics/font.o
	rm -f graphics/nxsurface.o
	rm -f graphics/tileset.o
	rm -f graphics/sprites.o
	rm -f sslib.o
	rm -f sound.o
	rm -f pmd.o
	rm -f console.o
	rm -f debug.o
	rm -f autogen/AssignSprites.o
	rm -f siflib/sif.o
	rm -f siflib/sifloader.o
	rm -f siflib/sectSprites.o
	rm -f siflib/sectStringArray.o
	rm -f common/InitList.o
	rm -f common/StringList.o
	rm -f common/DBuffer.o
	rm -f common/DString.o
	rm -f common/bufio.o
	rm -f common/stat.o
	rm -f common/misc.o
	rm -f ika

cleanfdh:
	rm -f main.fdh
	rm -f game.fdh
	rm -f object.fdh
	rm -f map.fdh
	rm -f input.fdh
	rm -f player.fdh
	rm -f ai.fdh
	rm -f caret.fdh
	rm -f event.fdh
	rm -f eventreader.fdh
	rm -f textbox.fdh
	rm -f itembox.fdh
	rm -f effect.fdh
	rm -f ironhead.fdh
	rm -f roll.fdh
	rm -f title.fdh
	rm -f editor/editor.fdh
	rm -f editor/dropdown.fdh
	rm -f editor/tilemode.fdh
	rm -f editor/spritemode.fdh
	rm -f graphics/graphics.fdh
	rm -f graphics/font.fdh
	rm -f graphics/nxsurface.fdh
	rm -f graphics/tileset.fdh
	rm -f graphics/sprites.fdh
	rm -f sslib.fdh
	rm -f sound.fdh
	rm -f pmd.fdh
	rm -f console.fdh
	rm -f debug.fdh
	rm -f autogen/AssignSprites.fdh
	rm -f siflib/sif.fdh
	rm -f siflib/sifloader.fdh
	rm -f siflib/sectSprites.fdh
	rm -f siflib/sectStringArray.fdh
	rm -f common/InitList.fdh
	rm -f common/StringList.fdh
	rm -f common/DBuffer.fdh
	rm -f common/DString.fdh
	rm -f common/bufio.fdh
	rm -f common/stat.fdh
	rm -f common/misc.fdh

cleanall: clean cleanfdh

