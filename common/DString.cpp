
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "DString.h"
#include "DString.fdh"


DString::DString()
{
}

DString::DString(const char *str)
{
	SetTo(str);
}

DString::DString(const char *str, int length)
{
	SetTo(str, length);
}

DString::DString(DString &other)
{
	SetTo(other.String(), other.Length());
}

/*
void c------------------------------() {}
*/

void DString::SetTo(const char *str, int length)
{
	fBuffer.SetTo((uint8_t *)str, length);
}

void DString::SetTo(const char *str)
{
	fBuffer.SetTo((uint8_t *)str, strlen(str));
}

void DString::SetTo(DString *other)
{
	fBuffer.SetTo(other->fBuffer.Data(), other->fBuffer.Length());
}

void DString::SetTo(DString &other)
{
	fBuffer.SetTo(other.fBuffer.Data(), other.fBuffer.Length());
}

/*
void c------------------------------() {}
*/

void DString::AppendString(const char *str)
{
	fBuffer.AppendData((uint8_t *)str, strlen(str));
}

void DString::AppendString(const char *str, int length)
{
	fBuffer.AppendData((uint8_t *)str, length);
}

void DString::AppendChar(uchar ch)
{
	fBuffer.AppendData((uint8_t *)&ch, 1);
}

/*
void c------------------------------() {}
*/

int DString::ReplaceString(const char *from, const char *to)
{
	if (!from[0])
	{
		staterr("attempt to replace empty string (with '%s') ignored", to);
		return false;
	}
	
	DString newstr;
	const char *str = String();
	int fromlen, tolen;	// we'll only init these once we know there are hits

	int numhits = 0;
	int seekindex = 0;
	for(;;)
	{
		// find the next hit in the remaining unprocessed part of the string
		const char *rest = str + seekindex;
		const char *hitptr = strstr(rest, from);
		if (!hitptr)
		{	// no more hits in string
			if (numhits)
			{
				newstr.AppendString(rest);
				SetTo(newstr);
			}
			
			return numhits;
		}
		
		// calculate these only once we know we'll need to
		// (if there are no hits in string, we wouldn't need to)
		if (!numhits++)
		{
			fromlen = strlen(from);
			tolen = strlen(to);
		}
		
		int hitindex = (hitptr - rest);
		//stat("seekindex = %d, hitindex = %d, fromlen = %d, tolen = %d, rest = '%s'", seekindex, hitindex, fromlen, tolen, rest);
		
		// add the "rest" string we searched -- up to the hit index
		// test is just to eliminate needless concats of 0-length strings
		// (e.g. if "str" starts with "from" or contains "fromfrom")
		if (hitindex != 0) newstr.AppendString(rest, hitindex);

		// adding "to" instead of "from"
		newstr.AppendString(to, tolen);
		
		// next time, continue processing at hitindex + skip the len of from
		seekindex += (hitindex + fromlen);
	}
}

/*{
	char *str = String();
	char *hit = strstr(str, oldstring);
	if (!hit) return;	// avoid strlens in common case of no hits
	
	int oldlen = strlen(oldstring);
	int newlen = strlen(newstring);
	
	for(;;)
	{
		DString temp(str, (hit - str));
		temp.AppendString(newstring);
		temp.AppendString(hit + oldlen);
		
		SetTo(temp.String());
		
		int index = (hit - str) + newlen;
		str = String() + index;
		
		hit = strstr(str, oldstring);
		if (!hit) break;
	}
}*/

/*
void c------------------------------() {}
*/

void DString::EnsureAlloc(int min_required)
{
	fBuffer.EnsureAlloc(min_required);
}

void DString::ReplaceUnprintableChars()
{
	fBuffer.ReplaceUnprintableChars();
}

/*
void c------------------------------() {}
*/

void DString::Clear()
{
	fBuffer.Clear();
}

int DString::Length()
{
	return fBuffer.Length();
}

char *DString::String()
{
	return fBuffer.String();
}
	

