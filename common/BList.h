
#ifndef __BLIST2_H
#define __BLIST2_H

#include <vector>

// shim implementation of the old BList for back-compat in a few places such as
// in siflib and StringList; the original BList was taken from MIT-licensed code
// from the Haiku project but is a pretty simple class; this re-implements the
// same basic interface using a std::vector

class BList
{
public:
	BList() {}
	BList(int allocSize) {
		items.reserve(allocSize);
	}
	void AddItem(void *item);
	void *RemoveItem(int index);
	bool RemoveItem(void *item);
	void MakeEmpty();
	
	void *ItemAt(int index) const;
	void *FirstItem() const;
	void *LastItem() const;
	int IndexOf(void *item) const;

	bool HasItem(void *item) const;
	int CountItems() const;
	bool IsEmpty() const;
	
private:
	bool inrange(int index) const;
	std::vector<void *> items;
};


inline bool BList::inrange(int index) const {
	return (index >= 0 && index < items.size());
}

inline void BList::AddItem(void *item) {
	items.push_back(item);
}

inline void BList::MakeEmpty() {
	items.clear();
}

inline void *BList::ItemAt(int index) const {
	return inrange(index) ? items[index] : nullptr;
}

inline void *BList::FirstItem() const {
	return items.size() ? items[0] : nullptr;
}

inline void *BList::LastItem() const {
	int size = items.size();
	return size ? items[size - 1] : nullptr;
}

inline int BList::IndexOf(void *item) const {
	int i, sz = items.size();
	for(i=0;i<sz;i++) {
		if (items[sz] == item) return i;
	}
	return -1;
}
inline bool BList::HasItem(void *item) const {
	for(auto *chk : items) {
		if (chk == item) return true;
	}
	return false;
}

inline void *BList::RemoveItem(int index) {
	void *removedItem = nullptr;
	if (inrange(index)) {
		removedItem = items[index];
		auto it = items.begin() + index;
		items.erase(it);
	}
	return removedItem;
}
inline bool BList::RemoveItem(void *item) {
	int idx = IndexOf(item);
	if (idx >= 0) {
		RemoveItem(idx);
		return true;
	}
	return false;
}

inline int BList::CountItems() const {
	return items.size();
}

inline bool BList::IsEmpty() const {
	return items.size() == 0;
}

///////////////////////////////////////////////////////////////////////////////
// now we put our own List<> template class over the basic void * BList
///////////////////////////////////////////////////////////////////////////////

enum ListOptions {
	LIST_DELETE_ON_DESTROY	= 0x10
};

// use this wrapper around the old BList on all new code. it provides type-safety and prevents having to cast
// when accessing items via e.g. ItemAt() so is much nicer. it will store a list of pointers to whatever type
// you give it in the <>
template<class T>
class List : BList		// make BList an inaccessible base or else you could e.g. AddItem(void *) and break type-safety
{
public:
	List() {
		deleteContentsOnDestroy = false;
	}
	List(int count)
		: BList(count)
	{
		deleteContentsOnDestroy = false;
	}
	List(ListOptions options)
	{
		deleteContentsOnDestroy = (options & LIST_DELETE_ON_DESTROY);
	}
	//List(ListOptions options, int count)
		//: BList(count)
	//{
		//deleteContentsOnDestroy = (options & LIST_DELETE_ON_DESTROY);
	//}
	//List(List<T> &anotherList)
		//: BList(anotherList)
	//{
		//deleteContentsOnDestroy = false;
	//}
	~List()
	{
		if (deleteContentsOnDestroy)
			DeleteContents();
	}
	
	inline void SetAutoDestroy(bool enable = true) {
		deleteContentsOnDestroy = enable;
	}
	
	inline void AddItem(T* item) {
		BList::AddItem((void*)item);
	}
	//inline void AddItem(T* item, int index) {
		//BList::AddItem((void*)item, index);
	//}
	
	//inline void AddList(const List<T> *list) {
		//BList::AddList(list);
	//}
	//inline void AddList(const List<T> *list, int index) {
		//BList::AddList(list, index);
	//}
	
	inline T* ItemAt(int index) const {
		return (T*)BList::ItemAt(index);
	}
	
	int CountItems() const {
		return BList::CountItems();
	}
	
	void MakeEmpty() {
		BList::MakeEmpty();
	}
	
	bool IsEmpty() const {
		return BList::IsEmpty();
	}
	
	inline T* FirstItem() const {
		return (T*)BList::FirstItem();
	}
	
	inline T* LastItem() const {
		return (T*)BList::LastItem();
	}
	
	inline T* RemoveItem(int index) {
		return (T*)BList::RemoveItem(index);
	}
	
	inline bool RemoveItem(T* item) {
		return BList::RemoveItem((void *)item);
	}

	//inline bool RemoveItems(int index, int count) {
		//return BList::RemoveItems(index, count);
	//}

	//inline bool DeleteItems(int index, int count) {
		//int end = index + (count - 1), ci;
		//if (end >= (ci = CountItems())) end = ci - 1;
		//for(int i = ((index >= 0) ? index : 0); i<=end; i++) delete ItemAt(i);
		//return RemoveItems(index, count);
	//}
	
	//inline bool ReplaceItem(int index, T* newItem) {
		//return BList::ReplaceItem(index, newItem);
	//}
	
	inline T* PopItem() {
		return RemoveItem(CountItems() - 1);
	}
	
	int IndexOf(void* item) const {
		return BList::IndexOf(item);
	}
	inline bool HasItem(T* item) const {
		return BList::HasItem(item);
	}
	
	//inline T** ToArray() {
		//return (T**)BList::Items();
	//}
	
	//inline void SortItems(int (*compareFunc)(T* a, T* b)) {
		//currentCompareFunc = compareFunc;
		//BList::SortItems([](const void *pa, const void *pb) -> int {
			//return currentCompareFunc(*((T**)pa), *((T**)pb));
		//});
	//}
	//inline bool SwapItems(int32_t indexA, int32_t indexB) {
		//return BList::SwapItems(indexA, indexB);
	//}
	//inline bool MoveItem(int32_t fromIndex, int32_t toIndex) {
		//return BList::MoveItem(fromIndex, toIndex);
	//}
	
	inline void DeleteContents(void)
	{
		for(auto *item : *this)
			delete item;
		
		MakeEmpty();
	}
	
	// find the index of an item simply by it's pointer.
	inline int FindItem(T* item) const {
		return BList::IndexOf(item);
	}
	
	bool Equals(List<T> &other) const { return Equals(*other); }
	bool Equals(List<T> *other) const
	{
		if (CountItems() != other->CountItems()) return false;
		for(int i=0;;i++)
		{
			auto *a = ItemAt(i);
			if (!a) return true;
			if (a != other->ItemAt(i)) return false;
		}
	}
	
	inline T* operator[] (int index) { return ItemAt(index); }
	inline bool operator== (const List<T> &other) const { return Equals(*other); }
	inline bool operator!= (const List<T> &other) const { return !Equals(*other); }

public:
	bool deleteContentsOnDestroy;
	
	class iterator
	{
		public:
			iterator(const List<T> *list, int startIndex) {
				this->list = list;
				index = startIndex;
			}
			
			iterator &operator++() {
				index++;
				return *this;
			}
			iterator &operator--() {
				index--;
				return *this;
			}
			iterator operator++(int) {
				iterator temp = *this;
				index++;
				return temp;
			}
			iterator operator--(int) {
				iterator temp = *this;
				index--;
				return temp;
			}
			
			bool operator== (const iterator &other) const {
				return index == other.index;
			}
			bool operator!= (const iterator &other) const {
				return index != other.index;
			}
			bool operator> (const iterator &other) const {
				return index > other.index;
			}
			bool operator< (const iterator &other) const {
				return index < other.index;
			}
			bool operator>= (const iterator &other) const {
				return index >= other.index;
			}
			bool operator<= (const iterator &other) const {
				return index <= other.index;
			}
			iterator operator= (const iterator &other) {
				list = other.list;
				index = other.index;
				return *this;
			}
			
			T* operator*() const {
				return list->ItemAt(index);
			}
		
		private:
			int index;
			const List<T> *list;
	};
	
	iterator begin() const {
		return iterator(this, 0);
	}
	iterator end() const {
		return iterator(this, CountItems());
	}
};

#endif

