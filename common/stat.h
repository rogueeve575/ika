
#ifndef __STAT_H
#define __STAT_H

void SetLogFilename(const char *fname);
void writelog(const char *buf, bool append_cr);
void stat(const char *fmt, ...);
char *stprintf(const char *fmt, ...);

#define staterr(...)	_staterr(__FILE__, __FUNCTION__, __VA_ARGS__)
void _staterr(const char *file, const char *function, const char *fmt, ...);

#endif

