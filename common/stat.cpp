
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#include "basics.h"
#include "stat.h"
#include "misc.h"

#define MAXBUFSIZE		1024
char logfilename[64] = { 0 };
void writelog(const char *buf, bool append_cr);


void SetLogFilename(const char *fname)
{
	maxcpy(logfilename, fname, sizeof(logfilename));
	remove(logfilename);
	
	stat("Log set %d", time(NULL));
}

void writelog(const char *buf, bool append_cr)
{
FILE *fp;

	fp = fopen(logfilename, "a+");
	if (fp)
	{
		fputs(buf, fp);
		if (append_cr) fputc('\n', fp);
		
		fclose(fp);
	}
}

/*
void c------------------------------() {}
*/

void stat(const char *fmt, ...)
{
va_list ar;
char buffer[MAXBUFSIZE];

	va_start(ar, fmt);
	vsnprintf(buffer, sizeof(buffer), fmt, ar);
	va_end(ar);
	
	puts(buffer);
	fflush(stdout);
	
	if (logfilename[0])
		writelog(buffer, true);
}

void _staterr(const char *file, const char *function, const char *fmt, ...)
{
va_list ar;
char buffer[MAXBUFSIZE];

	va_start(ar, fmt);
	vsnprintf(buffer, sizeof(buffer), fmt, ar);
	va_end(ar);

	stat("\e[1;93m %s \e[91m<<\e[97m %s: %s \e[91m>>\e[0m", file, function, buffer);
}


char *stprintf(const char *fmt, ...)
{
va_list ar;
char *str = GetStaticStr();

	va_start(ar, fmt);
	vsnprintf(str, 255, fmt, ar);
	va_end(ar);
	
	return str;
}

