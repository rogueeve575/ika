
#ifndef _BASICS_H
#define _BASICS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/param.h>	// MAXPATHLEN
#include <errno.h>
typedef unsigned char		uchar;
#define UNUSED(x) UNUSED_ ## x __attribute__((unused))

#include "stat.h"

#define SWAP(A, B)	{ A ^= B; B ^= A; A ^= B; }

#ifndef MIN
#define MIN(A, B)	( ( (A) < (B) ) ? (A) : (B) )
#endif

#ifndef MAX
#define MAX(A, B)	( ( (A) > (B) ) ? (A) : (B) )
#endif

#endif
