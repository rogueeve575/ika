
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <vector>
#include <cassert>

#include "DString.h"
#include "StringList.h"
#include "StringList.fdh"
#include "basics.h"

StringList::StringList() {
	_init();
}
StringList::StringList(int preAllocCount)
	: items(preAllocCount)
{
	_init();
}
StringList::StringList(const char *str) {
	_init(str);
}
StringList::StringList(const char *str, int length) {
	_init(str, length);
}
StringList::StringList(DString *str) {
	_init(str->String(), str->Length());
}
StringList::StringList(DString &str) {
	_init(str.String(), str.Length());
}
StringList::StringList(StringList &other) {
	for(auto *str : other) AddString(str);
}
StringList::StringList(StringList *other) {
	for(auto *str : *other) AddString(str);
}

// construct a StringList from a plain array of strings.
// you can either specify the number of strings in the list, or you can pass a NULL terminated list too -
// if you pass nitems as -1, it will stop once it encounters a NULL string in the array.
StringList::StringList(const char **array, int nitems)
{
	AddArray(array, nitems);
}

/*StringList::StringList(const char *splitString, const char *delim) {
	_init();
	AddSplitString(splitString, delim);
}*/

void StringList::_init(const char *contents, int len)
{
	if (contents)
	{
		if (len < 0) len = strlen(contents);
		AddString(contents, len);
	}
}

StringList::~StringList()
{
	MakeEmpty();
}

/*
void c------------------------------() {}
*/

int StringList::Split(const char *splitString, const char *delim, bool ignore_empty)
{
	char *orgstr = strdup(splitString);
	int delim_len = strlen(delim);
	
	/* TODO: can we use the AddString(str, len) function to do this
	   without actually having to split up the string with NULs, and thus avoid the strdup()?
	   as well as having to strlen each string?
	
	   I won't try it just yet until it can be properly tested, but I'm thinking
	   something like:
	//
		int len = ptr ? (ptr - str) : strlen(str);
		AddString(str, len);
	*/
	
	int orgcount = CountItems();
	char *str = orgstr;
	for(;;)
	{
		char *ptr = strstr(str, delim);
		if (ptr) *ptr = 0;
		
		//stat("Adding string -%s-", str);
		
		int len = ptr ? (ptr - str) : strlen(str);
		//assert(len == strlen(str));		// testing mode to see if we can do this safely
		
		if (len || !ignore_empty)
			AddString(str, len);
		
		if (!ptr) break;
		str = ptr + delim_len;
	}
	
	free(orgstr);
	return CountItems() - orgcount;
}

// basically a repeat of the other Split(), except we use strchr instead of
// delim, and don't have to strlen() the length of delim
int StringList::Split(const char *splitString, char delim, bool ignore_empty)
{
	char *orgstr = strdup(splitString);
	
	int orgcount = CountItems();
	char *str = orgstr;
	for(;;)
	{
		char *ptr = strchr(str, delim);
		if (ptr) *ptr = 0;
		
		int len = ptr ? (ptr - str) : strlen(str);
		//assert(len == strlen(str));		// testing mode to see if we can do this safely
		
		if (len || !ignore_empty)
			AddString(str, len);
		
		if (!ptr) break;
		str = ptr + 1;
	}
	
	free(orgstr);
	return CountItems() - orgcount;
}

// ABI back-compat
int StringList::Split(const char *splitString, const char *delim) {
	return Split(splitString, delim, false);
}
int StringList::Split(const char *splitString, char delim) {
	return Split(splitString, delim, false);
}

// return a static string that joins the contents of the table, seperated by delim.
// e.g. "a, b, c". delim defaults to comma and specifies the ", " between items.
// last_delim defaults to NULL, but if specified, can be used to make e.g. "a, b, and c"-type lists
//void StringList::Join(DString *out, const char *delim, const char *last_delim,
					//const char *surroundPre, const char *surroundPost)
//{
	//if (surroundPre && !surroundPost)
		//surroundPost = surroundPre;

	//int delim_length;
	//if (delim)
		//delim_length = strlen(delim);		// save some time for each Append
	//else {
		//delim = ", ";
		//delim_length = 2;
	//}
	
	//int nitems = CountItems();
	//int lastItem = nitems - 1;
	//for(int i=0;i<nitems;i++)
	//{
		//if (i != 0)
		//{
			//if (i == lastItem && last_delim)
				//out->Append(last_delim);
			//else
				//out->Append(delim, delim_length);
		//}
		
		//if (surroundPre)
			//out->Append(surroundPre);

		// support having NULL strings in the StringList - not a very common thing to do,
		// but we'll support it.
		//const char *str = StringAt(i);
		//if (!str) str = "(null)";
		//out->Append(str);

		//if (surroundPost)
			//out->Append(surroundPost);
	//}
//}

//void StringList::Join(DString *out, char delim)
//{
	// TODO: more optimized version of the above Join() function that can skip the strlen() and the
	// last_delim bit? it wouldn't optimize much, so just something to consider.
	//char ch[2] = { delim, 0 };
	//Join(out, ch, NULL);
//}

//const char *StringList::Join(char delim) {
	//DString out;
	//Join(&out, delim);
	//return out.StaticString();
//}
//const char *StringList::Join(const char *delim, const char *last_delim,
						//const char *surroundPre, const char *surroundPost) {
	//DString out;
	//Join(&out, delim, last_delim, surroundPre, surroundPost);
	//return out.StaticString();
//}

// back-compat
//void StringList::Join(DString *out, const char *delim, const char *last_delim) {
	//Join(out, delim, last_delim, NULL, NULL);
//}
//const char *StringList::Join(const char *delim, const char *last_delim) {
	//return Join(delim, last_delim, NULL, NULL);
//}

/*
void c------------------------------() {}
*/

bool StringList::ContainsString(const char *term)
{
const char *str;

	for(int i=0; (str = StringAt(i)); i++)
	{
		if (!strcmp(str, term))
			return true;
	}
	
	return false;
}

bool StringList::ContainsCaseString(const char *term)
{
const char *str;

	for(int i=0; (str = StringAt(i)); i++)
	{
		if (!strcasecmp(str, term))
			return true;
	}
	
	return false;
}

bool StringList::ContainsString(DString *_term) {
	return ContainsString(_term->String());
}
bool StringList::ContainsString(DString &_term) {
	return ContainsString(_term.String());
}
bool StringList::ContainsCaseString(DString *_term) {
	return ContainsCaseString(_term->String());
}
bool StringList::ContainsCaseString(DString &_term) {
	return ContainsCaseString(_term.String());
}

int StringList::IndexOfString(const char *term)
{
char *str;

	for(int i=0; (str = StringAt(i)); i++)
	{
		if (!strcmp(str, term))
			return i;
	}
	
	return -1;
}

/*
void c------------------------------() {}
*/

void StringList::AddString(const char *str)
{
	items.AddItem((char *)strdup(str));
}
//void StringList::AddStringAt(const char *str, int position)
//{
	//items.AddItem((char *)strdup(str), position);
//}

void StringList::AddString(const char *str, int len)
{
	if (len < 0) len = strlen(str);
	char *temp = (char *)malloc(len + 1);
	memcpy(temp, str, len);
	temp[len] = 0;
	
	items.AddItem(temp);
}

void StringList::AddString(DString *str) {
	AddString(str->String(), str->Length());
}
void StringList::AddString(DString &str) {
	AddString(str.String(), str.Length());
}

// NOTE: when using this function; you MUST have already strdup()'d or otherwise allocated the string on the heap,
// using C-style functions, not "new" operator, or BAD things will happen later, such as on MakeEmpty, or in the destructor.
void StringList::AddStringNoCopy(char *str)
{
	items.AddItem((char *)str);
}

void StringList::AddArray(const char **array, int nitems)
{
	if (nitems < 0)
	{
		for(int i=0;array[i];i++)
			AddString(array[i]);
	}
	else
	{
		for(int i=0;i<nitems;i++)
			AddString(array[i]);
	}
}

void StringList::Concat(const StringList *other) {
	int i, len = other->CountItems();
	for(i=0;i<len;i++)
		AddString(other->StringAt(i));
}

/*
void c------------------------------() {}
*/

// removes the string at the given index from the list, returns zero on success or nonzero if index was out-of-bounds
bool StringList::RemoveString(int index)
{
	char *str = StringAt(index);
	if (str)
	{
		items.RemoveItem(index);
		free(str);
		return 0;
	}
	
	return 1;
}

const char *StringList::RemoveAndGetStringAt(int index)
{
	char *str = StringAt(index);
	if (str)
	{
		items.RemoveItem(index);
		return str;
	}
	
	return NULL;
}


// removes occurances of the given string from the list, if any, and returns number of removed items
// all = true: remove ALL occurances, all = false, remove the first occurance
int StringList::RemoveString(const char *str, bool all)
{
int removedCount = 0;
char *entry;

	for(int i=0; (entry = StringAt(i)); i++)
	{
		if (!strcmp(entry, str))
		{
			items.RemoveItem(i--);
			free(entry);
			removedCount++;
			if (!all) break;
		}
	}
	
	return removedCount;
}

// same as RemoveString, but is case-insensitive
int StringList::RemoveCaseString(const char *str, bool all)
{
int removedCount = 0;
char *entry;

	for(int i=0; (entry = StringAt(i)); i++)
	{
		if (!strcasecmp(entry, str))
		{
			items.RemoveItem(i--);
			free(entry);
			removedCount++;
			if (!all) break;
		}
	}
	
	return removedCount;
}

/*bool StringList::ReplaceString(int index, const char *newstring)
{
	char *str = StringAt(index);
	if (!str) return 1;					// index out of bounds. return error.
	if (str == newstring) return 0;		// strings are literally the same string, we need do nothing.
	
	// save pointer to old string so we know whether or not we need to update pointer in list at the end
	char *orgstrptr = str;
	
	int oldcopylen = strlen(str) + 1;
	int copylen = strlen(newstring) + 1;
	
	// if strings contain identical contents. we need do nothing.
	// since we know the length we can use memcmp which is likely faster that strcmp since it doesn't have to check for 0 char.
	// subtract 1 from the length to compare because there's no point in checking the 0 char we know they both have at the end.
	//
	// TODO: does this really save any time, doing a memcmp and then exiting, vs. doing a needless memmove -
	// whereas in the majority of cases where the strings are not identical, we will be wasting time doing a memcmp
	// if the lengths are the same.
	//if (oldcopylen == copylen && !memcmp(str, newstring, copylen - 1))
	//	return 0;
	
	// if lengths are the same, then we can re-use the same pointer, no need to realloc.
	// this "optimization" however, does require an extra strlen - which may nullify the advantage we gain
	// of not having realloc copy the string
	if (copylen != oldcopylen)
	{
		// growing string?
		if (copylen > oldcopylen)
		{
			// don't have realloc needlessly copy the contents of the str we're about to overwrite.
			// this does however, mean that it's possible that realloc could have grown the buffer without changing it's start point,
			// and we'll miss out on that, and be more likely to end up with a different ptr, necessitating calling items.ReplaceItem().
			// however, the latter call is probably quite cheap - and cheaper than the memcpy() that realloc would have done.
			free(str);
			str = NULL;		// essentially turn the realloc into a malloc
		}
		// ^ else we're shrinking string, and there's a good chance that realloc will be able to return the same base ptr.
		
		// allocate enough space for the new string and it's NUL terminator
		// we are assuming here, that when realloc is asked to *reduce* the length, that it simply decrements the stored length
		// of the buffer at the given pointer, and thus doesn't copy anything.
		str = (char *)realloc(str, copylen);
	}
	
	// copy the new string plus it's NUL terminator into the buffer, whether that be the same or a new buffer.
	// - use memmove, in case the new string is within a portion of the old string.
	memmove(str, newstring, copylen);
	
	// if realloc changed the pointer to str, we need to update the pointer in the list.
	// otherwise, we have simply changed the contents and possibly size that the existing pointer points to, so we're done.
	if (str != orgstrptr)
		items.ReplaceItem(index, str);
	
	return 0;
}*/

/*
void c------------------------------() {}
*/

//void StringList::SwapItems(int index1, int index2) {
	//items.SwapItems(index1, index2);
//}

void StringList::Dump()
{
int count = CountItems();

	stat("= StringList %p; %d entries =", this, count);
	for(int i=0;i<count;i++)
	{
		char *str = StringAt(i);
		stat("   (index %d @ <%p>): '%s'", i, str, str ? str : "(null)");
	}
	
	stat("");
}

/*
void c------------------------------() {}
*/

char *StringList::StringAt(int index) const
{
	return items.ItemAt(index);
}

int32_t StringList::CountItems() const
{
	return items.CountItems();
}

void StringList::MakeEmpty()
{
	for(auto *str : items)
		free(str);
	
	items.MakeEmpty();
}

/*
void c------------------------------() {}
*/

StringList &StringList::operator= (const StringList &other)
{
	StringList *self = (StringList *)this;
	StringList *other2 = (StringList *)&other;
	
	MakeEmpty();
	for(auto *str : *other2) self->AddString(str);
	
	return *this;
}

bool StringList::Equals(StringList *other)
{
	if (CountItems() != other->CountItems())
		return false;
	
	for(int i=0;;i++)
	{
		char *str1 = StringAt(i);
		char *str2 = other->StringAt(i);
		
		if (!str1)		// reached end of list (we already know both lists are the same size)
			return true;
		
		if (strcmp(str1, str2) != 0)
			return false;
	}
}

bool StringList::Equals(StringList &other) {
	return Equals(&other);
}

bool StringList::operator== (const StringList &other) const {
	StringList *self = (StringList *)this;		// casting away const on this (this is safe, we don't modify anything)
	return self->Equals((StringList *)&other);
}
bool StringList::operator!= (const StringList &other) const {
	StringList *self = (StringList *)this;		// casting away const on this (this is safe, we don't modify anything)
	return !self->Equals((StringList *)&other);
}

// shortcut to StringAt using array notation
const char* StringList::operator[] (int index) {
	return StringAt(index);
}

