
#ifndef _LIBKT_STRINGLIST_H
#define _LIBKT_STRINGLIST_H

#include "BList.h"
#include "DString.h"

class StringList
{
public:
	StringList();													// create an empty list with default pre-allocation
	StringList(int preAllocCount);									// create an empty list with given pre-allocation
	StringList(const char *str);									// create a list with a single item
	StringList(const char *str, int length);						// create a list with a single item, size known
	StringList(DString *str);										// create a list with a single item from a DString ptr
	StringList(DString &str);										// create a list with a single item from a DString reference
	StringList(const char **array, int nitems = -1);				// create a list from an array of strings
//	StringList(const char *splitString, const char *delim);			// create a list from a single string, splitting it at "delim"
	StringList(StringList &other);									// create a list from a reference to another StringList
	StringList(StringList *other);									// create a list from a pointer to another Stringlist
	
	~StringList();
	
	void AddString(const char *str);
	void AddString(const char *str, int len);
	void AddString(DString *str);
	void AddString(DString &str);
	//void AddStringAt(const char *str, int position);
	void Concat(const StringList *other);

	// add an array, either by specifying the number of items, or, if nitems is omitted, assuming that the array is NULL-terminated
	void AddArray(const char **array, int nitems = -1);

	char *StringAt(int index) const;
	void MakeEmpty();
	
	// split a string on the given delimiter, and append each of the split components to the StringList.
	// returns the number of items added.
	int Split(const char *splitString, const char *delim, bool ignore_empty);
	int Split(const char *splitString, char delim, bool ignore_empty);
	int Split(const char *splitString, const char *delim);
	int Split(const char *splitString, char delim);
	
	// combine a list into a single string. the optional "last_delim" can be used to delimit the last entry in the StringList.
	// this is useful for easily making joins such as "x, y, z, and w" (in that example, delim is ", " and last_delim is ", and ")
	void Join(DString *out, const char *delim, const char *last_delim,
						const char *surroundPre, const char *surroundPost = NULL);
	const char *Join(const char *delim, const char *last_delim,
						const char *surroundPre, const char *surroundPost = NULL);

	// back-compat
	void Join(DString *out, const char *delim = NULL, const char *last_delim = NULL);
	const char *Join(const char *delim = NULL, const char *last_delim = NULL);

	// single-char versions
	void Join(DString *out, char delim);
	const char *Join(char delim);
	
	// NOTE: when using this function; use must have already strdup() or otherwise allocated the string on the heap,
	// or BAD things will happen on MakeEmpty which is also called by the destructor
	void AddStringNoCopy(char *str);
	
	bool ContainsString(const char *term);
	bool ContainsString(DString *term);
	bool ContainsString(DString &term);
	
	bool ContainsCaseString(const char *term);
	bool ContainsCaseString(DString *term);
	bool ContainsCaseString(DString &term);
	
	// remove and free string at given index. returns 1 if index was out of range.
	bool RemoveString(int index);
	// remove and free occurances of given string (first occurance only if all=false, else remove all occurances).
	// returns the number of strings removed.
	int RemoveString(const char *str, bool all = true);
	// the same as RemoveString, except the match is case-insensitive.
	int RemoveCaseString(const char *str, bool all = true);
	// remove string and return the string to the caller. the returned string will need to be free()d by the caller.
	const char *RemoveAndGetStringAt(int index);
	
	int IndexOfString(const char *term);
	
	//bool ReplaceString(int index, const char *newstring);
	//void SwapItems(int index1, int index2);
	void Dump();
	
	int32_t CountItems() const;
	
	// returns an array (not-guaranteed-to-be-NULL-terminated, you must use CountItems() to know it's length),
	// containing all the strings in the StringList
	//const char **ToArray();
	
	bool Equals(StringList *other);
	bool Equals(StringList &other);
	StringList &operator= (const StringList &other);
	bool operator== (const StringList &other) const;
	bool operator!= (const StringList &other) const;
	const char* operator[] (int index);

private:
	void _init(const char *initial_contents = NULL, int contents_len = -1);
	List<char> items;	// looks weird, but because of the semantics of List<T>, this is a list of char *
	
	class iterator
	{
		public:
			iterator(const StringList *list, int startIndex) {
				this->list = list;
				index = startIndex;
			}
			
			iterator &operator++()
			{
				index++;
				return *this;
			}
			
			const char* operator*() const {
				return list->StringAt(index);
			}
			
			bool operator!= (const iterator &other) const {
				return index != other.index;
			}
		
		private:
			int index;
			const StringList *list;
	};
	
public:
	iterator begin() const {
		return iterator(this, 0);
	}
	
	iterator end() const {
		return iterator(this, CountItems());
	}
};


#endif

