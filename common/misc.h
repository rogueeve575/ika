
#ifndef __MISC_H
#define __MISC_H

uint16_t fgeti(FILE *fp);
uint32_t fgetl(FILE *fp);
void fputi(uint16_t word, FILE *fp);
void fputl(uint32_t word, FILE *fp);
uint16_t fgeti(FILE *fp);
uint32_t fgetl(FILE *fp);
void fputi(uint16_t word, FILE *fp);
void fputl(uint32_t word, FILE *fp);
double fgetfloat(FILE *fp);
void fputfloat(double q, FILE *fp);
void freadstring(FILE *fp, char *buf, int max);
void fputstring(char *buf, FILE *fp);
void fputstringnonull(const char *buf, FILE *fp);
bool fverifystring(FILE *fp, const char *str);
void fgetline(FILE *fp, char *str, int maxlen);
int filesize(FILE *fp);
bool file_exists(const char *fname);
int randrange(int min, int max);
uint32_t getrand();
void seedrand(uint32_t newseed);
bool strbegin(const char *bigstr, const char *smallstr);
bool strcasebegin(const char *bigstr, const char *smallstr);
int count_string_list(const char *list[]);
char *GetStaticStr(int *size_out = nullptr);
void maxcpy(char *dst, const char *src, int maxlen);
void fresetboolean(void);
char fbooleanread(FILE *fp);
void fbooleanwrite(char bit, FILE *fp);
void fbooleanflush(FILE *fp);

enum ISNFlags {	// flags for is_string_numeric
	ISN_POSITIVE_INTEGERS_ONLY = 0,
	ISN_ALLOW_DECIMAL_POINT = 0x1,
	ISN_ALLOW_NEGATIVE = 0x2,
	ISN_ALLOW_HEX = 0x4,		// allow string to start with "0x", "0b", "0o", etc.
};

bool is_string_numeric(const char *str, ISNFlags flags = ISN_POSITIVE_INTEGERS_ONLY);

#endif

