
#include "ika.h"
#include "ironhead.h"
#include "event.h"		// StartScript()

Ironhead ironh;


void init_ironhead()
{
	memset(&ironh, 0, sizeof(ironh));
	ironh.exists = true;
	
	ironh.hp = 15;
	ironh.touchscript = 1000;
	ironh.defeatscript = 1001;
	
	ironh.x = MAPX(131);
	ironh.y = MAPY(90);
	ironh.x2 = MAPX(134);
	ironh.y2 = MAPY(91);
	
	ironh.bbox.x1 = (16<<CSF);
	ironh.bbox.y1 = (2<<CSF);
	ironh.bbox.x2 = (48<<CSF);
	ironh.bbox.y2 = (22<<CSF);
}

void draw_ironhead()
{
int headframe;

	if (!ironh.exists)
		return;
	
	// flashing for attack state
	if (ironh.state == 2)
		headframe = (ironh.flashtimer & 1);
	else
		headframe = 0;
	
	// draw
	int scr_x = (ironh.x >> CSF) - (map.xscroll >> CSF);
	int scr_y = (ironh.y >> CSF) - (map.yscroll >> CSF);
	
	if (ironh.dir == LEFT)
	{
		draw_sprite(scr_x, scr_y, SPR_IRONHEAD, headframe, LEFT);
		draw_sprite(scr_x+32, scr_y, SPR_IRONHEAD, ironh.frame, LEFT);
	}
	else
	{
		draw_sprite(scr_x+32, scr_y, SPR_IRONHEAD, headframe, RIGHT);
		draw_sprite(scr_x, scr_y, SPR_IRONHEAD, ironh.frame, RIGHT);
	}
}


void run_ironhead()
{
Ironhead *o = &ironh;
CaretMaker *cm;

	if (o->hurttimer)
		o->hurttimer--;
	
	if (o->flashtimer)
		o->flashtimer--;
	
	//debug("state: %d", o->state);
	//debug("hp: %d", o->hp);
	
	switch(o->state)
	{
		case 0:
		{
			if (o->x > o->x2) o->xinertia -= 6;
			if (o->x < o->x2) o->xinertia += 6;
			if (o->y > o->y2) o->yinertia -= 4;
			if (o->y < o->y2) o->yinertia += 4;
			
			if ((o->xinertia < 0 && o->dir == RIGHT) || \
				(o->xinertia > 0 && o->dir == LEFT))
			{
				o->dir ^= 1;
				
				cm = CreateCaretMaker(CAR_BUBBLE, 5);
				if (o->dir == LEFT)
				{
					cm->x = o->x + (48<<CSF);
					cm->min_xinertia = 0x200;
					cm->max_xinertia = 0x400;
				}
				else
				{
					cm->x = o->x + (16<<CSF);
					cm->min_xinertia = -0x400;
					cm->max_xinertia = -0x200;
				}
				
				cm->y = o->y + (12<<CSF);
				cm->min_yinertia = -0x400;
				cm->max_yinertia = 0x400;
			}
			
			ANIMATE(8, 2, 9);
			
			LIMITX(0x800);
			LIMITY(0x800);
		}
		break;
		
		case 1:
		{
			if (o->x > player->x) o->xinertia -= 8;
			if (o->x < player->x) o->xinertia += 8;
			if (o->y > player->y) o->yinertia -= 8;
			if (o->y < player->y) o->yinertia += 8;
			
			if (o->flashtimer <= 0)
			{
				if ((o->y < player->y) && ((o->y + (8<<CSF)) > player->y))
				{
					o->flashtimer = 400;
					o->yinertia = 0;
					o->state = 2;
					
					if ((o->x + (24<<CSF)) > player->x)
						o->xinertia = -0x4000;
					else
						o->xinertia = 0x4000;
				}
			}
			
			if (o->xinertia < 0) o->dir = LEFT;
			if (o->xinertia > 0) o->dir = RIGHT;
			
			ANIMATE(4, 2, 9);
			
			LIMITX(0x800);
			LIMITY(0x800);
		}
		break;
		
		case 2:
		{
			o->frame = 2;
			
			if (o->xinertia > 0) o->xinertia -= 8;
			if (o->xinertia < 0) o->xinertia += 8;
			
			if (o->xinertia < 8 && o->xinertia >= -8)
				o->state = 1;
			
			// star trail
			if ((o->flashtimer % 5) == 0)
			{
				CaretMaker *cm = CreateCaretMaker(CAR_STAR);
				
				cm->x = o->x + (32<<CSF);
				cm->y = o->y + (12<<CSF);
				cm->xrange = 16;
				cm->yrange = 12;
				
				cm->min_xinertia = -o->xinertia;
				cm->max_xinertia = -o->xinertia;
				
				cm->min_yinertia = -0x400;
				cm->max_yinertia = 0x400;
			}
		}
		break;
	}
	
	o->x += o->xinertia;
	o->y += o->yinertia;
}

void hurt_ironhead(int amt)
{
Ironhead *o = &ironh;

	if (o->hurttimer != 0)
		return;
	
	o->hp -= amt;
	if (o->hp < 0) o->hp = 0;
	o->hurttimer = 100;
	
	CaretMaker *cm = CreateCaretMaker(CAR_NUMBERS);
	{
		cm->x = o->x + (8<<CSF);
		cm->y = o->y - (4<<CSF);
		cm->frame = 10 - amt;
		cm->xrange = 1;
	}
	
	sound(SND_BOSSOUCH);
}

/*
void c------------------------------() {}
*/

void PRunIronheadInteraction(void)
{
	if (!ironh.exists)
		return;
	
	uint8_t blockflag = 0x00;
	
	const int ironhead_left = (ironh.x + ironh.bbox.x1);
	const int ironhead_right = (ironh.x + ironh.bbox.x2);
	const int ironhead_top = (ironh.y + ironh.bbox.y1);
	const int ironhead_bottom = (ironh.y + ironh.bbox.y2);
	const int player_bottom = (player->y + (PLAYER_HEIGHT << CSF));
	const int player_right = (player->x + (PLAYER_WIDTH << CSF));
	
	// push player right
	if (player->x < ironhead_right && \
		player->x > ironhead_right - 0x1400)
	{
		if (player->y < ironhead_bottom - 0x1000 && \
			player_bottom > ironhead_top + 0x1000)
		{
			player->x = ironhead_right;
			player->xinertia = 0;
			blockflag |= BLOCK_LEFT;
		}
	}
	
	// player hit him from below
	if (player->y < ironhead_bottom && \
		player->y > ironhead_bottom - 0x1400)
	{
		if (player->x < ironhead_right - 0x1000 && \
			player_right > ironhead_left + 0x1000)
		{
			if (player->yinertia < -100)
				sound(SND_HITHEAD);
			
			player->y = ironhead_bottom;
			player->yinertia = 0;
			
			blockflag |= BLOCK_ABOVE;
		}
	}
	
	// push player left
	if (player_right > ironhead_left && \
		player_right < ironhead_left + 0x1400)
	{
		if (player->y < ironhead_bottom - 0x1000 && \
			player_bottom > ironhead_top + 0x1000)
		{
			player->x = ironhead_left - (PLAYER_WIDTH << CSF);
			player->xinertia = 0;
			
			blockflag |= BLOCK_RIGHT;
		}
	}
	
	// player riding on top
	if (player_bottom > ironhead_top && \
		player_bottom < ironhead_top + 0x1400)
	{
		if (player->x < ironhead_right - 0x1000 && \
			player_right > ironhead_left + 0x1000)
		{
			player->inair = false;
			player->y = ironhead_top - (PLAYER_HEIGHT << CSF);
			
			if (player->yinertia > 0)
				player->yinertia = 0;
			
			blockflag |= BLOCK_BELOW;
		}
	}
	
	if (blockflag)
	{
		switch(ironh.state)
		{
			case 0:
			{
				if (!player->canattacktime)
				{
					StartScript(ironh.touchscript);
					player->canattacktime = 100;
				}
			}
			break;
			
			case 1:
			{
				if (!ironh.hurttimer)
				{
					if (blockflag == BLOCK_ABOVE)
					{
						ironh.yinertia = -0x400;
						player->yinertia = 0x400;
						
						hurt_ironhead((player->controlmode == CONTROL_CAPACITOR) ? 3 : 1);
					}
					else if (player->controlmode == CONTROL_CAPACITOR)
					{
						hurt_ironhead(3);
						
						if (player->dir == LEFT)
						{
							ironh.xinertia = -0x400;
						}
						else if (player->dir == RIGHT)
						{
							ironh.xinertia = 0x400;
						}
					}
				}
			}
			break;
			
			case 2:
			{
				if (blockflag == BLOCK_ABOVE)
				{
					ironh.yinertia = -0x400;
					player->yinertia = 0x400;
					
					hurt_ironhead((player->controlmode == CONTROL_CAPACITOR) ? 3 : 1);
				}
				else if (!player->hurtflashtime)
				{
					if (player->x > (ironh.x + (24<<CSF)))
						player->xinertia = 0x400;
					else
						player->xinertia = -0x400;
					
					hurtplayer(3);
				}
			}
			break;
		}
	}
	
	// defeated script
	if (ironh.state != 0 && ironh.hp <= 0)
	{
		StartScript(ironh.defeatscript);
		player->canattacktime = 100;
		
		ironh.state = 0;
		ironh.hurttimer = 0;
		ironh.xinertia = 0;
		ironh.yinertia = 0;
	}
}


