
#ifndef _CARET_H
#define _CARET_H

#define MAX_CARETS		120
#define MAX_CARETMAKERS	10

enum Carets
{
	// "bubbles" in original
	CAR_STAR,
	CAR_BUBBLE,
	CAR_NUMBERS,
	CAR_LEVELUP,
	
	// "carets"/risers/floattext in original
	//CAR_STARS		= 5,		// caret type 0 - stars effect
	//CAR_RISENUMBER	= 7,		// caret type 2 - rising damage/XP numbers
	
	
	NUM_CARETS
};

struct Caret
{
	bool in_use;
	uint8_t type;
	int x, y;
	int xinertia, yinertia;
	
	uint8_t frame;
	uint8_t dir;
	int timer, timer2;
};

struct CaretMaker
{
	bool in_use;
	uint8_t type;
	uint8_t frame;
	uint8_t amt;
	uint8_t dir;
	
	int x, y;
	
	int max_xinertia, min_xinertia;
	int max_yinertia, min_yinertia;
	int xrange, yrange;
};

Caret *CreateCaret(int type=0);
CaretMaker *CreateCaretMaker(int type=0, int amt=1);

#endif



