
#include "ika.h"
#include "textbox.h"
#include "textbox.fdh"

#define MAXLINELEN			26
#define CONTENT_X			(MSG_X + 16)
#define CONTENT_Y			(MSG_Y + 6)

#define FONT_SPACING		8
#define LINE_SPACING		16

TextBox textbox;


enum State
{
	STATE_NORMAL,
	STATE_SCROLLING
};

bool TextBox::Init()
{
	Reset();
	return 0;
}

void TextBox::Reset()
{
	ClearText();
	
	fVisible = false;
	fSpedUp = false;
	fScrolling = false;
	
	fWaitingKey = false;
	fClearAfterNOD = false;
	fYNPrompt = false;
	
	fEditorPrompt = false;
	fCallbackOnYes = NULL;
}

/*
void c------------------------------() {}
*/

void TextBox::SetVisible(bool enable)
{
	if (fVisible != enable)
	{
		if (enable)
			Reset();
		
		fVisible = enable;
		newinputs = 0;
	}
}

// returns true if a message box is up
bool TextBox::IsVisible(void)
{
	return fVisible;
}

// returns true if the message box is "busy"
bool TextBox::IsBusy(void)
{
	if (fCWHead != fCWTail) return true;
	if (fWaitingKey) return true;
	if (fYNPrompt) return true;
	
	return false;
}

void TextBox::WaitForKey(void)
{
	fWaitingKey = true;
	fClearAfterNOD = true;
	fAnimTimer = 0;
	fAnimFrame = 0;
}

void TextBox::ShowYNPrompt(int scriptOnYes, int initialSel)
{
	if (!fYNPrompt)
	{
		sound(SND_YESNO);
		newinputs &= ~(ZKEY | LEFTKEY | RIGHTKEY);
		
		fYNPrompt = true;
		fScriptOnYes = scriptOnYes;
		fYNSel = initialSel;
	}
}

void TextBox::EditorPrompt(const char *text, bool initialSel, void (*OnYes)())
{
	SetVisible(false);
	SetVisible(true);
	
	fEditorPrompt = true;
	fCallbackOnYes = OnYes;
	SetText(text);
	ShowYNPrompt(0, initialSel);
}

/*
void c------------------------------() {}
*/

// add the specified text into the message buffer
// so it starts appearing.
void TextBox::AddText(const char *str)
{
	for(int i=0;str[i];i++)
		fCharsWaiting[fCWHead++] = str[i];
}

// clear all text in the message box
void TextBox::ClearText()
{
	for(int i=0;i<MSG_NLINES;i++)
		fLine[i].len = 0;
	
	fCurLine = 0;
	fTextTimer = 0;
	fCWHead = fCWTail = 0;
	
	fTextYOffset = 0;
}

void TextBox::SetText(const char *str)
{
	ClearText();
	strcpy(fLine[0].text, str);
	fLine[0].len = strlen(str);
}

void TextBox::Newline()
{
	fCurLine++;
	if (fCurLine >= MSG_NLINES)
	{
		fCurLine = (MSG_NLINES - 1);
		fScrolling = true;
	}
}

/*
void c------------------------------() {}
*/

void TextBox::Run()
{
	// yes/no prompt up
	if (fYNPrompt)
	{
		RunYesNo();
		if (fYNPrompt) return;
	}
	
	// "NOD" up
	if (fWaitingKey)
	{
		if (newinputs & ZKEY)
		{
			newinputs &= ~ZKEY;
			fWaitingKey = false;
			
			if (fClearAfterNOD)
				ClearText();
		}
		else
		{
			// animate dancing star
			if (++fAnimTimer > 3)
			{
				fAnimTimer = 0;
				if (++fAnimFrame > 7)
					fAnimFrame = 0;
			}
			
			return;
		}
	}
	
	// scrolling in next line
	if (fScrolling)
	{
		fTextYOffset -= (LINE_SPACING / 4);
		if (fTextYOffset <= -LINE_SPACING)
		{
			fScrolling = false;
			fTextYOffset = 0;
			fTextTimer = 0;
			
			strcpy(fLine[0].text, fLine[1].text);
			fLine[0].len = fLine[1].len;
			
			fLine[1].len = 0;
			fCurLine = 1;
		}
		else
		{
			return;
		}
	}
	
	// chars appearing...
	if (fCWHead != fCWTail)
	{
		if ((inputs & ZKEY) || ++fTextTimer > 4)
		{
			fTextTimer = 0;
			AddNextChar();
		}
	}
	else
	{
		fTextTimer = 0;
	}
}

void TextBox::AddNextChar(void)
{
	sound(SND_MESSAGE);
	char ch = fCharsWaiting[fCWTail++];
	
	if (fLine[fCurLine].len < MSG_MAXCHARS)
		fLine[fCurLine].text[fLine[fCurLine].len++] = ch;
}


void TextBox::Draw(void)
{
	if (!fVisible) return;
	
	// draw frame
	draw_sprite(MSG_X, MSG_Y, SPR_TEXTBOX, 0);
	draw_sprite(MSG_X+150, MSG_Y, SPR_TEXTBOX, 1);
	
	
	// draw text
	int x = CONTENT_X;
	int y = (CONTENT_Y + fTextYOffset);
	set_clip_rect(CONTENT_X, CONTENT_Y, MSG_W, MSG_H);
	
	for(int i=0;i<MSG_NLINES;i++)
	{
		if (fLine[i].len)
		{
			fLine[i].text[fLine[i].len] = 0;
			
			font_draw(x+1, y+1, fLine[i].text, FONT_SPACING, &bluefont);
			font_draw(x, y, fLine[i].text, FONT_SPACING, &whitefont);
		}
		
		y += LINE_SPACING;
	}
	
	// release the clipping region clipping our drawing to the text box
	clear_clip_rect();
	
	// dancing star
	if (fWaitingKey)
	{
		x = (MSG_X + MSG_W) - 8 - 8;
		y = (MSG_Y + MSG_H) - 2 - 16;
		draw_sprite(x, y, SPR_MSGSTAR, fAnimFrame);
	}
	
	if (fYNPrompt)
		DrawYesNo();
}

/*
void c------------------------------() {}
*/

#define YN_SPACING	96

#define YN_W		48
#define YN_H		16
#define	YN_Y		((SCREEN_HEIGHT / 2) - 20)
#define YN_X		((SCREEN_WIDTH / 2) - (YN_W / 2) - (YN_SPACING / 2))

void TextBox::DrawYesNo(void)
{
	// selector
	int x = (fYNSel == 0) ? YN_X : (YN_X+YN_SPACING);
	draw_sprite(x - 9, YN_Y - 7, SPR_YESNOSEL);
	
	// yes & no
	draw_sprite(YN_X, YN_Y, SPR_YESNO, 0);
	draw_sprite(YN_X+YN_SPACING, YN_Y, SPR_YESNO, 1);
}

void TextBox::RunYesNo(void)
{
	if (newinputs & LEFTKEY)
	{
		if (fYNSel == 1) fYNSel = 0;
		sound(SND_READY);
	}
	
	if (newinputs & RIGHTKEY)
	{
		if (fYNSel == 0) fYNSel = 1;
		sound(SND_READY);
	}
	
	if (newinputs & ZKEY)
	{
		sound(SND_PROPEL);
		ClearText();
		fYNPrompt = 0;
		
		if (fYNSel == YES)
		{
			if (fCallbackOnYes)
				(*fCallbackOnYes)();
			else
				StartScript(fScriptOnYes);
		}
		
		if (fEditorPrompt)
			SetVisible(false);
	}
}








