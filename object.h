
#ifndef _OBJECT_H
#define _OBJECT_H

#define MAX_OBJECTS		100

// object "TY's" (that's the name shown in the Ikachan editor).
// these names aren't the same as the ones shown in the editor;
// I renamed them to match better with what they do.
enum
{
	TY_EVENT		= 0,		// runs event on touch, not solid
	TY_NULL			= 1,		// doesn't run event, not solid.
	TY_ENEMY		= 2,		// runs event on death, is solid
	TY_SOLIDEVENT	= 3,		// runs event, is solid
	
	NUM_TY
};

// those are actually bitmask combinations of the below:
#define TY_NO_EVENT		0x01
#define TY_SOLID		0x02

struct Object
{
	// ---------------------------------------
	
	bool exists;
	uint8_t ty;
	uint8_t type;
	uint16_t event;
	
	uint16_t timer;
	uint8_t state;
	uint8_t frame;
	uint16_t animtimer;
	
	int dir;
	int x, y;
	int xinertia, yinertia;
	int x2, y2;
	
	// flags which are set if an object is touching a wall, ceiling, or floor
	// they're addressable either as an array or individually.
	union {
		struct { uint8_t blockr, blockl, blocku, blockd; };
		uint8_t block[4];
		uint32_t blockflags;
	};
	
// ---------------------------------------

	void SetType(int type);
	void ChangeType(int type);
	void BringToFront();
	void PushBehind(Object *behind);
	void PushBehind(int objtype);
	
	void Draw();
	
	// --------------------------------------- hit detection w/ map
	
	uint32_t GetAttributes(const SIFPoint *pointlist, int npoints, int *tile = NULL);
	
	bool CheckAttribute(const SIFPoint *pointlist, int npoints, uint32_t attrmask, \
						int *tile_x = NULL, int *tile_y = NULL);
	
	bool CheckSolidIntersect(Object *other, const SIFPoint *pointlist, int npoints);
	
	// --------------------------------------- overridden convenience versions of above
	
	bool CheckAttribute(SIFPointList *points, uint32_t attrmask, int *tile_x = NULL, int *tile_y = NULL)
	{
		return CheckAttribute(&points->point[0], points->count, attrmask, tile_x, tile_y);
	}
	
	uint32_t GetAttributes(SIFPointList *points, int *tile = NULL)
	{
		return GetAttributes(&points->point[0], points->count, tile);
	}
	
	bool CheckSolidIntersect(Object *other, SIFPointList *points)
	{
		return CheckSolidIntersect(other, &points->point[0], points->count);
	}
	
	// ---------------------------------------
	
	void UpdateBlockStates(uint8_t updatemask);
	
	// ---------------------------------------
	
	bool apply_xinertia(int inertia);
	bool apply_yinertia(int inertia);
	void Move(void);
	void SnapToGround();
	
	// ---------------------------------------
	
	void RunAI();
	void DealContactDamage();
	
	void OnTick();
	void OnAftermove();
	void OnSpawn();
	void OnDeath();
	
	// ---------------------------------------
	
	void Delete();			// mark for deletion at end of frame
	void Destroy();			// delete immediately
	
	// ---------------------------------------
	
	int Width();
	int Height();
	
	int BBoxWidth();
	int BBoxHeight();
	
	int CenterX();
	int CenterY();
	
	int Left();
	int Right();
	int Top();
	int Bottom();
	
	int SolidLeft();
	int SolidRight();
	int SolidTop();
	int SolidBottom();
	
	int ActionPointX();
	int ActionPointY();
	int ActionPoint2X();
	int ActionPoint2Y();
	int DrawPointX();
	int DrawPointY();
	
	SIFSprite *Sprite();
};

#if 0
// original version -- but it has sequence point issues and doesn't work with -O2

// one rarely needs write a for loop this nutso, but it's still valid C, and manages
// to run the body of the loop on each existing object, while setting O to point to it.
#define FOREACH_OBJECT(O)	\
	for(int ITERATOR=0;	\
		({ while(!objects[ITERATOR].exists && ITERATOR < MAX_OBJECTS) { ITERATOR++; } }), \
		(O = &objects[ITERATOR]), (ITERATOR < MAX_OBJECTS);	\
		ITERATOR++)

#else

// this version should be much more foolproof (and far less confusing).
// this does abuse the "loop" braces a little as they actually become the body
// of the "if", but it's not like the original didn't have any "abuses".
#define FOREACH_OBJECT(O)	\
	for(int ITERATOR=0; O = &objects[ITERATOR], ITERATOR < MAX_OBJECTS; ITERATOR++)	\
		if (O->exists)

#endif

namespace Objects
{
	void DeleteAll(void);
	void DestroyAll(void);
	void UpdateAllBlockStates();
}


// game objects
enum
{
	OBJ_HARI,		// puffer fish
	OBJ_ISOGIN,		// sea anemone (floor npcs)
	OBJ_KANI,		// crab
	OBJ_SAVE,
	OBJ_CHIBI,		// ben
	OBJ_HOSHI,
	OBJ_SIGN,		// also fish bones & invisible triggers
	OBJ_CARRY,
	OBJ_JUEL,		// red/blue fish
	OBJ_SHIP,
	
	NUM_OBJECT_TYPES
};

struct ObjProp
{
	int sprite;
};

extern Object objects[MAX_OBJECTS];
extern ObjProp objprop[NUM_OBJECT_TYPES];

inline int Object::Width()			{ return (sprites[objprop[type].sprite].w << CSF); }
inline int Object::Height()			{ return (sprites[objprop[type].sprite].h << CSF); }

inline int Object::BBoxWidth()		{ return (((sprites[objprop[type].sprite].bbox.x2 - sprites[objprop[type].sprite].bbox.x1) + 1) << CSF); }
inline int Object::BBoxHeight()		{ return (((sprites[objprop[type].sprite].bbox.y2 - sprites[objprop[type].sprite].bbox.y1) + 1) << CSF); }

inline int Object::CenterX()		{ return (this->x + (Width() / 2)) - DrawPointX(); }
inline int Object::CenterY()		{ return (this->y + (Height() / 2)) - DrawPointY(); }

inline int Object::Left()			{ return (this->x + (sprites[objprop[type].sprite].bbox.x1 << CSF)); }
inline int Object::Right()			{ return (this->x + (sprites[objprop[type].sprite].bbox.x2 << CSF)); }
inline int Object::Top()			{ return (this->y + (sprites[objprop[type].sprite].bbox.y1 << CSF)); }
inline int Object::Bottom()			{ return (this->y + (sprites[objprop[type].sprite].bbox.y2 << CSF)); }

inline int Object::SolidLeft()		{ return (this->x + (sprites[objprop[type].sprite].solidbox.x1 << CSF)); }
inline int Object::SolidRight()		{ return (this->x + (sprites[objprop[type].sprite].solidbox.x2 << CSF)); }
inline int Object::SolidTop()		{ return (this->y + (sprites[objprop[type].sprite].solidbox.y1 << CSF)); }
inline int Object::SolidBottom()	{ return (this->y + (sprites[objprop[type].sprite].solidbox.y2 << CSF)); }

inline int Object::ActionPointX()	{ return (this->x + (sprites[objprop[type].sprite].frame[this->frame].dir[this->dir].actionpoint.x << CSF)); }
inline int Object::ActionPointY()	{ return (this->y + (sprites[objprop[type].sprite].frame[this->frame].dir[this->dir].actionpoint.y << CSF)); }
inline int Object::ActionPoint2X()	{ return (this->x + (sprites[objprop[type].sprite].frame[this->frame].dir[this->dir].actionpoint2.x << CSF)); }
inline int Object::ActionPoint2Y()	{ return (this->y + (sprites[objprop[type].sprite].frame[this->frame].dir[this->dir].actionpoint2.y << CSF)); }

inline int Object::DrawPointX()		{ return (sprites[objprop[type].sprite].frame[this->frame].dir[this->dir].drawpoint.x << CSF); }
inline int Object::DrawPointY()		{ return (sprites[objprop[type].sprite].frame[this->frame].dir[this->dir].drawpoint.y << CSF); }

inline SIFSprite *Object::Sprite()	{ return &sprites[objprop[type].sprite]; }

extern Object dummy_object;

#endif


