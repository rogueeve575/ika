
#include <SDL/SDL.h>
#include <unistd.h>
#include <math.h>
#include <vector>
#include "common/basics.h"
#include "datadirs.h"

#include "sslib.h"
#include "pmd.h"
#include "pmd.fdh"

// music synthesis engine for Ikachan .pmd files
// Pixel uses a wavetable-synthesis type of system for this, similar to the
// org files from Cave Story but a bit simpler in some aspects

//#define DEBUG(...)		stat(__VA_ARGS__)
#define DEBUG(...)

static void load_instrument(FILE *fp, stInstrument *ins);
static bool load_drums(void);
static void clear_running_notes(void);

static stSong song;
static std::vector<stRunningNote *> running_notes;

#define BUFFER_SAMPLES		(SAMPLE_RATE / 2)	// 0.5 seconds of music
#define NUM_BUFFERS			8
static struct
{
	// pointer to the raw PCM sound data
	// 16-bit stereo audio
	signed short samples[BUFFER_SAMPLES*2];		// *2 for stereo
	uint8_t state;
} buffers[NUM_BUFFERS];

enum {	// buffer state
	BUFFER_AVAIL,		// unused, can be filled with data
	BUFFER_READY,		// filled with data, queued to play
};

#define NUM_DRUMS		7
static struct
{
	uint8_t *samples;
	uint32_t length;
} drum[NUM_DRUMS];

static SDL_sem *threadWakeupSem;

static const char *drum_names[NUM_DRUMS] =
{
	"bass1", "bass2", "snare1", NULL, "hat1", "hat2", "symbal1"
};

static const int pitch_table[] =
{
	0x060F, 0x0674, 0x06D3, 0x0738,
	0x07A3, 0x081A, 0x089D, 0x0914,
	0x099D, 0x0A38, 0x0AD2, 0x0B7A
};


bool pmd_init(void)
{
	memset(&song, 0, sizeof(song));
	set_all_buffers_unused();
	
	if (load_drums())
		return 1;
	
	SSReserveChannel(PMD_CHANNEL);
	SSSetVolume(PMD_CHANNEL, SDL_MIX_MAXVOLUME);
	threadWakeupSem = SDL_CreateSemaphore(0);
	
	stat("pmd player ready");
	return 0;
}

void pmd_close(void)
{
int i;

	SSAbortChannel(PMD_CHANNEL);
	
	for(i=0;i<NUM_DRUMS;i++)
	{
		if (drum[i].samples)
			SDL_FreeWAV(drum[i].samples);
	}
	
	clear_running_notes();
}

/*
void c------------------------------() {}
*/

bool pmd_load(const char *fname)
{
FILE *fp;
int i, t;

	stat("----------------------");
	stat("pmd_load: opening '%s'", fname);
	pmd_stop();
	
	fp = fopen(fname, "rb");
	if (!fp)
	{
		staterr("pmd_load: failed to open '%s'", fname);
		return 1;
	}
	
	if (!fverifystring(fp, "PMD"))
	{
		staterr("pmd_load: not a PMD file: '%s'", fname);
		fclose(fp);
		return 1;
	}
	
	fgetc(fp);		// I dunno
	fgetl(fp);		// I dunno
	song.ms_per_beat = fgetl(fp);	// music wait
	song.loop_start = fgetl(fp);
	song.loop_end = fgetl(fp);
	song.nnotes = fgetl(fp);
	
	stat("song wait: %d (0x%04x)", song.ms_per_beat, song.ms_per_beat);
	stat("nnotes: %d (0x%04x)", song.nnotes, song.nnotes);
	stat("loop: %d-%d", song.loop_start, song.loop_end);
	
	// original PiyoPiyo and Ikachan seem to play it slightly slower than specified,
	// although PiyoPiyoPlayer plays it as spec'd.
	if (strstr(fname, "Buriki"))
		song.ms_per_beat += 10;
	else
		song.ms_per_beat += 5;
	
	// load instrument samples
	for(i=0;i<NUM_TRACKS;i++)
		load_instrument(fp, &song.track[i].instrument);
	
	for(i=0;i<TOTAL_TRACKS;i++)
		song.track[i].no = i;
	
	// drum volume
	song.drum_volume[0] = fgetl(fp);
	song.drum_volume[1] = (song.drum_volume[0] * 70) / 100;
	
	// load music notes
	for(t=0;t<TOTAL_TRACKS;t++)
	{
		stTrack *track = &song.track[t];
		
		for(i=0;i<song.nnotes;i++)
		{
			uint32_t mask = fgetl(fp);
			
			track->note[i].notemask = (mask & 0xffffff);
			
			track->note[i].panning = (mask >> 24);
			if (!track->note[i].panning)
				track->note[i].panning = PAN_CENTER;
			else
				track->note[i].panning--;
		}
	}
	
	return 0;
}

// loads octave, icon, and wavetable for a track.
static void load_instrument(FILE *fp, stInstrument *ins) {
	ins->octave = fgetc(fp);
	ins->icon = fgetc(fp);
	
	fgeti(fp);
	
	ins->notelength = fgetl(fp);
	ins->volume = fgeti(fp);
	
	for(int i=0;i<10;i++)
		fgetc(fp);
	
	// read instrument waveform
	for(int i=0;i<WAVE_LENGTH;i++)
		ins->wave[i] = fgetc(fp);
	
	// read envelope
	for(int i=0;i<ENV_LENGTH;i++)
		ins->env[i] = fgetc(fp);
}

/*
void c------------------------------() {}
*/

static bool load_drums(void) {
	for(int i=0;i<NUM_DRUMS;i++) {
		if (!drum_names[i]) {
			drum[i].samples = NULL;
			continue;
		}
		
		char fname[MAXPATHLEN];
		GetSoundFilename(drum_names[i], fname, sizeof(fname));

		SDL_AudioSpec spec;
		if (!SDL_LoadWAV(fname, &spec, &drum[i].samples, &drum[i].length)) {
			staterr("failed to open %s", fname);
			return 1;
		}
		
		if (spec.freq != SAMPLE_RATE || spec.format != AUDIO_U8) {
			staterr("unexpected audio format in %s", fname);
			return 1;
		}
		
		// preconvert from 8-bit signed to 8-bit unsigned
		for(int j=0;j<drum[i].length;j++)
			drum[i].samples[j] -= 0x80;
	}
	
	return 0;
}


static void drum_begin(int note, int pan)
{
int drumno, volume;
	
	drumno = (note / 2);
	if (drumno >= NUM_DRUMS || !drum[drumno].samples)
	{
		staterr("invalid/silent drum note %d / drum %d", note, drumno);
		return;
	}
	
	// compute volume
	volume = song.drum_volume[note & 1];
	
	int volume_left, volume_right;
	ComputeVolumeRatios(volume, pan, &volume_left, &volume_right);
	
	// create queueable sound buffer
	stRunningNote *snd = new stRunningNote;
	snd->curpos = 0;
	
	snd->nsamples = drum[drumno].length;
	snd->samples = (int16_t *)malloc(snd->nsamples * sizeof(int16_t) * 2);
	
	// upconvert 8-bit to 16 and apply pan
	int i, audioval, scale, outpos = 0;
	if (volume_left == volume_right)
	{
		for(i=0;i<drum[drumno].length;i++)
		{
			audioval = (int8_t)drum[drumno].samples[i];
			audioval <<= 8;
			
			scale = audioval;
			scale *= volume_left;
			scale /= PMD_MAX_VOLUME;
			snd->samples[outpos++] = scale;
			snd->samples[outpos++] = scale;
		}
	}
	else
	{
		for(i=0;i<drum[drumno].length;i++)
		{
			audioval = (int8_t)drum[drumno].samples[i];
			audioval <<= 8;
			
			scale = audioval;
			scale *= volume_left;
			scale /= PMD_MAX_VOLUME;
			snd->samples[outpos++] = scale;
			
			scale = audioval;
			scale *= volume_right;
			scale /= PMD_MAX_VOLUME;
			snd->samples[outpos++] = scale;
		}
	}
	
	running_notes.push_back(snd);
	DEBUG("started drum %d on channel %d", note, running_notes.size() - 1);
}
	
/*
void c------------------------------() {}
*/

void pmd_start(void)
{
	pmd_stop();
	set_all_buffers_unused();

	song.beat = 0;
	song.samples = 0;
	song.samples_per_beat = MSToSamples(song.ms_per_beat);
	song.samples_left_in_beat = 0;
	
	stat("Starting pmd playback: %d ms per beat, %d samples per beat", \
		song.ms_per_beat, song.samples_per_beat);
	
	pmd_set_volume(PMD_NORMAL_VOLUME);
	song.playing = true;
	
	// generate the first buffer now so we know something is playing
	// before we return
	gen_next_buffer();
	
	// start music synthesis thread
	song.thread = SDL_CreateThread(pmd_thread, NULL);
	if (!song.thread) staterr("failed to create music thread!");
}

void pmd_stop(void)
{
	if (song.playing)
	{
		SSAbortChannel(PMD_CHANNEL);
		song.playing = false;
		
		// wait for thread to exit (triggered by setting song.playing = false)
		if (song.thread)
		{
			SDL_SemPost(threadWakeupSem);
			SDL_WaitThread(song.thread, NULL);
			song.thread = NULL;
		}
		
		clear_running_notes();
		set_all_buffers_unused();
	}
}

bool pmd_is_playing()
{
	return song.playing;
}

/*
void c------------------------------() {}
*/

// music synthesis thread
int pmd_thread(void *param)
{
	stat("pmd: thread started");
	
	while(song.playing)
	{
		while(song.playing && gen_next_buffer());
		
		// sleep on the semaphore until we're woken up by a buffer being finished
		//stat("sleeping");
		SDL_SemWait(threadWakeupSem);
	}
	
	stat("thread: exiting");
	return 0;
}

static bool gen_next_buffer(void)
{
	// find an unused buffer
	int bufferno = find_unused_buffer();
	if (bufferno == -1)
	{
		//stat("all buffers full");
		return 0;
	}
	
	// fill it
	//stat("filling buffer %d", bufferno);
	generate_music(buffers[bufferno].samples, BUFFER_SAMPLES);
	buffers[bufferno].state = BUFFER_READY;
	
	// enqueue it
	SSEnqueueChunk(PMD_CHANNEL, buffers[bufferno].samples, BUFFER_SAMPLES, \
			(void *)(size_t)bufferno, pmd_buffer_finished);
	
	//usleep(400 * 1000);
	return 1;
}

static void pmd_buffer_finished(int chan, void *userdata)
{
	int bufferno = (int)(size_t)userdata;
	//stat("buffer finished: %d", bufferno);
	
	buffers[bufferno].state = BUFFER_AVAIL;
	SDL_SemPost(threadWakeupSem);
}

static int find_unused_buffer(void)
{
static int i = 0;
int start = i;

	for(;;)
	{
		if (++i >= NUM_BUFFERS)
			i = 0;
		
		if (buffers[i].state == BUFFER_AVAIL)
			return i;
		
		if (i == start)
			return -1;
	}
}

static void set_all_buffers_unused(void)
{
	for(int i=0;i<NUM_BUFFERS;i++)
		buffers[i].state = BUFFER_AVAIL;
}

void pmd_run(void)
{
	if (!song.playing)
		return;
	/*
	debug("beat: %d", song.beat);
	debug("running_notes: %d", running_notes.size());
	debug("wait: %d/%d", song.ms_per_beat, song.samples_per_beat);
	*/
	
	run_fade();
}

/*
void c------------------------------() {}
*/

// mix audio from any running notes into the output buffer,
// and start any new notes as we run across their sample-positions
static void generate_music(signed short *outbuffer, int len)
{
	// account for stereo
	len *= 2;
	
	int outpos = 0;
	while(outpos < len)
	{
		// start any notes that are supposed to begin on this sample
		if (--song.samples_left_in_beat <= 0)
		{
			for(int t=0;t<TOTAL_TRACKS;t++)
				start_notes(&song.track[t], song.beat);
			
			song.beat++;
			song.samples_left_in_beat = song.samples_per_beat;
			
			if (song.beat >= song.loop_end)
				song.beat = song.loop_start;
		}
		
		// mix all currently-playing notes/drums together
		int mix_left = 0;
		int mix_right = 0;
		int nrnotes = running_notes.size();
		for(int i=0;i<nrnotes;i++)
		{
			stRunningNote *snd = running_notes[i];
			mix_left  += snd->samples[snd->curpos];
			mix_right += snd->samples[snd->curpos + 1];
			snd->curpos += 2;
			
			if (snd->curpos >= snd->nsamples * 2)	// note/drum completed?
			{
				DEBUG("channel %d completed", i);
				auto it = running_notes.begin() + i;
				running_notes.erase(it);
				i--; nrnotes--;
				
				delete snd;
			}
		}
	
		// I think this is just arbitrary; not sure
		mix_left /= 3;
		mix_right /= 3;
		
		if (mix_left  < -32767) mix_left = -32767;
		if (mix_left  > 32767) mix_left = 32767;
		if (mix_right < -32767) mix_right = -32767;
		if (mix_right > 32767) mix_right = 32767;
		
		#if __BYTE_ORDER == __BIG_ENDIAN
		#define MAKE_LE(INPUT)		((INPUT >> 8) | (INPUT << 8))
		outbuffer[outpos++] = MAKE_LE(mix_left);
		outbuffer[outpos++] = MAKE_LE(mix_right);
		#else
		outbuffer[outpos++] = mix_left;
		outbuffer[outpos++] = mix_right;
		#endif
	}
}

/*
void c------------------------------() {}
*/

static void start_notes(stTrack *track, int beat)
{
	uint32_t notemask = track->note[beat].notemask;
	
	if (notemask != 0)
	{
		//stat(" %d>>  %08x : %d", track->no, notemask, track->instrument.volume);
		
		for(int note=0;note<24;note++)
		{
			if (notemask & 1)
			{
				if (track->no == DRUM_TRACK)
					drum_begin(note, track->note[beat].panning);
				else
					note_begin(note, track->note[beat].panning, &track->instrument);
			}
			
			notemask >>= 1;
		}
	}
}


static void note_begin(int note, int pan, stInstrument *ins)
{
	stRunningNote *snd = new stRunningNote;
	synthesize_note(note, pan, ins, snd);
	snd->curpos = 0;
	
	running_notes.push_back(snd);
	DEBUG("started note %d on channel %d", note, running_notes.size() - 1);
}

/*
void c------------------------------() {}
*/

static void synthesize_note(int note, int pan, stInstrument *ins, stRunningNote *snd)
{
int i;

	int notelength = ins->notelength;
	int octscaler = (1 << ins->octave);
	
	uint8_t *output_buffer = (uint8_t *)malloc(notelength);
	uint8_t *ptr = output_buffer;
	
	int wavepos = 0;
	int waverate;
	
	// each pmd "octave" actually covers two musical octaves...so for
	// the upper half of the range repeat the lower notes but play them
	// twice as high.
	if (note < 12)
		waverate = (pitch_table[note] * octscaler) / 16;
	else
		waverate = (pitch_table[note - 12] * octscaler) / 8;
	
	// create the note - we're copying one of several waveforms given in the
	// file and just changing it's pitch to play the instrument at different
	// notes.
	for(i=0;i<notelength;i++)
	{
		uint8_t readpos = (wavepos / 256);
		int sample = (int8_t)ins->wave[readpos];
		
		// equivalent to (i * (ENV_LENGTH / notelength)) but better precision
		sample *= ins->env[(i * ENV_LENGTH) / notelength];
		sample /= 128;
		
		*(ptr++) = sample;
		wavepos += waverate;
	}
	
	// convert sound to 16-bit and add panning
	int total_samples = (notelength * 2);	// *2 for stereo - "total samples"
	snd->samples = (int16_t *)malloc(total_samples * 2 * sizeof(int16_t));
	snd->nsamples = notelength;			// number of "stereo samples" we'll tell sslib
	
	int volume_left, volume_right;
	ComputeVolumeRatios(ins->volume, pan, &volume_left, &volume_right);
	
	int outpos = 0;
	int audioval, scale;
	
	// optimization where we only have to compute one scaled
	// volume because both speakers are the same volume
	// (which the majority of notes are this way)
	if (volume_left == volume_right)
	{
		for(i=0;i<notelength;i++)
		{
			audioval = (int8_t)output_buffer[i];
			audioval <<= 8;
			
			scale = audioval;
			scale *= volume_left;
			scale /= PMD_MAX_VOLUME;
			snd->samples[outpos++] = scale;
			snd->samples[outpos++] = scale;
		}
	}
	else
	{
		for(i=0;i<notelength;i++)
		{
			audioval = (int8_t)output_buffer[i];
			audioval <<= 8;
			
			scale = audioval;
			scale *= volume_left;
			scale /= PMD_MAX_VOLUME;
			snd->samples[outpos++] = scale;
			
			scale = audioval;
			scale *= volume_right;
			scale /= PMD_MAX_VOLUME;
			snd->samples[outpos++] = scale;
		}
	}
	
	free(output_buffer);
}

static void clear_running_notes(void) {
	for(auto *snd : running_notes)
		delete snd;

	running_notes.clear();
	//while(running_notes.CountItems())
		//delete (stRunningNote *)running_notes.RemoveItem(0);
}

/*
void c------------------------------() {}
*/

/*
static void draw_samples(signed short *samples, int nsamples)
{
int i;
double x;
double xrate;

	// draw backdrop
	FillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, 0);
	const char *str = stprintf("xrate %.2f nsamples %d", xrate, nsamples);
	font_draw(4, 16, str, 5, &greenfont);
	
	DrawLine(0, SCREEN_HEIGHT/2, SCREEN_WIDTH, SCREEN_HEIGHT/2, 80, 192, 80);
	
	// get width rate
	xrate = (SCREEN_WIDTH / (double)nsamples);
	
	// samples to stereo samples
	nsamples *= 2;
	
	for(i=0;i<nsamples;i+=2)
	{
		plot_sample(samples[i], (int)x, 0, 255, 255);
		plot_sample(samples[i+1], (int)x, 0, 255, 0);
		x += xrate;
	}
}

static void plot_sample(signed short sample, int x, int r, int g, int b)
{
int y;
double yrate = (double)(SCREEN_HEIGHT / 2) / (double)32767;

	y = (SCREEN_HEIGHT / 2) + (sample * yrate);
	DrawPixel(x, y, r, g, b);
}
*/

/*
void c------------------------------() {}
*/

// set the song volume to the specified value immediately and cancel any fade.
void pmd_set_volume(int vol)
{
	song.volume = vol;
	song.desired_volume = vol;
	song.volume_timer = 0;
	
	SSSetVolume(PMD_CHANNEL, song.volume);
}

// fade the song volume gradually either up or down to the specified volume.
void pmd_fade(int vol)
{
	song.desired_volume = vol;
}


static void run_fade(void)
{
	if (song.volume != song.desired_volume)
	{
		if (++song.volume_timer > PMD_FADE_RATE)
		{
			song.volume_timer = 0;
			song.volume += (song.volume < song.desired_volume) ? 1 : -1;
			
			SSSetVolume(PMD_CHANNEL, song.volume);
		}
	}
}

/*
void c------------------------------() {}
*/

// given a volume and a panning value, it returns two values
// between 0 and PMD_MAX_VOLUME which are how loud to play
// the left and right channels.
static void ComputeVolumeRatios(int volume, int panning, \
							int *volume_left, int *volume_right)
{
	if (panning == PAN_CENTER)
	{
		*volume_left = volume;
		*volume_right = volume;
	}
	else if (panning < PAN_CENTER)
	{	// panning left (make right channel quieter)
		int left = volume;
		left *= panning;
		left /= PAN_CENTER;
		*volume_left = left;
		*volume_right = volume;
	}
	else
	{	// panning right
		int right = volume;
		right *= (PAN_FULL_RIGHT - panning);
		right /= PAN_CENTER;
		*volume_right = right;
		*volume_left = volume;
	}
}

// converts a time in milliseconds to that same time length in samples
static int MSToSamples(int ms)
{
	return (int)(((double)SAMPLE_RATE / (double)1000) * (double)ms);
}





