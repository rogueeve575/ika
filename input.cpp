
#include "ika.h"
#include "input.fdh"

Mouse mouse;
uint32_t mappings[SDLK_LAST];

uint32_t inputs, newinputs;
int last_sdl_key;

bool input_init(void)
{
	memset(&mouse, 0, sizeof(mouse));
	mouse.x = (SCREEN_WIDTH / 2);
	mouse.y = (SCREEN_HEIGHT / 2);
	
	memset(mappings, 0, sizeof(mappings));
	inputs = 0;
	newinputs = 0;
	
	// default mappings
	mappings[SDLK_LEFT] = LEFTKEY;
	mappings[SDLK_RIGHT] = RIGHTKEY;
	mappings[SDLK_UP] = UPKEY;
	mappings[SDLK_DOWN] = DOWNKEY;
	
	mappings[SDLK_DELETE] = DELKEY;
	mappings[SDLK_b] = BKEY;
	mappings[SDLK_c] = CKEY;
	mappings[SDLK_n] = NKEY;
	mappings[SDLK_p] = PKEY;
	mappings[SDLK_m] = MKEY;
	mappings[SDLK_r] = RKEY;
	
	mappings[SDLK_SPACE] = SPACEKEY;
	mappings[SDLK_LSHIFT] = SHIFTKEY;
	mappings[SDLK_x] = XKEY;
	mappings[SDLK_z] = ZKEY;
	mappings[SDLK_s] = SKEY;
	
	mappings[SDLK_ESCAPE] = ESCKEY;
	mappings[SDLK_F1] = F1KEY;
	mappings[SDLK_F2] = F2KEY;
	mappings[SDLK_F3] = F3KEY;
	mappings[SDLK_F4] = F4KEY;
	
	mappings[SDLK_TAB] = TABKEY;
	
	return 0;
}

// set the SDL key that triggers an input
void input_remap(int keymask, int sdl_key)
{
	stat("input_remap(%d => %d)", keymask, sdl_key);
	int old_mapping = input_get_mapping(keymask);
	if (old_mapping != -1)
		mappings[old_mapping] = 0;
	
	mappings[sdl_key] = keymask;
}

// get which SDL key triggers a given input
int input_get_mapping(int keymask)
{
int i;

	for(i=0;i<=SDLK_LAST;i++)
	{
		if (mappings[i] == keymask)
			return i;
	}
	
	return -1;
}

/*
void c------------------------------() {}
*/

void input_poll(void)
{
static uint8_t shiftstates = 0;
SDL_Event evt;
int key;

	while(SDL_PollEvent(&evt))
	{
		switch(evt.type)
		{
			case SDL_KEYDOWN:
			case SDL_KEYUP:
			{
				key = evt.key.keysym.sym;
				
				if (console.IsVisible() && !IsNonConsoleKey(key))
				{
					if (key == SDLK_LSHIFT)
					{
						if (evt.type == SDL_KEYDOWN)
							shiftstates |= LEFTMASK;
						else
							shiftstates &= ~LEFTMASK;
					}
					else if (key == SDLK_RSHIFT)
					{
						if (evt.type == SDL_KEYDOWN)
							shiftstates |= RIGHTMASK;
						else
							shiftstates &= ~RIGHTMASK;
					}
					else
					{
						int ch = key;
						if (shiftstates != 0)
						{
							ch = toupper(ch);
							if (ch == '.') ch = '>';
							if (ch == '-') ch = '_';
							if (ch == '/') ch = '?';
							if (ch == '1') ch = '!';
						}
						
						if (evt.type == SDL_KEYDOWN)
							console.HandleKey(ch);
						else
							console.HandleKeyRelease(ch);
					}
				}
				else
				{
					uint32_t imask = mappings[key];
					if (imask)
					{
						if (evt.type == SDL_KEYDOWN)
						{
							inputs |= imask;
							newinputs |= imask;
						}
						else
						{
							inputs &= ~imask;
						}
					}
					
					if (evt.type == SDL_KEYDOWN)
					{
						if (key == '`' || key == '~')	// bring up console (works anywhere)
						{
							sound(SND_PROPEL);
							console.SetVisible(true);
						}
						else
						{
							last_sdl_key = key;
						}
					}
				}
			}
			break;
			
			case SDL_MOUSEMOTION:
				UpdateMouse(evt.motion.x, evt.motion.y);
			break;
			
			case SDL_MOUSEBUTTONDOWN:
				UpdateMouse(evt.motion.x, evt.motion.y);
				if (evt.button.button == SDL_BUTTON_LEFT) mouse.lbutton = true;
				if (evt.button.button == SDL_BUTTON_RIGHT) mouse.rbutton = true;
				mouse.btndown = true;
			break;
			
			case SDL_MOUSEBUTTONUP:
				UpdateMouse(evt.motion.x, evt.motion.y);
				if (evt.button.button == SDL_BUTTON_LEFT) mouse.lbutton = false;
				if (evt.button.button == SDL_BUTTON_RIGHT) mouse.rbutton = false;
				mouse.btnup = true;
			break;
			
			case SDL_QUIT:
				game.running = false;
			break;
		}
	}
}

static void UpdateMouse(int x, int y)
{
	x /= SCALE;
	y /= SCALE;
	
	if (x != mouse.x || y != mouse.y)
	{
		mouse.x = x;
		mouse.y = y;
		
		mouse.moved = true;
	}
}

bool MouseInRect(Rect *rect)
{
	return (mouse.x >= rect->x1 && mouse.y >= rect->y1 && \
			mouse.x <= rect->x2 && mouse.y <= rect->y2);
}


// keys that we don't want to send to the console
// even if the console is up.
static int IsNonConsoleKey(int key)
{
static const int nosend[] = { SDLK_LEFT, SDLK_RIGHT, 0 };

	for(int i=0;nosend[i];i++)
		if (key == nosend[i])
			return true;
	
	return false;
}


void input_close(void)
{

}

/*
void c------------------------------() {}
*/

static const uint32_t buttons[] = { XKEY, ZKEY, 0 };

bool buttondown(void)
{
	for(int i=0;buttons[i];i++)
	{
		if (inputs & buttons[i])
			return 1;
	}
	
	return 0;
}

bool buttonjustpushed(void)
{
	for(int i=0;buttons[i];i++)
	{
		if (newinputs & buttons[i])
			return 1;
	}
	
	return 0;
}





