
#include "ika.h"
#include "game.h"
#include "roll.h"
#include "title.h"
#include "editor/editor.h"
#include "game.fdh"

const char *savefilename = "Ika.rec";
static struct TickFunctions
{
	void (*OnTick)(void);
	bool (*OnEnter)(void);
	void (*OnExit)(void);
}
tickfunctions[] =
{
	normal_tick,	NULL,		NULL,			// GM_NORMAL
	roll_tick,		NULL,		roll_close,		// GM_ROLL
	title_tick,		title_init, title_close,	// GM_TITLE
	editor_tick,	editor_init,editor_close,	// GM_EDITOR
};

// init Game object: only called once during startup
bool Game::init()
{
	memset(&game, 0, sizeof(game));
	memset(objprop, 0, sizeof(objprop));
	
	AssignSprites();
	return 0;
}

// reset things to prepare for entry to the next stage
bool Game::initlevel()
{
	DeleteCarets();
	
	game.hidescene = false;
	game.entcount = 0;
	map.focustype = FOCUS_PLAYER;
	
	if (objects_init())
		return 1;
	
	InitPlayer();
	textbox.Init();
	itembox.Init();
	start_effect(EFFECT_NONE);
	
	return 0;
}

/*
void c------------------------------() {}
*/

void Game::tick(void)
{
	debug_clear();
	
	(*tickfunctions[game.mode].OnTick)();
	
	debug_draw();
	console.Draw();
}

bool Game::setmode(int newmode)
{
	stat("Entering tick mode %d", newmode);
	
	if (tickfunctions[game.mode].OnExit)
		tickfunctions[game.mode].OnExit();
	
	game.mode = newmode;
	
	if (tickfunctions[game.mode].OnEnter)
	{
		if (tickfunctions[game.mode].OnEnter())
		{
			staterr("failed to initilize mode %d", newmode);
			game.running = false;
			return 1;
		}
	}
	
	return 0;
}

/*
void c------------------------------() {}
*/

void normal_tick(void)
{
	if (itembox.IsVisible())
	{
		itembox.Run();
	}
	else
	{
		Objects::UpdateAllBlockStates();

		if (is_debugmode()) {
			if (newinputs & F3KEY) { player->level++; player->level %= 7; }
			if (newinputs & F4KEY) { game_save(); sound(SND_SAVE); console.Print("Game saved."); }
		}

		// this is actually a weird part of the original game for some reason,
		// so we'll let it happen all the time just for accuracy.
		if (newinputs & XKEY) player->x += 0x200;
		
		if (!GameFrozen())
		{
			run_ironhead();
			run_ai();
			
			if (!(inputs & F2KEY))
			{
				if (player->enabled)
				{
					PRunCurrentControlMode();
					
					if (player->controlmode != CONTROL_ESCAPING)
					{
						PRunObjectInteraction();
						PRunIronheadInteraction();	// in ironhead.cpp
						PRunMapInteraction();
					}
				}
			}
			else
			{
				MapMover();
			}
			
			if ((newinputs & SKEY) && GetCurrentScript() == -1) {
				// TODO: "S" may actually also be part of the original game
				// without special debug mode. I don't recall.
				if (is_debugmode())
					itembox.SetVisible(true);
			}
		}
		
		run_carets();
		run_caretmakers();
		
		if (player->died)
		{
			player->died = false;
			StartScript(6);
		}
		
		// escaped!
		if (player->y < -(32<<CSF) && GetCurrentScript() == -1)
		{
			StartScript(4000);
		}
		
		map_scroll_do();
		Tileset::move_bubbles();
	}
	
	textbox.Run();
	run_events();
	
	DrawScene();
}


bool GameFrozen(void)
{
	if (textbox.IsVisible()) return true;
	return false;
}


void DrawWorld(void)
{
	if (game.hidescene)
	{
		FillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, BLACK);
	}
	else
	{
		map_draw_backdrop();
		map_draw(0);
		
		Object *o;
		FOREACH_OBJECT(o)
		{
			o->Draw();
		}
		
		draw_ironhead();
		
		if (player->enabled && !(player->hurtflashtime & 1))
		{
			PDrawPlayer();
		}
		
		map_draw(TA_FOREGROUND);
		DrawCarets();
		
		draw_effects();
	}
}

void DrawScene(void)
{
	DrawWorld();
	PDrawStatusBar();
	
	textbox.Draw();
	itembox.Draw();
}

/*
void c------------------------------() {}
*/

bool Game::readflag(uint16_t no)
{
	if (no >= NUM_FLAGS)
		return false;
	
	uint8_t mask = (1 << (no & 7));
	bool result = (game.flagdata[no / 8] & mask);
	
	return result;
}

void Game::setflag(uint16_t no, bool newvalue)
{
	if (no < NUM_FLAGS)
	{
		uint8_t mask = (1 << (no & 7));
		uint8_t e = (no / 8);
		
		game.flagdata[e] |= mask;
		if (!newvalue) game.flagdata[e] ^= mask;
	}
}

/*
void c------------------------------() {}
*/

bool game_load(const char *fname)
{
FILE *fp;
int x, y, i;
	
	fp = fopen(fname, "rb");
	if (!fp)
	{
		staterr("failed to open savegame file '%s': %s", fname, strerror(errno));
		return 1;
	}
	
	if (!fverifystring(fp, "ika_en03"))
	{
		staterr("invalid savegame format!");
		fclose(fp);
		return 1;
	}
	
	stat("reading savefile %s...", fname);
	
	player->x 			= fgetl(fp);
	player->y			= fgetl(fp);
	player->hp			= fgeti(fp);
	player->xp			= fgeti(fp);
	player->level		= fgetc(fp);
	player->equipmask	= fgetc(fp);
	
	fread(game.flagdata, FLAGDATA_LEN, 1, fp);
	fread(player->inventory, NUM_INVENTORY_SLOTS, 1, fp);
	
	// load map
	map.xsize = fgetl(fp);
	map.ysize = fgetl(fp);
	
	if (map.xsize >= MAP_MAXSIZEX || map.ysize >= MAP_MAXSIZEY)
	{
		staterr("invalid map size %dx%d", map.xsize, map.ysize);
		return 1;
	}
	
	for(y=0;y<map.ysize;y++)
		for(x=0;x<map.xsize;x++)
			map.tiles[x][y] = fgetc(fp);
	
	// load objects
	for(i=0;i<MAX_OBJECTS;i++)
	{
		objects[i].x = fgetl(fp);
		objects[i].y = fgetl(fp);
		objects[i].x2 = fgetl(fp);
		objects[i].y2 = fgetl(fp);
		objects[i].xinertia = fgetl(fp);
		objects[i].yinertia = fgetl(fp);
	}
	
	map_centeron(player->CenterX(), player->CenterY());
	player->canattacktime = 100;
	PUpdateEquip();
	
	game.hidescene = false;
	game.hidestatusbar = false;

	stat("loaded savegame successfully, player x/y {%d, %d}, level %d",
			player->x, player->y, player->level);
	return 0;
}


bool game_save(const char *fname)
{
FILE *fp;
int x, y, i;

	stat("writing savefile %s...", fname);
	fp = fopen(fname, "wb");
	if (!fp)
	{
		staterr("failed to open savegame '%s'", fname);
		return 1;
	}
	
	fputstringnonull("ika_en03", fp);
	
	fputl(player->x, fp);
	fputl(player->y, fp);
	fputi(player->hp, fp);
	fputi(player->xp, fp);
	fputc(player->level, fp);
	fputc(player->equipmask, fp);
	
	fwrite(game.flagdata, FLAGDATA_LEN, 1, fp);
	fwrite(player->inventory, NUM_INVENTORY_SLOTS, 1, fp);
	
	// save map
	fputl(map.xsize, fp);
	fputl(map.ysize, fp);
	
	for(y=0;y<map.ysize;y++)
		for(x=0;x<map.xsize;x++)
			fputc(map.tiles[x][y], fp);
	
	// save objects
	for(i=0;i<MAX_OBJECTS;i++)
	{
		fputl(objects[i].x, fp);
		fputl(objects[i].y, fp);
		fputl(objects[i].x2, fp);
		fputl(objects[i].y2, fp);
		fputl(objects[i].xinertia, fp);
		fputl(objects[i].yinertia, fp);
	}
	
	return 0;
}

bool savefile_exists()
{
	return file_exists("Ika.rec");
}

/*
void c------------------------------() {}
*/

void start_loading(void)
{
	DrawScene();
	DrawLoading();
	screen->Flip();
	
	game.loadstarttime = SDL_GetTicks();
}

// emulate the brief loading pause by giving a minimum amount of time the
// loading screen must be visible.
void end_loading(void)
{
	int elapsed = (SDL_GetTicks() - game.loadstarttime);
	int left = (100 - elapsed);
	
	if (left > 0)
		SDL_Delay(left);
}

void DrawLoading(void)
{
	int x = (SCREEN_WIDTH / 2) - (sprites[SPR_LOADING].w / 2);
	int y = (SCREEN_HEIGHT / 2) - (sprites[SPR_LOADING].h - 4);
	
	draw_sprite(x, y, SPR_LOADING);
}

/*
void c------------------------------() {}
*/

void Game::close()
{
	if (tickfunctions[game.mode].OnExit)
		tickfunctions[game.mode].OnExit();
}





