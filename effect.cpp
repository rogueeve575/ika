
#include "ika.h"
#include "effect.h"
#include "effect.fdh"

static struct
{
	uint8_t state;
	int timer;
} ef;

bool effect_init(void)
{
	memset(&ef, 0, sizeof(ef));
	return 0;
}

bool draw_effects(void)
{
	switch(ef.state)
	{
		case EFFECT_QUAKE: 	   return run_earthquake();
		case EFFECT_LONGQUAKE: return run_long_earthquake();
		
		case EFFECT_FADEIN:  return run_fade(true);
		case EFFECT_FADEOUT: return run_fade(false);
	}
	
	return 0;
}

void start_effect(int type)
{
	ef.state = type;
	ef.timer = 0;
}

/*
void c------------------------------() {}
*/

bool run_earthquake(void)
{
CaretMaker *cm;

	if (++ef.timer > 60)
	{
		ef.state = 0;
		return 1;
	}
	
	map.xscroll += randrange(-10, 10) << CSF;
	map.yscroll += randrange(-10, 10) << CSF;
	
	static const int types[] = { CAR_BUBBLE, CAR_STAR, -1 };
	for(int i=0;types[i]!=-1;i++)
	{
		cm = CreateCaretMaker(types[i], 2);
		cm->x = map.xscroll + ((SCREEN_WIDTH / 2) << CSF);
		cm->y = map.yscroll + ((SCREEN_HEIGHT / 2) << CSF);
		cm->xrange = (SCREEN_WIDTH / 2) + 53;
		cm->yrange = (SCREEN_HEIGHT / 2) + 40;
	}
	
	return 0;
}


bool run_long_earthquake(void)
{
CaretMaker *cm;

	if ((++ef.timer % 4) == 0)
	{
		map.xscroll += randrange(-1, 1) << CSF;
		map.yscroll += randrange(-1, 1) << CSF;
		
		static const int types[] = { CAR_BUBBLE, CAR_STAR, -1 };
		for(int i=0;types[i]!=-1;i++)
		{
			cm = CreateCaretMaker(types[i], 2);
			cm->x = map.xscroll + ((SCREEN_WIDTH / 2) << CSF);
			cm->y = map.yscroll + ((SCREEN_HEIGHT / 2) << CSF);
			cm->xrange = (SCREEN_WIDTH / 2) + 53;
			cm->yrange = (SCREEN_HEIGHT / 2) + 40;
		}
	}
	
	return 0;
}


bool run_fade(bool fadein)
{
	int x, y;
	int startframe = 0;
	
	for(y=SCREEN_HEIGHT-16;y>=0;y-=16)
	{
		int f = 0;
		
		for(x=0;x<SCREEN_WIDTH;x+=16)
		{
			int frame = (ef.timer - startframe - f);
			
			if (frame < 0) frame = 0;
			if (frame > 15) frame = 15;
			
			if (fadein)
				frame = 15 - frame;
			
			if (frame)
				draw_sprite(x, y, SPR_FADE, frame);
			
			f++;
		}
		
		startframe++;
	}
	
	if (++ef.timer > 50)
	{
		ef.state = 0;
		return 1;
	}
	
	return 0;
}





