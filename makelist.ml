
<DEFAULT
COMPILE=g++ -g -O2 -c %SRCFILE% `sdl-config --cflags` %CPPFLAGS% -Wreturn-type -Wno-unused-variable -Wno-multichar -o %MODULE%.%OBJ_EXT%
LPREFIX=g++ -o %OUTPUT%
LSUFFIX=`sdl-config --libs` -lSDL_ttf
OBJ_EXT=o
OUTPUT=ika

>>
main.cpp
game.cpp
object.cpp
map.cpp
input.cpp
player.cpp
ai.cpp
caret.cpp
event.cpp
eventreader.cpp
textbox.cpp
itembox.cpp
effect.cpp
ironhead.cpp
roll.cpp
title.cpp

editor/editor.cpp
editor/dropdown.cpp
editor/tilemode.cpp
editor/spritemode.cpp

graphics/graphics.cpp
graphics/font.cpp
graphics/nxsurface.cpp
graphics/tileset.cpp
graphics/sprites.cpp

sslib.cpp
sound.cpp
pmd.cpp

console.cpp
debug.cpp

autogen/AssignSprites.cpp

siflib/sif.cpp
siflib/sifloader.cpp
siflib/sectSprites.cpp
siflib/sectStringArray.cpp

common/InitList.cpp
common/StringList.cpp
common/DBuffer.cpp
common/DString.cpp
common/bufio.cpp
common/stat.cpp
common/misc.cpp
<<

//sound/sound.cpp
