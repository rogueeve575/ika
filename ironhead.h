
#ifndef _IRONHEAD_H
#define _IRONHEAD_H

struct Ironhead
{
	bool exists;
	uint8_t state;
	
	int16_t hp;
	uint16_t hurttimer;
	
	uint16_t touchscript;
	uint16_t defeatscript;
	
	uint16_t flashtimer;
	
	uint8_t frame;
	uint16_t animtimer;
	
	uint8_t dir;
	
	int x, y;
	int xinertia, yinertia;
	int x2, y2;
	
	struct
	{
		int x1, y1, x2, y2;
	} bbox;
};

void init_ironhead();
void draw_ironhead();
void run_ironhead();
void hurt_ironhead(int amt);
void PRunIronheadInteraction(void);

extern Ironhead ironh;

#endif
