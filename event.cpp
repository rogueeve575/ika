
#include "ika.h"
#include "event.h"
#include "eventreader.h"
#include "event.fdh"

static Script script;
static ScriptCPU cpu;

static inline uint8_t read8(const uint8_t **program);
static inline uint16_t read16(const uint8_t **program);
static char *readtext(const uint8_t **program, int len, char *buffer);
static bool str_replace(char *str, const char *search, const char *replace);
const char *main_script_fname = "Event.ptx";

bool script_init(void)
{
	script.init();

	char fname[MAXPATHLEN];
	GetDataFilename(main_script_fname, fname, sizeof(fname));

	if (event_compile(fname, &script))
		return 1;
	
	memset(&cpu, 0, sizeof(cpu));
	StopScript();
	return 0;
}

void script_close(void)
{
	script.freedata();
}

/*
void c------------------------------() {}
*/

void StartScript(int no)
{
	cpu.program = script.program.Data();
	cpu.waittime = 0;
	
	JumpScript(no);
}

void StopScript(void)
{
	cpu.program = NULL;
	cpu.curscript = -1;
	
	textbox.SetVisible(false);
	textbox.ClearText();
}


void JumpScript(int no)
{
	if (no < 0 || no >= MAX_EVENTS || !script.epnames[no])
	{
		staterr("invalid event #%04d!", no);
		return;
	}
	
	if (script.epnames[no][0])
		stat(" -- starting event %04d: '%s'", no, script.epnames[no]);
	else
		stat(" -- starting event %04d", no);
	
	cpu.program = script.program.Data();
	cpu.curscript = no;
	cpu.ip = script.entrypoints[no];
}

void WaitScript(int time)
{
	cpu.waittime += time;
}

int GetCurrentScript()
{
	return cpu.curscript;
}


/*
void c------------------------------() {}
*/

void run_events(void)
{
ScriptCommand cmd;
int len;

	//#define IGNORE	{ staterr("IGNORING OPCODE...."); break; }
	
	for(;;)
	{
		if (!cpu.program) return;
		if (textbox.IsBusy()) return;
		
		if (cpu.waittime)
		{
			cpu.waittime--;
			return;
		}
		
		len = FetchCommand(cpu.program, cpu.ip, &cmd);
		PrintCommand(&cmd, cpu.ip);
		cpu.ip += len;
		
		if (ExecCommand(&cmd))
		{
			console.Print("invalid opcode %s; script halted", GetOpName(cmd.op));
			StopScript();
			return;
		}
	}
}


// execute the given script command
bool ExecCommand(ScriptCommand *cmd)
{
	switch(cmd->op)
	{
		// -basics--------------------------------
		case OP_FJ:		// flagjump to event Y if flag X is set
			if (game.readflag(cmd->p1)) JumpScript(cmd->p2);
		break;
		
		case OP_FPLUS:
			game.setflag(cmd->p1, true);
		break;
		
		case OP_IJ: if (PHasItem(cmd->p1)) { JumpScript(cmd->p2); } break;
		case OP_IPLUS: PGiveItem(cmd->p1); sound(SND_ITEM); break;
		case OP_IMINUS: PTakeItem(cmd->p1); break;
		
		case OP_SE:
			sound(cmd->p1);
		break;
		
		case OP_WA:
			cpu.waittime = cmd->p1;
		break;
		
		case OP_END:
			StopScript();
		break;
		
		case OP_EX:
			game.running = false;
		break;
		
		// -savegame------------------------------
		
		case OP_CE: restore_enemies(); break;
		case OP_DL:
		{
			if (!savefile_exists())
				BeginMaruAme();
			else {
				sound(SND_SAVE);
				if (game_load()) game.running = false;
				start_effect(EFFECT_FADEIN);
				
				if (cpu.curscript == 1)
					music(0, true);
			}
		}
		break;
		
		case OP_DS:
		{
			sound(SND_SAVE);
			game_save();
		}
		break;
		
		case OP_RE:
		{
			player->enabled = true;
			player->died = false;
			player->hurtflashtime = 0;
			player->canattacktime = 100;
			player->xinertia = 0;
			player->yinertia = 0;
			player->dir = UP;
			player->frame = 0;
		}
		break;
		
		// -escape--------------------------------
		
		case OP_EC:		// someone boarded the Ship
			game.entcount++;
			if (game.entcount == 12) ironh.exists = false;
			sound(SND_ITEM);
		break;
		
		case OP_EJ:
			if (game.entcount == cmd->p1)
				JumpScript(cmd->p2);
		break;
		
		case OP_GE:
			player->controlmode = CONTROL_ESCAPING;
		break;
		
		// -effects-------------------------------
		
		case OP_EK: start_effect(EFFECT_QUAKE); break;
		case OP_FO: start_effect(EFFECT_FADEOUT); break;
		case OP_FI: start_effect(EFFECT_FADEIN); break;
		case OP_LE: start_effect(EFFECT_LONGQUAKE); break;
		
		// ---------------------------------------
		
		case OP_EPLUS:	// restore energy
			PGiveHP(9999);
			sound(SND_LIFEUP);
		break;
		
		// -control-------------------------------
		
		case OP_PC:		// change tile at X,Y to ZZZZ
			map.tiles[cmd->p1][cmd->p2] = cmd->p3;
		break;
		
		case OP_CM:		// move NPC X to Y:Z
		{
			if (cmd->p1 < MAX_OBJECTS)
			{
				objects[cmd->p1].x = MAPX(cmd->p2);
				objects[cmd->p1].y = MAPX(cmd->p3);
			}
		}
		break;
		
		case OP_MA:		// disable scene rendering
			game.hidescene = true;
		break;
		
		case OP_NM:		// enable scene rendering
			game.hidescene = false;
		break;
		
		case OP_FT:		// focus transfer
			map.focustype = cmd->p1;
			map.focusparm = cmd->p2;
		break;
		
		case OP_BO:		// start boss fight
			ironh.state = 1;
		break;
		
		case OP_CRE:	// <* credits roll
		{
			if (roll_init("Staff.ptx", "Staff.pbm", 5, false))
				game.running = false;
			else
				game.setmode(GM_ROLL);
		}
		break;
		
		// --music--------------------------------
		
		case OP_PF: pmd_fade(PMD_ZERO_VOLUME); break;			// fade music -> silence
		case OP_PH: pmd_fade(PMD_QUIET_VOLUME); break;			// fade music -> quiet
		case OP_PN: pmd_set_volume(PMD_NORMAL_VOLUME); break;	// music vol -> normal
		
		case OP_PD:
		{
			// don't restart music if it's the same track
			if (cmd->p1 != getlastsong() || !pmd_is_playing())
			{
				music(cmd->p1, (cmd->p1 != 4));
			}
		}
		break;
		
		case OP_PP: music(getlastsong(), false); break;
		case OP_PS: pmd_stop(); break;
		
		// --message------------------------------
		
		case OP_TEXT:
		{
			textbox.SetVisible(true);
			textbox.AddText(cmd->textbuffer);
		}
		break;
		
		case OP_NOD:
			textbox.WaitForKey();
		break;
		
		case OP_NEWLINE:
			textbox.Newline();
		break;
		
		case OP_YN:
		{
			bool def = YES;
			if (cpu.curscript == 6) def = NO;		// you died, want to exit?
			if (cpu.curscript == 1)
			{
				if (savefile_exists())
					def = NO;		// start a new game?
			}
			
			textbox.ShowYNPrompt(cmd->p1, def);
			
			// the prompt at title "Start a new game?"
			if (cpu.curscript == 1)
				textbox.fCallbackOnYes = BeginMaruAme;
		}
		break;
		
		default:
			return 1;
	}
	
	return 0;
}

/*
void c------------------------------() {}
*/

static inline uint8_t read8(const uint8_t **program)
{
	uint8_t value = *((uint8_t *)(*program));
	(*program)++;
	return value;
}

static inline uint16_t read16(const uint8_t **program)
{
	uint16_t value = *((uint16_t *)(*program));
	(*program) += 2;
	return value;
}

static char *readtext(const uint8_t **program, int len, char *buffer)
{
	memcpy(buffer, ((const char *)(*program)), len);
	buffer[len] = 0;
	
	(*program) += len;
	return buffer;
}

void PrintCommand(ScriptCommand *cmd, int ip)
{
char buffer[1024];
DString str;
	
	const char *scrip = (cpu.curscript != -1) ? stprintf("%04d", cpu.curscript) : "xxxx";
	sprintf(buffer, "%s:%04x    %s", scrip, ip, GetOpName(cmd->op));
	str.AppendString(buffer);
	
	if (cmd->op == OP_TEXT)
	{
		str.AppendString(stprintf(":%004d", cmd->p1));
		
		str.AppendString(" '");
		str.AppendString(cmd->textbuffer);
		str.AppendChar('\'');
	}
	else
	{
		for(int i=0;i<cmd->nparams;i++)
		{
			sprintf(buffer, "%04d", cmd->parm[i]);
			
			str.AppendChar((i == 0) ? ' ':'-');
			str.AppendString(buffer);
		}
	}
	
	stat("%s", str.String());
}

// read the next command at offset "pos" in the given program buffer
// and populate 'cmd'.
// returns the length of the command in bytes.
int FetchCommand(const uint8_t *program, int pos, ScriptCommand *cmd)
{
const uint8_t *orgprogram;

	program += pos;
	orgprogram = program;
	
	cmd->op = read8(&program);
	cmd->nparams = read8(&program);
	if (cmd->nparams > MAX_PARAMS)
		cmd->nparams = MAX_PARAMS;
	
	for(int i=0;i<cmd->nparams;i++)
		cmd->parm[i] = read16(&program);
	
	if (cmd->op == OP_TEXT)
	{
		readtext(&program, cmd->p1, cmd->textbuffer);
		fix_spelling(cmd->textbuffer);
	}
	
	return (program - orgprogram);
}

/*
void c------------------------------() {}
*/

// corrects spelling mistakes in the translation
void fix_spelling(char *buffer)
{
static const char *replace[] =
{
	"Thankyou",    		"Thank you",
	"outter",	  		"outer",
	"immediatley",		"immediately",
	"He is the one",	"He's the one",
	"What have you done, again?",		"What is it you've done again?",
	"if if",			"you if",		// in comment about globefish
	NULL,		   NULL
};

	for(int i=0;replace[i];i+=2)
	{
		while(str_replace(buffer, replace[i], replace[i+1])) ;
	}
	
	// Ben's message in inventory box; missing punctuation.
	if (!strcmp(buffer, "you know"))
		strcat(buffer, ".");
}

// in "str", replace the first occurance of "search" with "replace".
// returns nonzero if anything replaced.
static bool str_replace(char *str, const char *search, const char *replace)
{
	char *ptr, *srcptr;
	
	ptr = strstr(str, search);
	if (!ptr) return false;
	
	char tempbuffer[strlen(str) + 1];
	strcpy(tempbuffer, str);
	
	strcpy(ptr, replace);
	
	srcptr = tempbuffer + (ptr - str) + strlen(search);
	ptr += strlen(replace);
	
	strcpy(ptr, srcptr);
	
	return true;
}



