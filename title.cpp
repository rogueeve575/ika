
#include "ika.h"
#include "title.h"
#include "title.fdh"

static NXSurface *sfc = NULL;
static Title title;

const char *TitleBmp = "Opening.pbm";

bool title_init(void)
{
	char tbmpname[MAXPATHLEN];
	GetGraphicsFilename(TitleBmp, tbmpname, sizeof(tbmpname));
	sfc = NXSurface::FromFile(tbmpname);
	if (!sfc) {
		staterr("failed to load title graphics '%s", tbmpname);
		return 1;
	}
	
	title.logo_y = -83;
	title.scroller_x = -128;
	title.scroller_y = -64;
	title.presented_x = SCREEN_WIDTH;
	
	newinputs &= ~ZKEY;
	effect_init();
	
	music(3, false);
	return 0;
}

void title_close(void)
{
	delete sfc;
	sfc = NULL;
}

/*
void c------------------------------() {}
*/

void title_tick(void)
{
	if (newinputs & ZKEY)
		start_effect(EFFECT_FADEOUT);
	
	title_draw();
	
	if (draw_effects())
	{
		if (inputs & SKEY)
		{
			game.setmode(GM_EDITOR);
		}
		else
		{
			game.setmode(GM_NORMAL);
			game.hidescene = true;
			game.hidestatusbar = true;
			
			StartScript(1);
		}
	}
}


void title_draw(void)
{
	FillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 255, 255, 255);
	
	draw_sprite(65, title.logo_y, SPR_LOGO);
	draw_sprite(132, 137, SPR_KANI_TITLE);
	
	draw_sprite(title.presented_x, 205, SPR_PRESENTEDBY);
	
	for(int y=0;y<SCREEN_HEIGHT+64;y+=64)
	{
		int xa = (title.scroller_x >> 1);
		int ya = (title.scroller_y + y);
		
		draw_sprite(xa, ya, SPR_SCROLLER, 0, LEFT);
		draw_sprite((SCREEN_WIDTH - 48) - xa, ya, SPR_SCROLLER, 0, RIGHT);
	}
	
	if (title.logo_y < 45)
		title.logo_y++;
	
	if (++title.scroller_y >= 0)
		title.scroller_y -= 64;
	
	if (title.scroller_x < 0)
		title.scroller_x++;
	
	if (title.presented_x > 64)
	{
		title.presented_x -= 2;
		return;
	}
	
	// "Push Z" flasher
	if ((title.scroller_y & 32) == 0)
	{
		draw_sprite(128, 126, SPR_PUSH_Z);
	}
}
























