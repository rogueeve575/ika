
#ifndef __DATADIRS_H
#define __DATADIRS_H

const char *GetDataFilename(const char *fname, char *buffer, int bufsz);
const char *GetGraphicsFilename(const char *fname, char *buffer, int bufsz);
const char *GetMusicFilename(const char *fname, char *buffer, int bufsz);
const char *GetSoundFilename(const char *fname, char *buffer, int bufsz);

#endif

