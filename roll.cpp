
#include "ika.h"
#include "roll.h"
#include "common/llist.h"
#include "roll.fdh"

#define SCREEN_Y(Y)		( (Y) - (scroll_y >> CSF) )
#define TEXT_SPACING	5	// X-spacing between letters
#define LINE_SPACING	20	// Y-spacing between lines

static Roll *roll = NULL;


bool Roll::Init(const char *scriptname, const char *bgname, int songno, bool ngwhendone)
{
	// load data
	script.init();

	char scriptname_full[MAXPATHLEN];
	GetDataFilename(scriptname, scriptname_full, sizeof(scriptname_full));
	if (event_compile(scriptname_full, &script))
		return 1;

	char bgname_full[MAXPATHLEN];
	GetGraphicsFilename(bgname, bgname_full, sizeof(bgname_full));
	backdrop = NXSurface::FromFile(bgname_full, false);
	if (!backdrop) return 1;
	
	cpu.program = script.program.Data();
	cpu.ip = 0;
	
	// initilize roll
	spawn_y = (SCREEN_HEIGHT + 8);
	scroll_y = 0 << CSF;
	
	xoffset = 0;
	roll_running = true;
	
	lines_vis = 0;
	
	firstline = NULL;
	lastline = NULL;
	
	// start music
	music(songno, false);
	this->ngwhendone = ngwhendone;
	return 0;
}

Roll::~Roll()
{
	script.freedata();
	if (backdrop) { delete backdrop; backdrop = NULL; }
}

/*
void c------------------------------() {}
*/

void Roll::Tick(void)
{
	/*debug("scroll_y: %d", scroll_y>>CSF);
	debug("spawn_y: %d", spawn_y);
	debug("scr_spawn_y: %d", SCREEN_Y(spawn_y));
	debug("trigger: %d", SCREEN_HEIGHT+MARGIN);
	debug("lines_vis: %d", lines_vis);*/
	
	DrawSurface(backdrop, 0, 0);
	
	if (roll_running || lines_vis > 0)
	{
		int speed = (ngwhendone) ? 0x400 : 0x200;
		scroll_y += (inputs & SPACEKEY) ? 0xC00 : speed;
	}
	else if (ngwhendone)
	{
		BeginNewGame();
		return;
	}
	
	while(roll_running && SCREEN_Y(spawn_y) <= SCREEN_HEIGHT)
	{
		RunNextCommand();
	}
	
	DrawLines();
}


void Roll::RunNextCommand(void)
{
ScriptCommand cmd;

	int len = FetchCommand(cpu.program, cpu.ip, &cmd);
	PrintCommand(&cmd, cpu.ip);
	cpu.ip += len;
	
	switch(cmd.op)
	{
		case OP_TEXT:
		{
			RollLine *line = NewLine();
			
			maxcpy(line->text, cmd.textbuffer, sizeof(line->text));
			line->y = spawn_y;
			
			spawn_y += LINE_SPACING;
		}
		break;
		
		case OP_NEWLINE:
			spawn_y += LINE_SPACING;
		break;
		
		case OP_END:
			roll_running = false;
		break;
		
		default:	// pass "FO" to normal script executor
			ExecCommand(&cmd);
		break;
	}
}

/*
void c------------------------------() {}
*/

bool Roll::DrawLine(RollLine *line)
{
	int x = 12;
	int y = SCREEN_Y(line->y);
	
	if (y < -GetFontHeight())
		return true;	// line can be deleted now
	
	font_draw(x, y, line->text, TEXT_SPACING, &rollfont);
	return false;
}


void Roll::DrawLines()
{
RollLine *line, *next;

	line = firstline;
	while(line)
	{
		next = line->next;
		
		if (DrawLine(line))
		{
			RemoveLine(line);
			delete line;
		}
		
		line = next;
	}
}

/*
void c------------------------------() {}
*/

RollLine *Roll::NewLine()
{
	return AddLine(new RollLine);
}

RollLine *Roll::AddLine(RollLine *line)
{
	LL_ADD_END(line, prev, next, firstline, lastline);
	
	lines_vis++;
	return line;
}

void Roll::RemoveLine(RollLine *line)
{
	LL_REMOVE(line, prev, next, firstline, lastline);
	lines_vis--;
}

/*
void c------------------------------() {}
*/

bool roll_init(const char *scriptname, const char *bgname, int songno, bool ngwhendone)
{
	roll = new Roll;
	if (roll->Init(scriptname, bgname, songno, ngwhendone))
	{
		staterr("Roll initilization failed");
		return 1;
	}
	
	return 0;
}

void roll_close()
{
	delete roll;
	roll = NULL;
}

void roll_tick()
{
	if (roll)
		roll->Tick();
}

