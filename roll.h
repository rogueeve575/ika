
#ifndef _ROLL_H
#define _ROLL_H

#include "event.h"
#include "eventreader.h"

#define ROLL_MAX_LINELEN		80

// a currently displayed line of text
struct RollLine
{
	char text[ROLL_MAX_LINELEN];
	int y;
	
	RollLine *next, *prev;
};

class Roll
{
public:
	bool Init(const char *scriptname, const char *bgname, int songno, bool ngwhendone);
	void Tick();
	~Roll();
	
private:
	void RunNextCommand();
	
	RollLine *NewLine();
	RollLine *AddLine(RollLine *line);
	void RemoveLine(RollLine *line);
	
	void DrawLines();
	bool DrawLine(RollLine *line);
	
	Script script;
	ScriptCPU cpu;
	NXSurface *backdrop;
	
	bool ngwhendone;	// start a new game when roll finished?

	int spawn_y;		// position of next line relative to top of roll
	int scroll_y;		// CSFd roll position
	
	int xoffset;		// x position of next line
	
	// turns off scrolling and further script execution at end
	bool roll_running;
	int lines_vis;
	
	RollLine *firstline, *lastline;
};


void roll_tick(void);
void roll_close(void);

#endif
