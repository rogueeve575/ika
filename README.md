
# nxIka

This is a portable full clone/reimplementation of the freeware Studio Pixel
game "Ikachan" in SDL.

I originally wrote this some time ago (after writing [NXEngine](https://nxengine.sourceforge.io/)), but never got around to really properly releasing it besides sending it to a few people here and there.

This includes a few fixes and improvements not released before; I haven't done any game development in a while, having moved onto other things, but I don't want this code to just languish since there *was* a fair bit of effort put into it at the time.

Note that there is another unrelated [Ikachan clone/port](https://github.com/cuckydev/Ikachan) who beat me to release; the other one is based on a decompile I believe, whereas this one was written by hand with some reverse engineering work.

### Building

The latest fixups and improvements are in the "fixups" branch; this is probably what you'll want; as the "master" branch currently contains mostly the state as I found the project on my hard drive before picking it up again, which was a little broken.

On Linux/UNIX you should be able to just:
```
    $ git clone https://gitlab.com/rogueeve575/ika.git
    $ cd ika
    $ git checkout -b fixups origin/fixups
    $ make -j$(nproc)
```
### Dependencies:
Dependencies are few; you'll need SDL 1.2 (the old version) and SDL_ttf. For Arch Linux:
```
$ sudo pacman -S sdl12-compat sdl_ttf --needed
```
...should be sufficient. Other (personal) libraries used by the program are included in-tree in the 'common/' directory and should automatically link in to the main executable with just a normal make.

Oh and btw, I don't really know what the "nx" in either of my clones stands for :-P.

