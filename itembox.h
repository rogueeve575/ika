
#ifndef _ITEMBOX_H
#define _ITEMBOX_H

#define IB_X				72
#define IB_Y				128
#define ISEL_X				(IB_X + 8)
#define ISEL_Y				(IB_Y + 8)

class ItemBox
{
public:
	bool Init();
	void Reset();
	
	void SetVisible(bool enable);
	bool IsVisible(void);
	void Update();
	
	void Run();
	void Draw();
	
private:
	bool fVisible;
	int fCurSel;
};

extern ItemBox itembox;

#endif

